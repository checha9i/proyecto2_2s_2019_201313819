/*module.exports = {
    id: "",
    valor: null,
    Tipo: "",
    rol: "variable",
    Tamano: 0,
    Dimensiones: null,
    ambito: "global",
    ref: false,
    posicion: 0,
    cantidad_dimensiones: 0,
    param: false,
    newVariable: (N, T, S, P) => {
        this.id = N;
        this.id = N;
        this.Tipo = T;
        this.ambito = S;
        this.posicion = P;
        this.rol = "variable";
        this.Tamano = 1;
        this.ref = false;
    },
    newConstant: (N, T, P) => {
        this.id = N;
        this.Tipo = T;
        this.ambito = "Global";
        this.posicion = P;
        this.rol = "constante";
        this.Tamano = 1;
        this.ref = false;
    }
}*/

class Simbolo {
    constructor() {
        this.id = "";
        this.valor = null;
        this.Tipo = "";
        this.Tamano = 0;
        this.TipoArreglo = "";
        this.rol = "variable";
        this.Dimensiones = null;
        this.ref = false;
        this.ambito = "global";
        this.posicion = 0;
        this.cantidad_dimensiones = 0;
        this.param = false;
        this.globalNoPrimeraPasada = false;
    }

    newVariable(N, T, S, P) {

        this.id = N;
        this.Tipo = T;
        this.ambito = S;
        this.posicion = P;
        this.rol = "variable";
        this.Tamano = 1;
        this.ref = false;
    }

    newConstant(N, T, P) {
        this.id = N;
        this.Tipo = T;
        this.ambito = "Global";
        this.posicion = P;
        this.rol = "constante";
        this.Tamano = 1;
        this.ref = false;
    }
}

module.exports = Simbolo;