module.exports = class Graficador {
    constructor() {
            /**
             * @author Cesar Solares
             *
             *
             *
             */
            this.CadenaDot = "";
        }
        /**
         *
         * @param raiz
         */
    GraficarAST(raiz) {
            //Creo una carpeta en /home/usuario/SalidasDot, en donde va estar todo
            var dir = "./SalidasDot";
            //Rutas para el .dot y la imagen .png
            var ruta_dot = "./routes/SalidasDot/ast.dot";
            var ruta_png = "./routes/SalidasDot/ast.png";
            //Arma el contenido del .dot
            this.armar_Cuerpo_dot(raiz, ruta_dot);
            const fs = require('fs');
            fs.writeFile(ruta_dot, "", (error) => {
                if (error)
                    console.log(error);
            });
            //Genera el archivo .dot y su imagen .png
            this.crearGrafo(ruta_dot, ruta_png);
            //Abre la imagen resultante .png
            this.autoAbrir(ruta_png);
        }
        //Este metodo se puede usar para graficar cualquier grafo
        //debido a q solo necesita al ruta del dot y la ruta de la salida->imagen
    crearGrafo(ruta_dot, ruta_png) {
            const fs = require('fs');
            var exec = require('child_process').exec;

            function puts(error, stdout, stderr) { sys.puts(stdout) }
            exec("if not exist \".\\\\routesSalidasDot\\\" mkdir .\\routes\\SalidasDot", function(error, stdout, stderr) {
                if (!error) {
                    // things worked!
                } else {
                    // things failed :(
                }
            });
            exec("if not exist " + ruta_dot + " echo \"\" > .\\routes\\SalidasDot\\ast.dot", function(error, stdout, stderr) {
                if (!error) {
                    // things worked!
                } else {
                    // things failed :(
                }
            });
            var texto = "";
            fs.readFile(ruta_dot, (error, datos) => {
                if (error) {
                    console.log(error);
                } else {
                    texto = datos.toString();
                }
            });
            fs.appendFile(ruta_dot, this.CadenaDot + "}", (error) => {
                if (error) {
                    console.log(error);
                } else {
                    console.log('El archivo fue creado');
                }
            });
            var tParam = "-Tpng";
            var tOParam = "-o";
            var cmd = "dot " + tParam + " " + ruta_dot + " " + tOParam + " " + ruta_png;

            exec(cmd, function(error, stdout, stderr) {
                if (!error) {
                    // things worked!
                } else {
                    // things failed :(
                }
            });
            // On Windows Only ...
            //const { spawn } = require('child_process');
            //const bat = spawn('cmd.exe', ['/c', cmd]);
        }
        //Este metodo es generico
        //Porque solo necesita un nodo para crear el .dot
    armar_Cuerpo_dot(raiz, ruta_dot) {
            var contador = 0;
            var buffer = require('fs');
            this.CadenaDot += "\ndigraph G {\r\nnode [shape=box, style=filled, color=skyblue, fontcolor=black];\n";
            this.CadenaDot += this.listarNodos(raiz, ruta_dot, buffer, contador, "");
        }
        //Este metodo es generico
        //Porque solo necesita un nodo para lista y generar una porcion
        //de lo que sera el fichero .dot
    listarNodos(praiz, ruta_dot, buffer, contador, acum) {
            //graphviz+="node"+contador+"[label=\""+praiz.valor+"\"];\n";
            var cont = contador;
            if (praiz.etiqueta != undefined) {
                acum += "node" + praiz.id + "[label=\"" + praiz.etiqueta.replace(/\"/gi, "\\\"") + "\"];\n";
            }


            if (praiz.hijos != undefined) {

                praiz.hijos.forEach(function(value) {
                    cont++;
                    acum += "\"node" + praiz.id + "\"->";
                    acum += "\"node" + value.id + "\";\n";
                    acum += Graficador.prototype.listarNodos(value, ruta_dot, buffer, cont, "");
                    cont++;
                });
            }

            return acum;
        }
        //Este metodo es generico
        //Porque solo necesita un nodo para lista y generar una porcion
        //de lo que sera el fichero .dot
    enlazarNodos(pRaiz, ruta_dot, buffer) {
            var relacion = "";
            pRaiz.hijos.forEach(function(value) {
                relacion += "\"node" + pRaiz.id + "\"->";
                relacion += "\"node" + value.id + "\";\n";
                buffer.appendFile(ruta_dot, relacion, (error) => {
                    if (error) {
                        throw error;
                    }
                });
                Graficador.prototype.enlazarNodos(value, ruta_dot, buffer);
            });
            //this.CadenaDot+= relacion;
        }
        //Este metodo es generico
        //Por que abre un fichero, archivo, etc. en base a la ruta
        //Lo abre con el programa predeterminado por el sistema
    autoAbrir(ruta) {
            // On Windows Only ...
            // const { spawn } = require('child_process');
            //const bat = spawn('cmd.exe', ['/c', ruta]);
            return true;
        }
        //Este metodo es generico
        //Porque crea un archivo plano en base a la ruta y el contenido que se le pase
        /**
         *
         * @param Ruta
         * @param Contenido
         */
    CrearDocumento(Ruta, Contenido) {
        var fs = require('fs');
        var exec = require('child_process').exec;

        function puts(error, stdout, stderr) { sys.puts(stdout) }
        //exec("if not exist \"C:\\SalidasDot\\\" mkdir C:\\SalidasDot", function(error, stdout, stderr) {
        exec("if not exist \".\\routes\\SalidasDot\\\" mkdir .\\routes\\SalidasDot", function(error, stdout, stderr) {
            if (!error) {
                // things worked!
            } else {
                // things failed :(
            }
        });
        exec("if not exist " + Ruta + " echo " + Contenido + "  > .\\routes\\SalidasDot\\ast.dot", function(error, stdout, stderr) {
            if (!error) {
                // things worked!
            } else {
                // things failed :(
            }
        });


    }
}