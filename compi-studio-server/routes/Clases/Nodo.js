class Nodo {

    constructor(id, valor) {
        this.id = id;
        this.hijos = [];
        this.etiqueta = valor;
    }

    add_hijo(id, valor) {
        let hijo = new Nodo(id, valor);
        this.hijos.push(hijo);
    }
}

module.exports = Nodo;