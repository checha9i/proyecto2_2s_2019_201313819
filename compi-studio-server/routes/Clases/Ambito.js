var Simbolo = require('./Simbolo');

class Ambito {
    constructor() {
        this.TS = [];
        this.padre = null;
        this.Salida = "";
        this.Salida_Funciones = "";
        this.Tam_actual = 0;
        this.NoParam = 0;
        this.NombreFunc = ""
        this.funcion_actual = "";
        this.TempIni = 0;
        this.TempActual = 0;
        this.CodigoNormal = true;
        this.Return = "";
        this.Temp_Tam = 0;
        this.LSalida = "";
    }

    new_ambito(ambito) {
        (ambito) => {
            let a = new Object.create(Ambito);
            a.padre = ambito;
            return a;
        }
    }
    remove_ambito(ambito) {
        return ambito.padre;
    }
    getSize() {
        return this.TS.length;
    }
}
module.exports = Ambito;