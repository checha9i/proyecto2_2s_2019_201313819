var parser = require('./Gramatica').parser;
var ast_parser = require('./AST').parser;
var Ambito = require('./Clases/Ambito');
var Simbolo = require('./Clases/Simbolo');
var Funcion = require('./Clases/Funcion');
var Nodo = require('./Clases/Nodo');
var Graficador = require('./Clases/Graficador');

class main {
    constructor() {
        this.Temporal = 0;
        this.Label = 0;
        this.Texto = "";
        this.Salida = "";
        this.ambito = new Ambito();
        this.contGlobal = 0;
        this.Funciones = [];
        this.LFinal = "";
        this.LVT = "";
        this.LFT = "";
        this.TipoCiclo = "";
        this.Types = [];
        this.Funcion_actual = [];
        this.Errores = [];
        this.NombreEntorno = [];
        this.contLocal = 0;
        this.ReporteTS = [];
        this.primerapasada = true;
        this.raiz = new Nodo(0, "S");
        this.Salida_Funciones = "";
    }


    //Limpiar variables
    Clear_Variables() {
        this.Funciones.length = 0
        this.Funciones = []
        this.Label = 0;
        this.Temporal = 0;
        this.Salida = "";
        this.ambito = new Ambito();
        this.contGlobal = 0;
        this.LFT = "";
        this.LVT = "";
        this.TipoCiclo = "";
        this.LFinal = "";
        this.Types = [];
        this.primerapasada = true;
        this.raiz = new Nodo(0, "S");

    }

    ReporteSimbolos() {
        let salida = [];
        let cont = 1;

        this.ReporteTS.forEach(s => {
            salida.push({
                cont: cont,
                id: s.id.toLowerCase(),
                valor: s.valor,
                tipo: s.Tipo,
                rol: s.rol
            });
            cont++;
        });

        return salida;
    }


    ReporteFunciones() {
            let salida = [];

            let cont = 1;

            /*this.ReporteTS.forEach(s => {
                salida.push({
                    cont: cont,
                    id: s.id,
                    valor: s.valor,
                    tipo: s.Tipo,
                    rol: s.rol
                });
                cont++;
            });*/
            //console.log(salida);
            return salida;
        }
        //funcion exec es para validar la cadena con el parser
    exec(input) {
        parser.Errores = { Errores: [] };
        return parser.parse(input);
    }

    getAST(input) {
            ast_parser.Errores = []
            return ast_parser.parse(input);
        }
        // esta funcion recibe la entrada y ejecuta con el parser, luego ejecuta el traductor
    Analizar(input) {
        //console.log(exec(input));
        var a = null;
        try {
            a = this.exec(input);
            this.Errores = [];
            this.Salida_Funciones = "";
            this.Clear_Variables();
            this.Errores = a[2].Errores
            this.ambito.Tam_actual++;
            this.C3D(a);

            this.main_init(a);
            this.main(a);
            this.AddVariablesToC3D();
            // console.log(this.Salida);
            let raiz = this.getAST(input);
            let g = new Graficador();
            g.GraficarAST(raiz);

        } catch (e) {
            console.log(e);
        }
        console.log(this.Errores);
        let r = {
            Salida: this.Salida,
            TS: this.ReporteSimbolos(),
            Errores: this.Errores,
            AST: null,
            Funciones: this.Funciones
        };

        return r;
    }

    BuscarGlobales(actual) {

        if (actual.hasOwnProperty('Funcion')) {
            this.primerapasada = false;
            actual.Funcion.Sentencias.forEach(element => {

                this.BuscarGlobales(element);
            });
        } else if (actual.hasOwnProperty('If')) {

            actual.If.Sentencias.forEach(element => {
                this.BuscarGlobales(element);
            });
        } else if (actual.hasOwnProperty('For')) {
            actual.For.Sentencias.forEach(element => {
                this.BuscarGlobales(element);
            });
        } else if (actual.hasOwnProperty('While')) {
            actual.While.Sentencias.forEach(element => {
                this.BuscarGlobales(element);
            });
        } else if (actual.hasOwnProperty('DoWhile')) {
            actual.DoWhile.Sentencias.forEach(element => {
                this.BuscarGlobales(element);
            });
        } else if (actual.hasOwnProperty('Switch')) {
            actual.Switch.ListaCases.forEach(element => {
                let Case = element.Case;
                Case.Sentencias.forEach(element2 => {
                    this.BuscarGlobales(element2);
                });
            });
            actual.Switch.Default.Sentencias.forEach(element => {
                this.BuscarGlobales(element);
            });
        } else if (actual.hasOwnProperty('Declaracion_Tipo4')) {
            let Declaracion_Tipo4 = actual.Declaracion_Tipo4;
            this.Declaracion_T4(Declaracion_Tipo4);
        }
    }
    C3D(input) {

        if (input.length == 3) {
            let rimport = input[0];
            let Sentencias = input[1];
            let Errores = input[2];

            Sentencias.map((sent) => {
                if (sent.hasOwnProperty('Declaracion_Tipo1')) {

                    let Declaracion_Tipo1 = sent.Declaracion_Tipo1;
                    this.Declaracion_T1(Declaracion_Tipo1);
                } else if (sent.hasOwnProperty('Declaracion_Tipo2')) {

                    let Declaracion_Tipo2 = sent.Declaracion_Tipo2;
                    this.Declaracion_T2(Declaracion_Tipo2);
                } else if (sent.hasOwnProperty('Declaracion_Tipo3')) {

                    let Declaracion_Tipo3 = sent.Declaracion_Tipo3;
                    this.Declaracion_T3(Declaracion_Tipo3);
                } else if (sent.hasOwnProperty('Declaracion_Tipo4')) {

                    let Declaracion_Tipo4 = sent.Declaracion_Tipo4;
                    this.Declaracion_T4(Declaracion_Tipo4);
                }
            });

            this.primerapasada = false;
            Sentencias.map((sent) => {

                if (sent.hasOwnProperty('Funcion')) {

                    this.BuscarGlobales(sent);
                    let F = sent.Funcion;
                    let P = [];
                    let N = "";

                    F.Parametros.forEach(p => {
                        P.push(p);
                        N += "_" + p.Tipo.Tipo;
                    });

                    F.Parametros = P;
                    F.Nombre += N.toLowerCase();
                    //  console.log(F.Nombre);
                    F.Codigo = false;
                    let b = false;
                    this.Funciones.forEach(Func => {
                        if (Func.Nombre.toLowerCase() == F.Nombre.toLowerCase()) {
                            b = true;
                        }
                    });
                    if (!b) {
                        this.Funciones.push(F);
                    }
                }
            });
            this.Importar(rimport);
        } else {
            this.Errores.push({
                error: {
                    No: this.Errores.length,
                    description: "Error en importar",
                    linea: 0,
                    columna: 0,
                    tipo: "Semantico"
                }
            })
        }
    }

    Importar(actual) {

        if (actual.length > 0) {
            actual.forEach(i => {

                let Importar = i.Import;
                let Nombre = Importar.Nombre;

                var fs = require('fs');
                var data = fs.readFileSync("./routes/Archivos/" + Nombre, "utf8");
                let a = this.exec(data);

                let rimport = a[0];
                let Sentencias = a[1];
                let Errores = a[2];
                Errores.Errores.forEach(err => {
                    this.Errores.push(err);
                });
                Sentencias.map((sent) => {

                    if (sent.hasOwnProperty('Funcion')) {

                        this.BuscarGlobales(sent);
                        let F = sent.Funcion;
                        let P = [];
                        let N = "";

                        F.Parametros.forEach(p => {
                            P.push(p);
                            N += "_" + p.Tipo.Tipo;
                        });

                        F.Parametros = P;
                        F.Nombre += N.toLowerCase();
                        //  console.log(F.Nombre);
                        F.Codigo = false;
                        let b = false;
                        this.Funciones.forEach(Func => {
                            if (Func.Nombre.toLowerCase() == F.Nombre.toLowerCase()) {
                                b = true;
                            }
                        });
                        if (!b) {
                            this.Funciones.push(F);
                        }
                    }
                });

            });
        }

    }

    Declaracion_T1(actual) {

        actual.LIST_IDENTIFICADORES.forEach(simbolo => {
            let s = new Simbolo();
            s.id = simbolo.toLowerCase();
            s.Tamano = 1;
            s.Tipo = actual.Tipo.Tipo;
            s.rol = "variable";
            s.posicion = this.contGlobal;

            if (actual.Tipo.hasOwnProperty('Corchetes')) {
                if (this.primerapasada) {
                    s.valor = actual.Valor;
                } else {
                    s.valor = 0;
                }
                s.Tipo = "Arreglo"
                s.TipoArreglo = actual.Tipo.Tipo;

                if (!this.SimboloExist(this.ambito, s.id.toLowerCase())) {
                    this.ambito.TS.push(s);
                    this.ReporteTS.push(s);
                    this.contGlobal++;
                    //   console.log(this.ambito.TS);
                } else {
                    this.Errores.push({
                        error: {
                            No: this.Errores.length,
                            description: "Ya existe la variable " + s.id.toLowerCase(),
                            linea: 0,
                            columna: 0,
                            tipo: "Semantico"
                        }
                    })
                }
            } else {
                if (this.primerapasada) {
                    s.valor = actual.Valor;
                } else {
                    s.valor = 0;
                }


                if (!this.SimboloExist(this.ambito, s.id.toLowerCase())) {
                    this.ambito.TS.push(s);
                    this.ReporteTS.push(s);
                    this.contGlobal++;
                    //   console.log(this.ambito.TS);
                } else {
                    this.Errores.push({
                        error: {
                            No: this.Errores.length,
                            description: "Ya existe la variable " + s.id.toLowerCase(),
                            linea: 0,
                            columna: 0,
                            tipo: "Semantico"
                        }
                    })
                }
            }


        });


    }

    AddVarToFunction(funcion, variable) {
        let a = false;
        this.Funciones.forEach(F => {
            if (F.Nombre[0].toLowerCase() == funcion.toLowerCase()) {
                let existe = false;
                F.variables.forEach(varia => {
                    if (varia.toLowerCase() == variable.id.toLowerCase()) {
                        return false;
                    }
                });
                if (!existe) {
                    F.variables.push(variable);
                    F.Tamano++;
                    a = true;
                    return true;
                }
            }
        });
        if (a) {
            return true;
        }
        return false;
    }


    get_posicion(ambito, name) {
        let ex = false;
        let pos = 0;
        ambito.TS.forEach(simbolo => {
            if (simbolo.id.toLowerCase() == name.toLowerCase()) {
                ex = true;
                pos = simbolo.posicion;
                return simbolo.posicion;
            }
        });
        if (ex) {
            return pos;
        }
        if (!ex && ambito.padre != null) {
            return get_posicion(ambito.padre, name);
        }
        return 0;
    }

    SimboloIsGlobal(ambito, name) {
        let ex = false;
        ambito.TS.forEach(simbolo => {
            if (simbolo.id.toLowerCase() == name.toLowerCase() && simbolo.ambito == "global") {
                ex = true;
                return true;
            }
        });

        if (ex) {
            return true;
        }
        if (!ex && ambito.padre != null) {
            return SimboloIsGlobal(ambito.padre, name);
        }

        return false;
    }

    Declaracion_T1_Local(actual) {
        let cont = 0;
        let salida = "";
        actual.Nombre.forEach(simbolo => {
            //LLENAR PARA TABLA DE SIMBOLOS
            let s = new Simbolo();
            s.id = simbolo.toLowerCase();
            s.Tamano = 1;


            if (actual.Tipo.hasOwnProperty('Corchetes')) {
                s.Tipo = "Arreglo";
                s.TipoArreglo = actual.Tipo.Tipo;
                actual.Tipo.Corchetes.forEach(e => {
                    s.cantidad_dimensiones++;
                });
            } else {
                s.Tipo = actual.Tipo.Tipo;
            }
            s.ambito = "local";
            s.rol = "variable";



            s.valor = actual.Valor;

            if (!this.SimboloExist_Local(this.ambito, s.id.toLowerCase())) {

                let T1 = this.getTemporal();
                this.EscribirC3D(T1 + " = P + " + this.ambito.Tam_actual + ";");
                //                this.EscribirC3D(Tpos + " = " + T1 + " + " + 1 + ";");

                let r = this.evaluar_expresion(actual.Valor);
                s.BooleanPrimitive = r.BooleanPrimitive;
                if (r.Tipo == "boolean" && r.Temporal == "" && r.BooleanPrimitive == false) {
                    let codi = r.Codigo + "\n";
                    let Tb = this.getTemporal();
                    let LS = this.getLabel();
                    r.EtiquetasV.forEach(ev => {
                        codi += ev + ":\n";
                    });

                    codi += Tb + " = 1;\n";
                    codi += "goto " + LS + ";\n"
                    r.EtiquetasF.forEach(ef => {
                        codi += ef + ":\n";
                    });
                    codi += Tb + " = 0;\n";
                    codi += LS + ":\n"
                    r.Codigo = codi;
                    r.Temporal = Tb;
                    r.BooleanPrimitive = true;
                }
                this.EscribirC3D(r.Codigo);
                if (r.Tipo == "string") {
                    this.EscribirC3D("Heap[H] = -1;");
                    this.EscribirC3D("H = H + 1;");
                }
                this.EscribirC3D("Stack[" + T1 + "] = " + r.Temporal + ";");
                s.posicion = this.ambito.Tam_actual;
                this.ambito.Tam_actual++;

                this.ambito.TS.push(s);
                this.ReporteTS.push(s);

            } else {
                this.Errores.push({
                    error: {
                        No: this.Errores.length,
                        description: "Declaracion " + s.id.toLowerCase(),
                        linea: 0,
                        columna: 0,
                        tipo: "Semantico"
                    }
                })
            }




        });
    }
    Declaracion_T2_Local(actual) {
        //LLENAR PARA TABLA DE SIMBOLOS
        let s = new Simbolo();
        s.id = actual.Nombre.toLowerCase();
        s.Tamano = 1;

        let Tpos = this.getTemporal();

        s.rol = "var";
        s.valor = actual.Valor;
        s.ambito = "local";
        if (!this.SimboloExist_Local(this.ambito, s.id.toLowerCase())) {

            let T1 = this.getTemporal();
            this.EscribirC3D(T1 + " = P + " + this.ambito.Tam_actual + ";");


            let r = this.evaluar_expresion(actual.Valor);
            if (r != null) {
                if (r.Tipo == "Arreglo") {
                    s.Tipo = "Arreglo";
                    s.TipoArreglo = r.TipoArreglo;

                }
                s.BooleanPrimitive = r.BooleanPrimitive;
                if (r.Tipo == "boolean" && r.Temporal == "" && r.BooleanPrimitive == false) {
                    let codi = r.Codigo + "\n";
                    let Tb = this.getTemporal();
                    let LS = this.getLabel();
                    r.EtiquetasV.forEach(ev => {
                        codi += ev + ":\n";
                    });

                    codi += Tb + " = 1;\n";
                    codi += "goto " + LS + ";\n"
                    r.EtiquetasF.forEach(ef => {
                        codi += ef + ":\n";
                    });
                    codi += Tb + " = 0;\n";
                    codi += LS + ":\n"
                    r.Codigo = codi;
                    r.Temporal = Tb;
                    r.BooleanPrimitive = true;
                }
                this.EscribirC3D(r.Codigo);
                if (r.Tipo == "string") {
                    this.EscribirC3D("Heap[H] = -1;");
                    this.EscribirC3D("H = H + 1;");
                }
                this.EscribirC3D("Stack[" + T1 + "] = " + r.Temporal + ";");
                s.posicion = this.ambito.Tam_actual;
                s.Tipo = r.Tipo;
                this.ambito.Tam_actual++;
                this.ambito.TS.push(s);
                this.ReporteTS.push(s);


            } else {
                this.Errores.push({
                    error: {
                        No: this.Errores.length,
                        description: "valor es null",
                        linea: 0,
                        columna: 0,
                        tipo: "Semantico"
                    }
                })
            }

        } else {
            this.Errores.push({
                error: {
                    No: this.Errores.length,
                    description: "Ya existe la variable " + s.id.toLowerCase(),
                    linea: 0,
                    columna: 0,
                    tipo: "Semantico"
                }
            })
        }
    }


    Declaracion_T2(actual) {
        let s = new Simbolo();
        s.id = actual.Nombre.toLowerCase();
        s.Tamano = 1;
        s.Tipo = this.getTipoVariable(actual.Valor);
        if (s.Tipo == "boolean") {
            s.BooleanPrimitive = true;
        }
        s.rol = "var";
        s.posicion = this.contGlobal;
        if (this.primerapasada) {
            s.valor = actual.Valor;
        } else {
            s.valor = 0;
        }
        if (!this.SimboloExist(this.ambito, s.id.toLowerCase())) {
            this.ambito.TS.push(s);
            this.ReporteTS.push(s);
            this.contGlobal++;

            //   console.log(this.ambito.TS);
        } else {
            this.Errores.push({
                error: {
                    No: this.Errores.length,
                    description: "Ya existe la variable " + s.id.toLowerCase(),
                    linea: 0,
                    columna: 0,
                    tipo: "Semantico"
                }
            })
        }
    }
    Declaracion_T3_Local(actual) {
        //LLENAR PARA TABLA DE SIMBOLOS

        let s = new Simbolo();
        s.id = actual.Nombre.toLowerCase();
        s.Tamano = 1;
        s.Tipo = this.getTipoVariable(actual.Valor);
        s.rol = "constante";
        let Tpos = this.getTemporal();
        s.posicion = Tpos;
        s.valor = actual.Valor;
        s.ambito = "local";
        if (!this.SimboloExist_Local(this.ambito, s.id.toLowerCase())) {
            let T1 = this.getTemporal();
            this.EscribirC3D(T1 + " = P + " + this.ambito.Tam_actual + ";");
            this.EscribirC3D(Tpos + " = " + T1 + " + " + 1 + ";");

            let r = this.evaluar_expresion(actual.Valor);
            s.BooleanPrimitive = r.BooleanPrimitive;

            if (r.Tipo == "boolean" && r.Temporal == "" && r.BooleanPrimitive == false) {
                let codi = r.Codigo + "\n";
                let Tb = this.getTemporal();
                let LS = this.getLabel();
                r.EtiquetasV.forEach(ev => {
                    codi += ev + ":\n";
                });

                codi += Tb + " = 1;\n";
                codi += "goto " + LS + ";\n"
                r.EtiquetasF.forEach(ef => {
                    codi += ef + ":\n";
                });
                codi += Tb + " = 0;\n";
                codi += LS + ":\n"
                r.Codigo = codi;
                r.Temporal = Tb;
                r.BooleanPrimitive = true;
            }

            if (r.Tipo == "Arreglo") {
                s.Tipo = "Arreglo";
                s.TipoArreglo = r.TipoArreglo;

            }
            this.EscribirC3D(r.Codigo);
            if (r.Tipo == "string") {
                this.EscribirC3D("Heap[H] = -1;");
                this.EscribirC3D("H = H + 1;");
            }
            s.Tipo = r.Tipo;
            s.TipoArreglo = r.TipoArreglo;
            this.EscribirC3D("Stack[" + Tpos + "] = " + r.Temporal + ";");
            this.ambito.Tam_actual++;

            this.ambito.TS.push(s);
            this.ReporteTS.push(s);


            //   console.log(this.ambito.TS);
        } else {
            this.Errores.push({
                error: {
                    No: this.Errores.length,
                    description: "Ya existe la variable " + s.id.toLowerCase(),
                    linea: 0,
                    columna: 0,
                    tipo: "Semantico"
                }
            })
        }
    }
    Declaracion_T3(actual) {
        let s = new Simbolo();
        s.id = actual.Nombre.toLowerCase();
        s.Tamano = 1;
        s.Tipo = this.getTipoVariable(actual.Valor);
        s.rol = "constante";
        s.posicion = this.contGlobal;
        if (s.Tipo == "boolean") {
            s.BooleanPrimitive = true;
        }
        if (this.primerapasada) {
            s.valor = actual.Valor;
        } else {
            s.valor = 0;
        }
        if (!this.SimboloExist(this.ambito, s.id.toLowerCase())) {
            this.ambito.TS.push(s);
            this.ReporteTS.push(s);
            this.contGlobal++;
            //   console.log(this.ambito.TS);
        } else {
            this.Errores.push({
                error: {
                    No: this.Errores.length,
                    description: "Ya existe la variable " + s.id.toLowerCase(),
                    linea: 0,
                    columna: 0,
                    tipo: "Semantico"
                }
            })
        }

    }

    addToGlobal(ambito, s) {
        if (ambito.padre != null) {
            this.addToGlobal(ambito.padre, s);
        } else {
            let exi = false;
            ambito.TS.map(i => {
                if (i.id.toLowerCase() == s.id.toLowerCase()) {
                    exi = true;
                }
            });
            if (!exi) {
                s.posicion = ambito.TS.length;
                let T1 = this.getTemporal();
                this.EscribirC3D(T1 + " = 0 + " + s.posicion + ";");
                let r = this.evaluar_expresion(s.valor);
                s.BooleanPrimitive = r.BooleanPrimitive;
                ambito.TS.push(s);
                this.ReporteTS.push(s);

                s.Tipo = r.Tipo;
                if (r.Tipo == "boolean" && r.Temporal == "" && r.BooleanPrimitive == false) {
                    let codi = r.Codigo + "\n";
                    let Tb = this.getTemporal();
                    let LS = this.getLabel();
                    r.EtiquetasV.forEach(ev => {
                        codi += ev + ":\n";
                    });

                    codi += Tb + " = 1;\n";
                    codi += "goto " + LS + ";\n"
                    r.EtiquetasF.forEach(ef => {
                        codi += ef + ":\n";
                    });
                    codi += Tb + " = 0;\n";
                    codi += LS + ":\n"
                    r.Codigo = codi;
                    r.Temporal = Tb;
                    r.BooleanPrimitive = true;
                }
                //    console.log(this.contGlobal);
                this.EscribirC3D(r.Codigo);
                this.EscribirC3D("Heap[" + T1 + "] = " + r.Temporal + ";");

            } else {
                this.Errores.push({
                    error: {
                        No: this.Errores.length,
                        description: "Ya existe la variable " + s.id.toLowerCase(),
                        linea: 0,
                        columna: 0,
                        tipo: "Semantico"
                    }
                })
            }

        }
    }

    Declaracion_T4_Local(actual) {
        let s = new Simbolo();
        s.id = actual.Nombre.toLowerCase();
        s.Tamano = 1;
        s.Tipo = this.getTipoVariable(actual.Valor);
        if (s.Tipo == "boolean") {
            s.BooleanPrimitive = true;
        }
        s.rol = "var";
        s.posicion = 0;

        s.valor = actual.Valor;

        //console.log(this.SimboloExist(this.ambito, s.id));
        this.addToGlobal(this.ambito, s);

    }

    Declaracion_T4(actual) {
        this.contGlobal++;
    }

    new_ambito(ambito) {
        let new_ambito = new Ambito();
        new_ambito.padre = ambito;
        return new_ambito;
    }

    borrar_ambito(ambito) {
        return ambito.padre;
    }

    main(actual) {

        let b = false;
        let Funcion = null;
        this.Funciones.forEach(F => {

            if (F.Nombre.toLowerCase() == "principal") {
                b = true;
                Funcion = F;
            }
        });

        if (b) {

            let aux = "";
            this.LFinal = this.getLabel();
            this.CodigoNormal = false;
            this.Salida += this.ambito.Salida_Funciones;
            this.Salida += "proc principal begin\n";
            this.Salida += "P = P + " + 0 + ";\n";
            this.Salida += "call main_init;\n";


            this.setFunctionLabel("principal");
            this.ambito = this.new_ambito(this.ambito);
            this.ambito.funcion_actual = "principal";
            this.ambito.TempIni = this.Temporal + 1;

            this.ambito.Tam_actual++;

            this.Sentencia(Funcion.Sentencias);


            let output = this.ambito.Salida_Funciones;

            this.ambito = this.borrar_ambito(this.ambito);




            this.Salida += output + "\n";

            this.Salida += this.getFunctionLabel("principal") + ":\n";
            this.Salida += "P = P - 0;\n";
            this.Salida += "end\n";
            this.CodigoNormal = true;

        } else {
            this.Errores.push({
                error: {
                    No: this.Errores.length,
                    description: "No hay metodo principal",
                    linea: 0,
                    columna: 0,
                    tipo: "Semantico"
                }
            })
        }

    }

    main_init(actual) {
        this.EscribirC3D("proc main_init begin");
        this.EscribirC3D("#*");
        this.EscribirC3D("Reservamos el espacio en el Heap");
        this.EscribirC3D("  para las variables globales");
        this.EscribirC3D("*#\n");
        this.EscribirC3D("H =  H + " + this.contGlobal + ";");
        this.ambito.TS.forEach(element => {
            // console.log(element);

            if (element.rol == "variable" || element.rol == "var") {

                let Temp = this.getTemporal();
                let val = this.evaluar_expresion(element.valor);
                if (val.Tipo == "Arreglo") {
                    element.Tipo = "Arreglo";
                    element.TipoArreglo = val.TipoArreglo;

                }
                if (val != undefined) {
                    this.EscribirC3D(Temp + " = 0 +" + element.posicion + ";");
                    if (val.Tipo == "boolean" && val.Temporal == "" && val.BooleanPrimitive == false) {
                        let codi = val.Codigo + "\n";
                        let Tb = this.getTemporal();
                        let LS = this.getLabel();
                        val.EtiquetasV.forEach(ev => {
                            codi += ev + ":\n";
                        });

                        codi += Tb + " = 1;\n";
                        codi += "goto " + LS + ";\n"
                        val.EtiquetasF.forEach(ef => {
                            codi += ef + ":\n";
                        });
                        codi += Tb + " = 0;\n";
                        codi += LS + ":\n"
                        r.Codigo = codi;
                        r.Temporal = Tb;
                        r.BooleanPrimitive = true;
                    }
                    this.EscribirC3D(val.Codigo);
                    //console.log(val);
                    if (val.Tipo == "string") {
                        this.EscribirC3D("Heap[H] = -1;");
                        this.EscribirC3D("H = H + 1;");
                    }

                    /*if (element.Tipo != val.Tipo) {
                            if (element.Tipo == "double" && val.Tipo == "integer") {
                                let Td = this.getTemporal();
                                this.EscribirC3D(Td + "= " + val.Temporal + " * 1.0;");
                                val.Temporal = Td;
    
                            } else {
                                this.EscribirC3D("#* Error de tipo porque la variable es de ");
                                this.EscribirC3D("tipo: " + element.Tipo + " y el valor que se desea ");
                                this.EscribirC3D("asignar es : " + val.Tipo);
                                this.EscribirC3D("*#\n");
                                this.EscribirC3D("E = 5;\n");
                            }
    
                        }*/
                    this.EscribirC3D("Heap[" + Temp + "] = " + val.Temporal + ";");
                }




            } else if (element.rol == "constante") {
                let Temp = this.getTemporal();
                let val = this.evaluar_expresion(element.valor);
                if (val != undefined) {
                    if (val.Tipo == "Arreglo") {
                        element.Tipo = "Arreglo";
                        element.TipoArreglo = val.TipoArreglo;

                    }
                    this.EscribirC3D(Temp + " = 0 +" + element.posicion + ";");

                    this.EscribirC3D(val.Codigo);

                    if (val.Tipo == "string") {
                        this.EscribirC3D("Heap[H] = -1;");
                        this.EscribirC3D("H = H + 1;");
                    }
                    this.EscribirC3D("Heap[" + Temp + "] = " + val.Temporal + ";");
                }
            }
        });

        this.EscribirC3D("end");
    }

    Decremento(actual) {
        if (actual.hasOwnProperty('Posicion')) {
            let Nombre = actual.Nombre.toLowerCase();
            let Posicion = actual.Posicion;
            if (this.SimboloExist(this.ambito, Nombre)) {
                let s = this.getSimbolo(this.ambito, Nombre);
                let pos = this.evaluar_expresion(Posicion);

                if (s.rol != "constante") {
                    if (s != null) {
                        if (s.ambito == "global") {
                            let T1 = this.getTemporal();
                            let T2 = this.getTemporal();
                            let T3 = this.getTemporal();
                            let T4 = this.getTemporal();
                            let T5 = this.getTemporal();
                            let T6 = this.getTemporal();
                            let T7 = this.getTemporal();
                            let T8 = this.getTemporal();
                            let T9 = this.getTemporal();
                            let LV = this.getLabel();
                            let LF = this.getLabel();
                            let LV2 = this.getLabel();

                            let LS = this.getLabel();
                            this.EscribirC3D(T1 + " = 0 + " + s.posicion + ";");
                            this.EscribirC3D(pos.Codigo);
                            this.EscribirC3D(T2 + " = Heap[" + T1 + "];");
                            this.EscribirC3D(T3 + " = Heap[" + T2 + "];");
                            this.EscribirC3D("if(" + pos.Temporal + " >= 1) goto " + LV + ";");
                            this.EscribirC3D("goto " + LF + ";");
                            this.EscribirC3D(LV + ":");
                            this.EscribirC3D("if(" + pos.Temporal + " <= " + T3 + ") goto " + LV2 + ";");
                            this.EscribirC3D("goto " + LF + ";");
                            this.EscribirC3D(LV2 + ":");
                            this.EscribirC3D(T4 + " = " + T2 + " + 1" + ";");
                            this.EscribirC3D(T5 + " = " + pos.Temporal + " - 1;");
                            this.EscribirC3D(T6 + " = " + T4 + " + " + T5 + ";");
                            this.EscribirC3D(T7 + " = Heap[" + T6 + "];");
                            this.EscribirC3D(T9 + " = 1;");
                            this.EscribirC3D(T10 + " = " + T7 + " - " + T9 + ";");
                            this.EscribirC3D("Heap[" + T6 + "] = " + T10 + ";");
                            this.EscribirC3D("goto " + LS + ";");
                            this.EscribirC3D(LF + ":");
                            this.EscribirC3D("E = 2;");
                            this.EscribirC3D(LS + ":");
                        } else {
                            let T1 = this.getTemporal();
                            let T2 = this.getTemporal();
                            let T3 = this.getTemporal();
                            let T4 = this.getTemporal();
                            let T5 = this.getTemporal();
                            let T6 = this.getTemporal();
                            let T7 = this.getTemporal();
                            let T8 = this.getTemporal();
                            let T9 = this.getTemporal();
                            let T10 = this.getTemporal();
                            let LV = this.getLabel();
                            let LF = this.getLabel();
                            let LV2 = this.getLabel();
                            let LS = this.getLabel();
                            this.EscribirC3D(T1 + " = P + " + s.posicion + ";");
                            this.EscribirC3D(pos.Codigo);
                            this.EscribirC3D(T2 + " = Stack[" + T1 + "];");
                            this.EscribirC3D(T3 + " = Heap[" + T2 + "];");
                            this.EscribirC3D("if(" + pos.Temporal + " >= 1) goto " + LV + ";");
                            this.EscribirC3D("goto " + LF + ";");
                            this.EscribirC3D(LV + ":");
                            this.EscribirC3D("if(" + pos.Temporal + " <= " + T3 + ") goto " + LV2 + ";");
                            this.EscribirC3D("goto " + LF + ";");
                            this.EscribirC3D(LV2 + ":");
                            this.EscribirC3D(T4 + " = " + T2 + " + 1" + ";");
                            this.EscribirC3D(T5 + " = " + pos.Temporal + " - 1;");
                            this.EscribirC3D(T6 + " = " + T4 + " + " + T5 + ";");
                            this.EscribirC3D(T7 + " = Heap[" + T6 + "];");
                            this.EscribirC3D(T9 + " = 1;");
                            this.EscribirC3D(T10 + " = " + T7 + " - " + T9 + ";");
                            this.EscribirC3D("Heap[" + T6 + "] = " + valor.T10 + ";");
                            this.EscribirC3D("goto " + LS + ";");
                            this.EscribirC3D(LF + ":");
                            this.EscribirC3D(LF + ":");
                            this.EscribirC3D("E = 2;");
                            this.EscribirC3D(LS + ":");
                        }
                    }
                } else {
                    this.Errores.push({
                        error: {
                            No: this.Errores.length,
                            description: "No se puede asignar un valor a una constante",
                            linea: 0,
                            columna: 0,
                            tipo: "Semantico"
                        }
                    })
                }
            } else {
                this.Errores.push({
                    error: {
                        No: this.Errores.length,
                        description: "No existe la variable",
                        linea: 0,
                        columna: 0,
                        tipo: "Semantico"
                    }
                })
            }

        } else {
            let Nombre = actual.Nombre.toLowerCase();
            if (this.SimboloExist(this.ambito, Nombre)) {
                let s = this.getSimbolo(this.ambito, Nombre);
                if (s.rol != "constante") {
                    if (s != null) {
                        if (s.ambito == "global") {
                            let T1 = this.getTemporal();
                            let T2 = this.getTemporal();
                            let T3 = this.getTemporal();
                            let T4 = this.getTemporal();
                            this.EscribirC3D(T1 + " = 0 + " + s.posicion + ";");
                            this.EscribirC3D(T2 + " = Heap[" + T1 + "];");
                            this.EscribirC3D(T3 + " = 1;");
                            this.EscribirC3D(T4 + " = " + T2 + " - " + T3 + ";");
                            this.EscribirC3D("Heap[" + T1 + "] = " + T4 + ";");
                        } else {
                            let T1 = this.getTemporal();
                            let T2 = this.getTemporal();
                            let T3 = this.getTemporal();
                            let T4 = this.getTemporal();

                            this.EscribirC3D(T1 + " = P + " + s.posicion + ";");
                            this.EscribirC3D(T2 + " = Stack[" + T1 + "];");
                            this.EscribirC3D(T3 + " = 1;");
                            this.EscribirC3D(T4 + " = " + T2 + " - " + T3 + ";");

                            this.EscribirC3D("Stack[" + T1 + "] = " + T4 + ";");
                        }
                    }
                } else {
                    this.Errores.push({
                        error: {
                            No: this.Errores.length,
                            description: "No se puede cambiar el valor de una constante",
                            linea: 0,
                            columna: 0,
                            tipo: "Semantico"
                        }
                    })
                }
            } else {
                this.Errores.push({
                    error: {
                        No: this.Errores.length,
                        description: "No existe la variable",
                        linea: 0,
                        columna: 0,
                        tipo: "Semantico"
                    }
                })
            }

        }
    }

    Incremento(actual) {

        if (actual.hasOwnProperty('Posicion')) {
            let Nombre = actual.Nombre.toLowerCase();
            let Posicion = actual.Posicion;
            if (this.SimboloExist(this.ambito, Nombre)) {
                let s = this.getSimbolo(this.ambito, Nombre);
                let pos = this.evaluar_expresion(Posicion);

                if (s.rol != "constante") {
                    if (s != null) {
                        if (s.ambito == "global") {
                            let T1 = this.getTemporal();
                            let T2 = this.getTemporal();
                            let T3 = this.getTemporal();
                            let T4 = this.getTemporal();
                            let T5 = this.getTemporal();
                            let T6 = this.getTemporal();
                            let T7 = this.getTemporal();
                            let T8 = this.getTemporal();
                            let T9 = this.getTemporal();
                            let LV = this.getLabel();
                            let LF = this.getLabel();
                            let LV2 = this.getLabel();
                            let LF2 = this.getLabel();
                            let LS = this.getLabel();
                            this.EscribirC3D(T1 + " = 0 + " + s.posicion + ";");
                            this.EscribirC3D(pos.Codigo);
                            this.EscribirC3D(T2 + " = Heap[" + T1 + "];");
                            this.EscribirC3D(T3 + " = Heap[" + T2 + "];");
                            this.EscribirC3D("if(" + pos.Temporal + " >= 1) goto " + LV + ";");
                            this.EscribirC3D("goto " + LF + ";");
                            this.EscribirC3D(LV + ":");
                            this.EscribirC3D("if(" + pos.Temporal + " <= " + T3 + ") goto " + LV2 + ";");
                            this.EscribirC3D("goto " + LF + ";");
                            this.EscribirC3D(LV2 + ":");
                            this.EscribirC3D(T4 + " = " + T2 + " + 1" + ";");
                            this.EscribirC3D(T5 + " = " + pos.Temporal + " - 1;");
                            this.EscribirC3D(T6 + " = " + T4 + " + " + T5 + ";");
                            this.EscribirC3D(T7 + " = Heap[" + T6 + "];");
                            this.EscribirC3D(T9 + " = 1;");
                            this.EscribirC3D(T10 + " = " + T7 + " + " + T9 + ";");
                            this.EscribirC3D("Heap[" + T6 + "] = " + T10 + ";");
                            this.EscribirC3D("goto " + LS + ";");
                            this.EscribirC3D(LF + ":");
                            this.EscribirC3D("E = 2;");
                            this.EscribirC3D(LS + ":");
                        } else {
                            let T1 = this.getTemporal();
                            let T2 = this.getTemporal();
                            let T3 = this.getTemporal();
                            let T4 = this.getTemporal();
                            let T5 = this.getTemporal();
                            let T6 = this.getTemporal();
                            let T7 = this.getTemporal();
                            let T9 = this.getTemporal();
                            let T10 = this.getTemporal();
                            let LV = this.getLabel();
                            let LF = this.getLabel();
                            let LV2 = this.getLabel();
                            let LS = this.getLabel();
                            this.EscribirC3D(T1 + " = P +" + s.posicion + ";");
                            this.EscribirC3D(pos.Codigo);
                            this.EscribirC3D(T2 + " = Stack[" + T1 + "];");
                            this.EscribirC3D(T3 + " = Heap[" + T2 + "];");
                            this.EscribirC3D("if(" + pos.Temporal + " >= 1) goto " + LV + ";");
                            this.EscribirC3D("goto " + LF + ";");
                            this.EscribirC3D(LV + ":");
                            this.EscribirC3D("if(" + pos.Temporal + " <= " + T3 + ") goto " + LV2 + ";");
                            this.EscribirC3D("goto " + LF + ";");
                            this.EscribirC3D(LV2 + ":");
                            this.EscribirC3D(T4 + " = " + T2 + " + 1" + ";");
                            this.EscribirC3D(T5 + " = " + pos.Temporal + " - 1;");
                            this.EscribirC3D(T6 + " = " + T4 + " + " + T5 + ";");
                            this.EscribirC3D(T7 + " = Heap[" + T6 + "];");
                            this.EscribirC3D(T9 + " = 1;");
                            this.EscribirC3D(T10 + " = " + T7 + " + " + T9 + ";");
                            this.EscribirC3D("Heap[" + T6 + "] = " + T10 + ";");
                            this.EscribirC3D("goto " + LS + ";");
                            this.EscribirC3D(LF + ":");

                            this.EscribirC3D("E = 2;");
                            this.EscribirC3D(LS + ":");
                        }
                    } else {
                        this.Errores.push({
                            error: {
                                No: this.Errores.length,
                                description: "valor es null",
                                linea: 0,
                                columna: 0,
                                tipo: "Semantico"
                            }
                        })
                    }
                } else {
                    this.Errores.push({
                        error: {
                            No: this.Errores.length,
                            description: "No se puede cambiar el valor de una constante",
                            linea: 0,
                            columna: 0,
                            tipo: "Semantico"
                        }
                    })
                }
            } else {
                this.Errores.push({
                    error: {
                        No: this.Errores.length,
                        description: "No existe la variable",
                        linea: 0,
                        columna: 0,
                        tipo: "Semantico"
                    }
                })
            }

        } else {
            let Nombre = actual.Nombre.toLowerCase();
            if (this.SimboloExist(this.ambito, Nombre)) {
                let s = this.getSimbolo(this.ambito, Nombre);
                if (s.rol != "constante") {
                    if (s != null) {
                        if (s.ambito == "global") {
                            let T1 = this.getTemporal();
                            let T2 = this.getTemporal();
                            let T3 = this.getTemporal();
                            let T4 = this.getTemporal();
                            this.EscribirC3D(T1 + " = 0 + " + s.posicion + ";");
                            this.EscribirC3D(T2 + " = Heap[" + T1 + "];");
                            this.EscribirC3D(T3 + " = 1;");
                            this.EscribirC3D(T4 + " = " + T2 + " + " + T3 + ";");
                            this.EscribirC3D("Heap[" + T1 + "] = " + T4 + ";");
                        } else {
                            let T1 = this.getTemporal();
                            let T2 = this.getTemporal();
                            let T3 = this.getTemporal();
                            let T4 = this.getTemporal();

                            this.EscribirC3D(T1 + " = P + " + s.posicion + ";");
                            this.EscribirC3D(T2 + " = Stack[" + T1 + "];");
                            this.EscribirC3D(T3 + " = 1;");
                            this.EscribirC3D(T4 + " = " + T2 + " + " + T3 + ";");

                            this.EscribirC3D("Stack[" + T1 + "] = " + T4 + ";");
                        }
                    }
                } else {
                    this.Errores.push({
                        error: {
                            No: this.Errores.length,
                            description: "No se puede cambiar el valor de una constante",
                            linea: 0,
                            columna: 0,
                            tipo: "Semantico"
                        }
                    })
                }
            } else {
                this.Errores.push({
                    error: {
                        No: this.Errores.length,
                        description: "No existe la variable",
                        linea: 0,
                        columna: 0,
                        tipo: "Semantico"
                    }
                })
            }


        }
    }

    Asignacion(actual) {


        if (actual.Nombre.hasOwnProperty('Posicion')) {
            let Nombre = actual.Nombre.Nombre.toLowerCase();
            let Posicion = actual.Nombre.Posicion;
            let valor = this.evaluar_expresion(actual.Valor);

            if (this.SimboloExist(this.ambito, Nombre)) {
                let s = this.getSimbolo(this.ambito, Nombre);
                let pos = this.evaluar_expresion(Posicion);

                if (valor.Tipo == "boolean" && valor.Temporal == "" && valor.BooleanPrimitive == false) {
                    let codi = valor.Codigo + "\n";
                    let Tb = this.getTemporal();
                    let LS = this.getLabel();
                    valor.EtiquetasV.forEach(ev => {
                        codi += ev + ":\n";
                    });

                    codi += Tb + " = 1;\n";
                    codi += "goto " + LS + ";\n"
                    valor.EtiquetasF.forEach(ef => {
                        codi += ef + ":\n";
                    });
                    codi += Tb + " = 0;\n";
                    codi += LS + ":\n"
                    valor.Codigo = codi;
                    valor.Temporal = Tb;
                    valor.BooleanPrimitive = true;
                }

                if (s.rol != "constante") {
                    if (s != null) {
                        if (s.ambito == "global") {
                            let T1 = this.getTemporal();
                            let T2 = this.getTemporal();
                            let T3 = this.getTemporal();
                            let T4 = this.getTemporal();
                            let T5 = this.getTemporal();
                            let T6 = this.getTemporal();
                            let LV = this.getLabel();
                            let LF = this.getLabel();
                            let LV2 = this.getLabel();
                            let LF2 = this.getLabel();
                            let LS = this.getLabel();

                            this.EscribirC3D(pos.Codigo);
                            this.EscribirC3D(T1 + " = 0 + " + s.posicion + ";");
                            this.EscribirC3D(T2 + " = Heap[" + T1 + "];");
                            this.EscribirC3D(T3 + " = Heap[" + T2 + "];");
                            this.EscribirC3D("if(" + pos.Temporal + " >= 1) goto " + LV + ";");
                            this.EscribirC3D("goto " + LF + ";");
                            this.EscribirC3D(LV + ":");
                            this.EscribirC3D("if(" + pos.Temporal + " <= " + T3 + ") goto " + LV2 + ";");
                            this.EscribirC3D("goto " + LF + ";");
                            this.EscribirC3D(LV2 + ":");
                            this.EscribirC3D(T4 + " = " + T2 + " + 1" + ";");
                            this.EscribirC3D(T5 + " = " + pos.Temporal + " - 1;");
                            this.EscribirC3D(T6 + " = " + T4 + " + " + T5 + ";");
                            this.EscribirC3D(valor.Codigo);
                            if (valor.Tipo == "string") {
                                this.EscribirC3D("Heap[H] = -1;");
                                this.EscribirC3D("H = H + 1;");
                            }
                            this.EscribirC3D("Heap[" + T6 + "] = " + valor.Temporal + ";");
                            this.EscribirC3D("goto " + LS + ";");
                            this.EscribirC3D(LF + ":");
                            this.EscribirC3D("E = 2;");
                            this.EscribirC3D(LS + ":");
                        } else {
                            let T1 = this.getTemporal();
                            let T2 = this.getTemporal();
                            let T3 = this.getTemporal();
                            let T4 = this.getTemporal();
                            let T5 = this.getTemporal();
                            let T6 = this.getTemporal();
                            let LV = this.getLabel();
                            let LF = this.getLabel();
                            let LV2 = this.getLabel();
                            let LS = this.getLabel();

                            this.EscribirC3D(pos.Codigo);
                            this.EscribirC3D(T1 + " = P +" + s.posicion + ";");
                            this.EscribirC3D(T2 + " = Stack[" + T1 + "];");
                            this.EscribirC3D(T3 + " = Heap[" + T2 + "];");
                            this.EscribirC3D("if(" + pos.Temporal + " >= 1) goto " + LV + ";");
                            this.EscribirC3D("goto " + LF + ";");
                            this.EscribirC3D(LV + ":");
                            this.EscribirC3D("if(" + pos.Temporal + " <= " + T3 + ") goto " + LV2 + ";");
                            this.EscribirC3D("goto " + LF + ";");
                            this.EscribirC3D(LV2 + ":");
                            this.EscribirC3D(T4 + " = " + T2 + " + 1" + ";");
                            this.EscribirC3D(T5 + " = " + pos.Temporal + " - 1;");
                            this.EscribirC3D(T6 + " = " + T4 + " + " + T5 + ";");
                            this.EscribirC3D(valor.Codigo);
                            if (valor.Tipo == "string") {
                                this.EscribirC3D("Heap[H] = -1;");
                                this.EscribirC3D("H = H + 1;");
                            }
                            this.EscribirC3D("Heap[" + T6 + "] = " + valor.Temporal + ";");
                            this.EscribirC3D("goto " + LS + ";");
                            this.EscribirC3D(LF + ":");
                            this.EscribirC3D("E = 2;");
                            this.EscribirC3D(LS + ":");
                        }
                    } else {
                        this.Errores.push({
                            error: {
                                No: this.Errores.length,
                                description: "El valor es null",
                                linea: 0,
                                columna: 0,
                                tipo: "Semantico"
                            }
                        })
                    }
                } else {
                    this.Errores.push({
                        error: {
                            No: this.Errores.length,
                            description: "No se puede asignar un valor a un constante",
                            linea: 0,
                            columna: 0,
                            tipo: "Semantico"
                        }
                    })
                }
            } else {
                this.Errores.push({
                    error: {
                        No: this.Errores.length,
                        description: "No existe la variable",
                        linea: 0,
                        columna: 0,
                        tipo: "Semantico"
                    }
                })
            }

        } else {
            let Nombre = actual.Nombre.Nombre.toLowerCase();
            let valor = this.evaluar_expresion(actual.Valor);

            if (this.SimboloExist(this.ambito, Nombre)) {
                let s = this.getSimbolo(this.ambito, Nombre);

                if (valor.Tipo == "boolean" && valor.Temporal == "") {
                    let codi = valor.Codigo + "\n";
                    let Tb = this.getTemporal();
                    let LS = this.getLabel();
                    valor.EtiquetasV.forEach(ev => {
                        codi += ev + ":\n";
                    });

                    codi += Tb + " = 1;\n";
                    codi += "goto " + LS + ";\n"
                    valor.EtiquetasF.forEach(ef => {
                        codi += ef + ":\n";
                    });
                    codi += Tb + " = 0;\n";
                    codi += LS + ":\n"
                    valor.Codigo = codi;
                    valor.Temporal = Tb;
                    valor.BooleanPrimitive = true;
                }

                if (s.rol != "constante") {
                    if (s != null) {
                        if (s.ambito == "global") {
                            let T1 = this.getTemporal();

                            this.EscribirC3D(valor.Codigo);
                            this.EscribirC3D(T1 + " = 0 + " + s.posicion + ";");
                            if (valor.Tipo == "string") {
                                this.EscribirC3D("Heap[H] = -1;");
                                this.EscribirC3D("H = H + 1;");
                            }
                            this.EscribirC3D("Heap[" + T1 + "] = " + valor.Temporal + ";");
                        } else {
                            let T1 = this.getTemporal();
                            let T2 = this.getTemporal();

                            this.EscribirC3D(valor.Codigo);
                            this.EscribirC3D(T1 + " = P +" + s.posicion + ";");
                            this.EscribirC3D(T2 + " = " + T1 + ";");
                            if (valor.Tipo == "string") {
                                this.EscribirC3D("Heap[H] = -1;");
                                this.EscribirC3D("H = H + 1;");
                            }
                            this.EscribirC3D("Stack[" + T1 + "] = " + valor.Temporal + ";");
                        }
                    }
                } else {
                    this.Errores.push({
                        error: {
                            No: this.Errores.length,
                            description: "Asignacion a constante",
                            linea: 0,
                            columna: 0,
                            tipo: "Semantico"
                        }
                    })
                }
            } else {
                this.Errores.push({
                    error: {
                        No: this.Errores.length,
                        description: "No existe la variable",
                        linea: 0,
                        columna: 0,
                        tipo: "Semantico"
                    }
                })
            }


        }
    }




    Sentencia(actual) {
        actual.forEach(s => {

            if (s.hasOwnProperty('If')) {
                this.IF(s.If);
            } else if (s.hasOwnProperty('Print')) {
                this.Print(s.Print);
            } else if (s.hasOwnProperty('While')) {
                this.While(s.While);
            } else if (s.hasOwnProperty('Switch')) {
                this.Switch(s.Switch);
            } else if (s.hasOwnProperty('DoWhile')) {
                this.Do_While(s.DoWhile);
            } else if (s.hasOwnProperty('For')) {
                this.FOR(s.For);
            } else if (s.hasOwnProperty('Declaracion_Tipo1')) {
                //    console.log("declaracion t 1");
                this.Declaracion_T1_Local(s.Declaracion_Tipo1);
            } else if (s.hasOwnProperty('Declaracion_Tipo2')) {
                //    console.log("declaracion t 1");
                this.Declaracion_T2_Local(s.Declaracion_Tipo2);
            } else if (s.hasOwnProperty('Declaracion_Tipo3')) {
                //    console.log("declaracion t 1");
                this.Declaracion_T3_Local(s.Declaracion_Tipo3);
            } else if (s.hasOwnProperty('Declaracion_Tipo4')) {
                //    console.log("declaracion t 1");
                this.Declaracion_T4_Local(s.Declaracion_Tipo4);
            } else if (s.hasOwnProperty('Asignacion')) {
                this.Asignacion(s.Asignacion);
            } else if (s.hasOwnProperty('Incremento')) {
                this.Incremento(s.Incremento);
            } else if (s.hasOwnProperty('Decremento')) {
                this.Decremento(s.Decremento);
            } else if (s.hasOwnProperty('Llamada_Funcion')) {
                this.Llamada_Funcion(s.Llamada_Funcion);
            } else if (s.hasOwnProperty('Break')) {
                //     console.log(this.TipoCiclo + ", " + this.LFT + ", " + this.LVT);
                if (this.TipoCiclo == "switch" && this.LFT != "" && this.LVT == "") {
                    this.EscribirC3D("goto " + this.LFT + ";");
                } else if (this.TipoCiclo != "" && this.LFT != "" && this.LVT != "") {
                    this.EscribirC3D("goto " + this.LFT + ";");
                } else {
                    //marcar error
                }
            } else if (s.hasOwnProperty('Continue')) {
                if (this.TipoCiclo != "" && this.LFT != "" && this.LVT != "" && this.TipoCiclo != "switch") {
                    this.EscribirC3D("goto " + this.LVT + ";");
                } else {
                    //marcar error
                }
            } else if (s.hasOwnProperty('Return')) {
                let r = this.evaluar_expresion(s.Return.Valor);

                this.EscribirC3D(r.Codigo);
                let T1 = this.getTemporal();
                this.EscribirC3D(T1 + " = P + " + 0 + ";");
                //console.log(r);
                //this.EscribirC3D("print( \"%i\"," + r.Temporal + ");");
                this.EscribirC3D("Stack[" + T1 + "] = " + r.Temporal + ";");
                this.EscribirC3D("goto " + this.getFunctionLabel(this.ambito.funcion_actual) + ";");

            } else {

                /*this.Errores.push({
                    error: {
                        No: this.Errores.length,
                        description: "sentencia desconocida",
                        linea: 0,
                        columna: 0,
                        tipo: "Semantico"
                    }
                })*/

            }
        });
    }

    Llamada_Funcion(actual) {
        try {
            let Nombre = actual.Nombre.toLowerCase();
            let res = "";
            let listaPar = [];
            let output = "";
            let TipoF = "";
            actual.Parametros.forEach(p => {
                let r = this.evaluar_expresion(p);
                listaPar.push(r);
                Nombre += "_" + r.Tipo;
            });
            if (this.Function_exist(Nombre)) {
                let f = this.getFunction(Nombre);
                TipoF = f.Tipo;
                let tam = f.Parametros.length;
                let Tamano_actual = this.ambito.Tam_actual;
                let Tamano_pasado = this.ambito.Tam_actual;
                this.ambito.TempActual = this.Temporal;
                let res_temps = "";
                let Tcont = this.getTemporal();
                let TposIniStack = this.getTemporal();
                output += "# Carga de Temporales al Stack\n";
                res_temps += Tcont + " = " + 0 + ";\n";
                res_temps += TposIniStack + " = P + " + Tamano_actual + ";\n";

                let Tpos = this.getTemporal();
                res_temps += Tpos + " = " + TposIniStack + " + " + Tcont + ";\n";

                for (let i = this.ambito.TempIni; i <= this.ambito.TempActual; i++) {


                    res_temps += "Stack[" + Tpos + "] = T" + i + ";\n";
                    res_temps += Tpos + " = " + Tpos + " + " + 1 + ";\n";
                    Tamano_actual++;

                }

                output += res_temps;
                output += "# Termina Carga de Temporales al Stack\n";


                let Treturn = this.getTemporal();

                this.ambito = this.new_ambito(this.ambito);

                this.ambito.funcion_actual = Nombre;
                this.ambito.Tam_actual++;
                if (f.Codigo == false) {
                    this.setFunctionLabel(Nombre);
                }
                this.ambito.Return = Treturn;

                this.ambito.TempIni = this.Temporal + 1;
                output += "# Cambio Simulado de Ambito\n";
                for (let i = 0; i < listaPar.length; i++) {

                    let T1 = this.getTemporal();
                    let T2 = this.getTemporal();

                    let s = new Simbolo();
                    s.id = f.Parametros[i].Nombre.toLowerCase();
                    if (f.Parametros[i].Tipo.hasOwnProperty('Corchetes')) {
                        s.Tipo = "Arreglo";
                        s.TipoArreglo = f.Parametros[i].Tipo.Tipo;
                        f.Parametros[i].Tipo.Corchetes.forEach(e => {
                            s.cantidad_dimensiones++;
                        });
                    } else {
                        s.Tipo = f.Parametros[i].Tipo.Tipo;
                    }
                    s.rol = "parametro";
                    s.ambito = "local";
                    if (!this.SimboloExist_Local(this.ambito, s.id.toLowerCase())) {

                        //output += T1 + " = P + " + this.ambito.padre.Tam_actual + ";\n";
                        output += T1 + " = P + " + Tamano_actual + ";\n";
                        output += T2 + " = " + T1 + " + " + this.ambito.Tam_actual + ";\n";
                        s.posicion = this.ambito.Tam_actual;
                        output += listaPar[i].Codigo + "\n";
                        this.ambito.Tam_actual++;
                        output += "Stack[" + T2 + "] = " + listaPar[i].Temporal + ";\n";
                        this.ambito.TS.push(s);
                    } else {
                        this.Errores.push({
                            error: {
                                No: this.Errores.length,
                                description: "Ya existe la variable",
                                linea: 0,
                                columna: 0,
                                tipo: "Semantico"
                            }
                        })
                    }
                }

                output += "# Cambio Formal de Ambito\n";
                //output += "P = P +" + this.ambito.padre.Tam_actual + ";\n";
                output += "P = P +" + Tamano_actual + ";\n";
                output += Treturn + " = P;\n";
                this.ambito.Tam_actual = listaPar.length + 1;
                // agregar parametros al nuevo ambito

                if (f.Codigo == false) {
                    this.setFunctionCodigoTrue(f.Nombre.toLowerCase());
                    this.Sentencia(f.Sentencias);
                    let a = "";
                    a += "proc " + f.Nombre.toLowerCase() + " begin\n";

                    a += this.ambito.Salida_Funciones + "\n";
                    a += this.getFunctionLabel(Nombre) + ":\n";
                    a += "end" + "\n";

                    this.addCodigoToFuncion(Nombre, a);

                }
                output += "call " + f.Nombre.toLowerCase() + ";\n";

                let TF = this.getTemporal();


                let TFpos = this.getTemporal();
                output += TFpos + " =  P + " + 0 + ";\n"
                output += TF + " = Stack[" + TFpos + "];\n";
                //console.log(this.ambito.padre.Tam_actual);



                this.ambito = this.borrar_ambito(this.ambito);

                //output += "P = P - " + this.ambito.Tam_actual + ";\n";
                output += "P = P - " + Tamano_actual + ";\n";
                res_temps = "";
                Tcont = this.getTemporal();
                TposIniStack = this.getTemporal();
                output += "# Sacar de Stack a Temporales\n";
                res_temps += Tcont + " = " + 0 + ";\n";
                res_temps += TposIniStack + " = P + " + Tamano_pasado + ";\n";
                Tpos = this.getTemporal();

                res_temps += Tpos + " = " + TposIniStack + " + " + Tcont + ";\n";


                for (let i = this.ambito.TempIni; i < this.ambito.TempActual; i++) {

                    res_temps += "T" + i + " = Stack[" + Tpos + "];\n";

                    res_temps += Tpos + " = " + Tpos + " + " + 1 + ";\n";
                }
                output += res_temps;
                output += "# Termina Sacar del Stack a Tepmorales\n";

                this.EscribirC3D(output);


            }
        } catch (e) {

            this.Errores.push({
                error: {
                    No: this.Errores.length,
                    description: e,
                    linea: 0,
                    columna: 0,
                    tipo: "Semantico"
                }
            })

        }

    }

    addCodigoToFuncion(name, code) {
        let t = false;

        this.Funciones.forEach(f => {
            if (f.Nombre.toLowerCase() == name.toLowerCase()) {
                f.Salida_Funcion = code;
            }
        });
    }

    getCodigoFunciones() {
        let code = "";
        this.Funciones.forEach(f => {
            if (f.Salida_Funcion != undefined) {
                code += f.Salida_Funcion;
            }

        });
        return code;
    }

    Function_exist(name) {
        let t = false;
        this.Funciones.forEach(f => {
            if (f.Nombre.toLowerCase() == name.toLowerCase()) {
                t = true;
                return true;
            }
        });
        return t;
    }

    getFunction(name) {
        let res = null;
        this.Funciones.forEach(f => {
            if (f.Nombre.toLowerCase() == name.toLowerCase()) {
                res = f;
                return f;
            }
        });
        return res;
    }

    setFunctionCodigoTrue(name) {
        let res = null;
        this.Funciones.forEach(f => {

            if (f.Nombre.toLowerCase() == name.toLowerCase()) {
                f.Codigo = true;
            }
        });
    }
    setFunctionLabel(name) {
        let res = null;
        this.Funciones.forEach(f => {

            if (f.Nombre.toLowerCase() == name.toLowerCase()) {
                f.LSalida = this.getLabel();
            }
        });
    }
    getFunctionLabel(name) {
        let res = "";
        this.Funciones.forEach(f => {

            if (f.Nombre.toLowerCase() == name.toLowerCase()) {
                res = f.LSalida;
            }
        });
        return res;
    }

    getSimbolo(ambito, name) {
        let ex = false;
        let valor = null;
        ambito.TS.forEach(simbolo => {
            if (simbolo.id.toLowerCase() == name.toLowerCase()) {
                valor = simbolo;
                ex = true;
                return simbolo;
            }
        });
        //  console.log(valor)
        if (ex) {
            return valor;
        }

        if (!ex && ambito.padre != null) {
            return this.getSimbolo(ambito.padre, name);
        }
        return null;
    }


    SimboloExist_Local(ambito, entry) {
        let siexiste = false;
        ambito.TS.forEach(s => {
            //   console.log(s.id + "<==>" + entry);
            if (s.id.toLowerCase() === entry.toLowerCase()) {

                siexiste = true;
                return true;
            }
        });

        if (siexiste) return true;
        return false;
    }

    SimboloExist(ambito, entry) {
        let siexiste = false;
        ambito.TS.forEach(s => {

            if (s.id.toLowerCase() === entry.toLowerCase()) {

                siexiste = true;
                return true;
            }
        });

        if (siexiste) return true;
        if (ambito.padre != null) {
            return this.SimboloExist(ambito.padre, entry);
        }


        return false;
    }



    AddVariablesToC3D() {
        let vars = "";
        vars += "var Stack[]; \n" +
            "var Heap[]; \n" +
            "var P = 0; \n" +
            "var H = 0; \n" +
            "var E = 0; \n";
        if (this.Temporal > 0) {
            let i = 1;
            while (i <= this.Temporal) {
                if (i == 1) {
                    vars += "var T" + i;
                } else {
                    vars += ", T" + i;
                }
                i++;
            }
            vars += ";\n";
        }
        vars += "\n\ncall principal;\n";
        vars += "goto " + this.LFinal + ";\n\n\n";

        this.Salida = vars + this.Salida;

        this.Salida += this.getCodigoFunciones() + "\n";
        this.Salida += this.LFinal + ":\n";

    }

    addToSalida(Salida, texto) {
        Salida += texto + " \n";
        return Salida;
    }

    //Funcion que escribe a la variable Texto que es la salida
    EscribirC3D(texto) {

        if (this.CodigoNormal) {
            this.Salida += texto + "\n";
        } else {

            this.ambito.Salida_Funciones += texto + "\n"
        }
    }


    // Funcion getTemporal genera el nuevo temporal
    getTemporal() {
            this.Temporal++;
            return "T" + this.Temporal;
        }
        //Funcion getLabel genera el label a utilizar
    getLabel() {
        this.Label++;
        return "L" + this.Label;
    }

    Print(actual) {
        let r = this.evaluar_expresion(actual.Valor);
        if (r != null) {
            if (r.Codigo == "" && r.Temporal == "" && r.Tipo == "") {
                this.Errores.push({
                    error: {
                        No: this.Errores.length,
                        description: JSON.stringify(actual.Valor),
                        linea: 0,
                        columna: 0,
                        tipo: "Semantico"
                    }
                })
            } else
            if (r.Tipo == "string") {
                let T = this.getTemporal();
                let T2 = this.getTemporal();

                let L = this.getLabel();
                let L2 = this.getLabel();
                let L3 = this.getLabel();
                this.EscribirC3D("# Empieza un Print");
                this.EscribirC3D(r.Codigo);
                this.EscribirC3D("Heap[H]= -1;");
                this.EscribirC3D("H = H + 1;");
                this.EscribirC3D(T + " = " + r.Temporal + ";");
                this.EscribirC3D(L3 + ": ");

                this.EscribirC3D(T2 + " = Heap[" + T + "];");
                this.EscribirC3D("if(" + T2 + " <> -1) goto " + L + ";");
                this.EscribirC3D("goto " + L2 + ";");
                this.EscribirC3D(L + ": ");
                this.EscribirC3D("print(\"%c\", " + T2 + ");");
                this.EscribirC3D(T + " = " + T + " + 1;");
                this.EscribirC3D("goto " + L3 + ";");
                this.EscribirC3D(L2 + ": ");
                this.EscribirC3D("print(\"%c\",13);");
            } else if (r.Tipo == "char") {
                this.EscribirC3D(r.Codigo);
                this.EscribirC3D("print(\"%c\"," + r.Temporal + ");");
                this.EscribirC3D("print(\"%c\",13);");
            } else if (r.Tipo == "double") {
                this.EscribirC3D(r.Codigo);
                this.EscribirC3D("print(\"%d\"," + r.Temporal + ");");
                this.EscribirC3D("print(\"%c\",13);");
            } else if (r.Tipo == "integer") {
                this.EscribirC3D(r.Codigo);
                this.EscribirC3D("print(\"%i\"," + r.Temporal + ");");
                this.EscribirC3D("print(\"%c\",13);");
            } else if (r.Tipo == "boolean") {

                let res = "";
                let T1 = this.getTemporal();
                let T2 = this.getTemporal();
                let T3 = this.getTemporal();
                let T4 = this.getTemporal();
                let T5 = this.getTemporal();
                let T6 = this.getTemporal();
                let T7 = this.getTemporal();
                let T8 = this.getTemporal();
                let T9 = this.getTemporal();
                let TH = this.getTemporal();
                let LV = this.getLabel();
                let LF = this.getLabel();
                let LS = this.getLabel();
                if (r.Tipo == "boolean" && r.Temporal == "" && r.BooleanPrimitive == false) {
                    let codi = r.Codigo + "\n";
                    let Tb = this.getTemporal();
                    let LS = this.getLabel();
                    r.EtiquetasV.forEach(ev => {
                        codi += ev + ":\n";
                    });

                    codi += Tb + " = 1;\n";
                    codi += "goto " + LS + ";\n"
                    r.EtiquetasF.forEach(ef => {
                        codi += ef + ":\n";

                    });
                    codi += Tb + " = 0;\n";
                    codi += LS + ":\n"
                    r.Codigo = codi;
                    r.Temporal = Tb;
                    r.BooleanPrimitive = true;
                }
                this.EscribirC3D(r.Codigo + " \n");

                this.EscribirC3D(TH + " = H;\n");
                this.EscribirC3D("#* Convertir boolean a String *#\n");
                this.EscribirC3D("if(" + r.Temporal + " == 1) goto " + LV + ";\n");
                this.EscribirC3D("goto " + LF + ";\n");
                this.EscribirC3D(LV + ":\n");
                this.EscribirC3D("#* TRUE *#\n");
                this.EscribirC3D("#* T *#\n");
                this.EscribirC3D(T1 + " = 116;\n");
                this.EscribirC3D("print(\"%c\", " + T1 + ");\n");
                this.EscribirC3D("#* R *#\n");
                this.EscribirC3D(T2 + " = 114;\n");
                this.EscribirC3D("print(\"%c\", " + T2 + ");\n");
                this.EscribirC3D("#* U *#\n");
                this.EscribirC3D(T3 + " = 117;\n");
                this.EscribirC3D("print(\"%c\", " + T3 + ");\n");
                this.EscribirC3D("#* E *#\n");
                this.EscribirC3D(T4 + " = 101;\n");
                this.EscribirC3D("print(\"%c\", " + T4 + ");\n");
                this.EscribirC3D("goto " + LS + ";\n");
                this.EscribirC3D(LF + ":\n");
                this.EscribirC3D("#* FALSE *#\n");
                this.EscribirC3D("#* TF *#\n");
                this.EscribirC3D(T5 + " = 102;\n");
                this.EscribirC3D("print(\"%c\", " + T5 + ");\n");
                this.EscribirC3D("#* a *#\n");
                this.EscribirC3D(T6 + " = 97;\n");
                this.EscribirC3D("print(\"%c\", " + T6 + ");\n");
                this.EscribirC3D("#* L *#\n");
                this.EscribirC3D(T7 + " = 108;\n");
                this.EscribirC3D("print(\"%c\", " + T7 + ");\n");
                this.EscribirC3D("#* S *#\n");
                this.EscribirC3D(T8 + " = 115;\n");
                this.EscribirC3D("print(\"%c\", " + T8 + ");\n");
                this.EscribirC3D("#* E *#\n");
                this.EscribirC3D(T9 + " = 101;\n");
                this.EscribirC3D("print(\"%c\", " + T9 + ");\n");
                this.EscribirC3D(LS + ":\n");
                this.EscribirC3D("print(\"%c\",13);\n");
            } else if (r.Tipo == "Arreglo") {
                this.EscribirC3D(r.Codigo);
                let Tcont = this.getTemporal();
                let Ttam = this.getTemporal();
                let Tpos = this.getTemporal();
                let Tval = this.getTemporal();
                let LVa = this.getLabel();
                let LFa = this.getLabel();
                let LVco = this.getLabel();
                let LFco = this.getLabel();
                let Lra = this.getLabel();

                this.EscribirC3D(Tcont + " = 1;");
                this.EscribirC3D(Ttam + " = Heap[" + r.Temporal + "];");
                this.EscribirC3D("print(\"%c\", " + 91 + ");");
                this.EscribirC3D(Lra + ":");
                this.EscribirC3D("if(" + Tcont + " <= " + Ttam + ") goto " + LVa + ";");
                this.EscribirC3D("goto " + LFa + ";");
                this.EscribirC3D(LVa + ":");
                this.EscribirC3D("if(" + Tcont + " > 1) goto " + LVco + ";");
                this.EscribirC3D("goto " + LFco + ";");
                this.EscribirC3D(LVco + ":");
                this.EscribirC3D("print(\"%c\", " + 44 + ");");
                this.EscribirC3D("print(\"%c\", " + 32 + ");");
                this.EscribirC3D(LFco + ":");
                this.EscribirC3D(Tpos + " = " + r.Temporal + " + " + Tcont + ";");
                this.EscribirC3D(Tval + " = Heap[" + Tpos + "];");
                switch (r.TipoArreglo) {
                    case "integer":
                        this.EscribirC3D("print(\"%i\"," + Tval + ");\n");
                        break;
                    case "double":
                        this.EscribirC3D("print(\"%d\"," + Tval + ");\n");
                        break;
                    case "char":
                        this.EscribirC3D("print(\"%i\"," + Tval + ");\n");
                        break;
                    case "boolean":
                        this.EscribirC3D("print(\"%i\"," + Tval + ");\n");
                        break;
                    case "string":
                        let T = this.getTemporal();
                        let T2 = this.getTemporal();

                        let L = this.getLabel();
                        let L2 = this.getLabel();
                        let L3 = this.getLabel();


                        this.EscribirC3D("Heap[H]= -1;");
                        this.EscribirC3D("H = H + 1;");
                        this.EscribirC3D(T + " = " + Tval + ";");
                        this.EscribirC3D(L3 + ": ");
                        this.EscribirC3D(T2 + " = Heap[" + T + "];");
                        this.EscribirC3D("if(" + T2 + " <> -1) goto " + L + ";");
                        this.EscribirC3D("goto " + L2 + ";");
                        this.EscribirC3D(L + ": ");
                        this.EscribirC3D("print(\"%c\", " + T2 + ");");
                        this.EscribirC3D(T + " = " + T + " + 1;");
                        this.EscribirC3D("goto " + L3 + ";");
                        this.EscribirC3D(L2 + ": ");

                        break;
                    default:

                        console.log("No imprime el arreglo :V lol");
                        break;
                }
                this.EscribirC3D(Tcont + "= " + Tcont + "+1;");
                this.EscribirC3D("goto " + Lra + ";");
                this.EscribirC3D(LFa + ":");
                this.EscribirC3D("print(\"%c\", " + 93 + ");\n");
                this.EscribirC3D("print(\"%c\",13);");
            } else {
                //console.log(r);
                console.log("No esta imprimiendo");
                // console.log(actual);
                this.Errores.push({
                    error: {
                        No: this.Errores.length,
                        description: "No esta imprimiendo, Tipo: " + r.Tipo,
                        linea: 0,
                        columna: 0,
                        tipo: "Semantico"
                    }
                })

            }
        }


    }

    Do_While(actual) {
        let r1 = this.evaluar_expresion(actual.Condicion);
        let aux1 = this.TipoCiclo;
        let aux2 = this.LVT;
        let aux3 = this.LFT;
        let L1 = this.getLabel();
        let L2 = this.getLabel();
        if (r1.Tipo != "boolean") {
            // marcar error
            this.Errores.push({
                error: {
                    No: this.Errores.length,
                    description: "condicion no es booleano",
                    linea: 0,
                    columna: 0,
                    tipo: "Semantico"
                }
            })

        } else {
            // break y continue
            this.TipoCiclo = "do_while";
            // continue
            this.LVT = L1;
            // break;
            this.LFT = L2;
            // break y continue
            this.EscribirC3D("# Empieza el Do While");
            this.EscribirC3D("# Etiquetas Verdaderas");
            r1.EtiquetasV.forEach(ev => {
                this.EscribirC3D(ev + ":");

            });

            this.EscribirC3D(L1 + ":");



            this.ambito = this.new_ambito(this.ambito);
            this.ambito.funcion_actual = this.ambito.padre.funcion_actual;
            this.ambito.Tam_actual = this.ambito.padre.Tam_actual;
            this.ambito.TempIni = this.ambito.padre.TempIni;
            this.ambito.Return = this.ambito.padre.Return;
            this.Sentencia(actual.Sentencias);


            this.ambito.padre.Salida_Funciones += this.ambito.Salida_Funciones;
            this.ambito = this.borrar_ambito(this.ambito);
            // break y continue
            this.TipoCiclo = aux1;
            // continue
            this.LVT = aux2;
            // break;
            this.LFT = aux3;
            // break y continue
            this.EscribirC3D(r1.Codigo);

            if (r1.BooleanPrimitive) {
                let LV = this.getLabel();
                let LF = this.getLabel();
                this.EscribirC3D("if(" + r1.Temporal + " == 1) goto " + LV);
                this.EscribirC3D("goto " + LF);

                r1.EtiquetasV.push(LV);
                r1.EtiquetasF.push(LF);
            }
            this.EscribirC3D("# Etiquetas Falsas");
            r1.EtiquetasF.forEach(ef => {
                this.EscribirC3D(ef + ":");
            });
            this.EscribirC3D("# Etiqueta Break");
            this.EscribirC3D(L2 + ":");

        }


    }

    Switch(actual) {

        let r = this.evaluar_expresion(actual.Condicion);
        let LSalida = this.getLabel();
        let LVerdaderas = [];
        let LFalsa = [];
        this.EscribirC3D(r.Codigo);
        let aux1 = this.TipoCiclo;
        let aux2 = this.LVT;
        let aux3 = this.LFT;
        let listaExpresiones = [];

        actual.ListaCases.forEach(Case => {
            let Comparar = Case.Case.Valor;
            let L1 = this.getLabel();
            let L2 = this.getLabel();
            LVerdaderas.push(L1);
            LFalsa.push(L2);

            let r1 = this.evaluar_expresion(Comparar);
            listaExpresiones.push(r1);
        });
        // ya cargadas los posibles matchs los recorro
        let i = 0;
        listaExpresiones.forEach(resultado => {
            this.EscribirC3D("# Empieza un Switch");
            this.EscribirC3D(resultado.Codigo);
            // break y continue
            this.TipoCiclo = "switch";
            // continue
            this.LVT = "";
            // break;
            this.LFT = LSalida;
            // break y continue
            this.EscribirC3D("# Cases");
            if (r.Tipo == "string" || resultado.Tipo == "string") {

            } else {
                if (LVerdaderas.length > 0 && LFalsa.length > 0) {
                    this.EscribirC3D("if(" + r.Temporal + " == " + resultado.Temporal + ") goto " + LVerdaderas[0] + ";");
                    this.EscribirC3D("goto " + LFalsa[0] + ";");
                    this.EscribirC3D(LVerdaderas[0] + ":");

                    //console.log(actual.ListaCases[i].Case);


                    this.ambito = this.new_ambito(this.ambito);
                    this.ambito.funcion_actual = this.ambito.padre.funcion_actual;
                    this.ambito.Tam_actual = this.ambito.padre.Tam_actual;
                    this.ambito.TempIni = this.ambito.padre.TempIni;
                    this.ambito.Return = this.ambito.padre.Return;
                    this.Sentencia(actual.ListaCases[i].Case.Sentencias);


                    this.ambito.padre.Salida_Funciones += this.ambito.Salida_Funciones;
                    this.ambito = this.borrar_ambito(this.ambito);
                    this.EscribirC3D("goto " + LSalida + ";");
                    this.EscribirC3D(LFalsa[0] + ":");
                    LVerdaderas.shift();
                    LFalsa.shift();
                    i++;
                }


            }
        });
        if (actual.hasOwnProperty('Default')) {
            this.EscribirC3D("# Default");

            this.ambito = this.new_ambito(this.ambito);
            this.ambito.funcion_actual = this.ambito.padre.funcion_actual;
            this.ambito.Tam_actual = this.ambito.padre.Tam_actual;
            this.ambito.TempIni = this.ambito.padre.TempIni;
            this.ambito.Return = this.ambito.padre.Return;
            this.Sentencia(actual.Default.Sentencias);


            this.ambito.padre.Salida_Funciones += this.ambito.Salida_Funciones;
            this.ambito = this.borrar_ambito(this.ambito);
        }

        // break y continue
        this.TipoCiclo = aux1;
        // continue
        this.LVT = aux2;
        // break;
        this.LFT = aux3;
        // break y continue
        this.EscribirC3D(LSalida + ":");
    }


    While(actual) {
        let r1 = this.evaluar_expresion(actual.Condicion);
        let L1 = this.getLabel();
        let aux1 = this.TipoCiclo;
        let aux2 = this.LVT;
        let aux3 = this.LFT;
        let L2 = this.getLabel();
        let L3 = this.getLabel();

        if (r1.Tipo != "boolean") {

            this.Errores.push({
                error: {
                    No: this.Errores.length,
                    description: "Condicion de While no es booleano",
                    linea: 0,
                    columna: 0,
                    tipo: "Semantico"
                }
            })

        } else {
            // break y continue
            this.TipoCiclo = "while";
            // continue
            this.LVT = L2;
            // break;
            this.LFT = L3;
            // break y continue
            this.EscribirC3D(L1 + ":");
            this.EscribirC3D(r1.Codigo);

            if (r1.BooleanPrimitive) {
                let LV = this.getLabel();
                let LF = this.getLabel();
                this.EscribirC3D("# Empieza el While");
                this.EscribirC3D("if(" + r1.Temporal + " == 1) goto " + LV + ";");
                this.EscribirC3D("goto " + LF + ";");
                r1.EtiquetasV.push(LV);
                r1.EtiquetasF.push(LF);
            }

            this.EscribirC3D("# Etiquetas Verdaderas");
            r1.EtiquetasV.forEach(ev => {

                this.EscribirC3D(ev + ":");

            });
            this.EscribirC3D("# Etiqueta Continue");
            this.EscribirC3D(L2 + ":");
            //sentencias while

            this.ambito = this.new_ambito(this.ambito);
            this.ambito.funcion_actual = this.ambito.padre.funcion_actual;
            this.ambito.Tam_actual = this.ambito.padre.Tam_actual;
            this.ambito.TempIni = this.ambito.padre.TempIni;
            this.ambito.Return = this.ambito.padre.Return;
            this.Sentencia(actual.Sentencias);


            this.ambito.padre.Salida_Funciones += this.ambito.Salida_Funciones;
            this.ambito = this.borrar_ambito(this.ambito);
            // break y continue
            this.TipoCiclo = aux1;
            // continue
            this.LVT = aux2;
            // break;
            this.LFT = aux3;
            // break y continue
            this.EscribirC3D("goto " + L1 + ";");
            this.EscribirC3D("# Etiquetas Falsas");
            r1.EtiquetasF.forEach(ef => {
                this.EscribirC3D(ef + ":");
            });
            this.EscribirC3D("# Etiqueta Break");
            this.EscribirC3D(L3 + ":");

        }

    }
    IF(actual) {
        try {


            let Condicion = this.evaluar_expresion(actual.Condicion);
            if (Condicion.Tipo == "boolean") {
                this.EscribirC3D(Condicion.Codigo);
                this.EscribirC3D("# Empieza el if");
                if (Condicion.BooleanPrimitive) {
                    let LV = this.getLabel();
                    let LF = this.getLabel();

                    this.EscribirC3D("if(" + Condicion.Temporal + " == 1) goto " + LV + ";");
                    this.EscribirC3D("goto " + LF + ";");

                    Condicion.EtiquetasV.push(LV);
                    Condicion.EtiquetasF.push(LF);
                }

                this.EscribirC3D("# Etiquetas Verdaderas");
                Condicion.EtiquetasV.forEach(etiqueta => {
                    this.EscribirC3D(etiqueta + ":");
                });

                //sentencias true

                //sentencias

                this.ambito = this.new_ambito(this.ambito);
                this.ambito.funcion_actual = this.ambito.padre.funcion_actual;
                this.ambito.Tam_actual = this.ambito.padre.Tam_actual;
                this.ambito.TempIni = this.ambito.padre.TempIni;
                this.ambito.Return = this.ambito.padre.Return;
                this.Sentencia(actual.Sentencias);



                this.ambito.padre.Salida_Funciones += this.ambito.Salida_Funciones;
                this.ambito = this.borrar_ambito(this.ambito);



                let Ls = this.getLabel();
                this.EscribirC3D("goto " + Ls + ";");
                this.EscribirC3D("# Etiquetas Falsas");
                Condicion.EtiquetasF.forEach(etiqueta => {
                    this.EscribirC3D(etiqueta + ":");
                });


                if (actual.hasOwnProperty('Else')) {

                    //sentencias falsas
                    let Els = actual.Else;
                    if (Els.hasOwnProperty('ELSE')) {

                        if (Els.ELSE.hasOwnProperty('If')) {

                            this.IF(Els.ELSE.If);
                        } else {
                            let sents = Els.ELSE.Sentencias;


                            this.ambito = this.new_ambito(this.ambito);
                            this.ambito.funcion_actual = this.ambito.padre.funcion_actual;
                            this.ambito.Tam_actual = this.ambito.padre.Tam_actual;
                            this.ambito.TempIni = this.ambito.padre.TempIni;
                            this.ambito.Return = this.ambito.padre.Return;
                            this.Sentencia(sents);


                            this.ambito.padre.Salida_Funciones += this.ambito.Salida_Funciones;
                            this.ambito = this.borrar_ambito(this.ambito);
                        }

                    }
                    //this.Sentencia(r;
                }
                this.EscribirC3D("# Etiqueta Salida");
                this.EscribirC3D(Ls + ":");

            } else {
                this.Errores.push({
                    error: {
                        No: this.Errores.length,
                        description: "Condicion if no es booleano",
                        linea: 0,
                        columna: 0,
                        tipo: "Semantico"
                    }
                })
            }


        } catch (e) {

        }

    }

    FOR(actual) {
        let dec = actual.Declaracion;




        let posicion;
        let aux1 = this.TipoCiclo;
        let aux2 = this.LVT;
        let aux3 = this.LFT;


        let T = this.getTemporal();
        let L1 = this.getLabel();
        let L2 = this.getLabel();
        let L3 = this.getLabel();
        let L4 = this.getLabel();

        // break y continue
        this.TipoCiclo = "FOR";
        // continue
        this.LVT = L4;
        // break;
        this.LFT = L2;
        // break y continue

        this.ambito = this.new_ambito(this.ambito);
        this.ambito.funcion_actual = this.ambito.padre.funcion_actual;
        this.ambito.Tam_actual = this.ambito.padre.Tam_actual;
        this.ambito.TempIni = this.ambito.padre.TempIni;
        this.ambito.Return = this.ambito.padre.Return;
        let T1 = this.getTemporal();
        let T2 = this.getTemporal();
        if (dec != null) {
            let nombre = actual.Declaracion.Declaracion_Tipo1.Nombre[0].toLowerCase();
            let r1 = this.evaluar_expresion(actual.Declaracion.Declaracion_Tipo1.Valor);

            let s = new Simbolo();
            s.id = nombre.toLowerCase();
            s.rol = "var";
            s.valor = actual.Declaracion.Declaracion_Tipo1.Valor;
            s.ambito = "local";
            s.Tipo = actual.Declaracion.Declaracion_Tipo1.Tipo.Tipo;

            if (!this.SimboloExist_Local(this.ambito, s.id.toLowerCase())) {

                let T1 = this.getTemporal();
                this.EscribirC3D(T1 + " = P + " + this.ambito.Tam_actual + ";");

                if (r1.Tipo == "Arreglo") {
                    s.Tipo = "Arreglo";
                    s.TipoArreglo = r1.TipoArreglo;
                }
                s.BooleanPrimitive = r1.BooleanPrimitive;
                this.EscribirC3D(r1.Codigo);
                if (r1.Tipo == "string") {
                    this.EscribirC3D("Heap[H] = -1;");
                    this.EscribirC3D("H = H + 1;");
                }
                this.EscribirC3D("Stack[" + T1 + "] = " + r1.Temporal + ";");
                s.posicion = this.ambito.Tam_actual;
                this.ambito.Tam_actual++;
                this.ambito.TS.push(s);

                this.ReporteTS.push(s);
            } else {
                this.Errores.push({
                    error: {
                        No: this.Errores.length,
                        description: "Ya existe la variable " + s.id.toLowerCase(),
                        linea: 0,
                        columna: 0,
                        tipo: "Semantico"
                    }
                })
            }

            this.EscribirC3D(r1.Codigo);

            this.EscribirC3D(T1 + " = P + " + s.posicion + ";\n");
            this.EscribirC3D(T2 + " = Stack[" + T1 + "] ;");
        }


        let r2 = this.evaluar_expresion(actual.Condicion);

        let LR = this.getLabel();
        this.EscribirC3D(LR + ":");
        if (actual.Condicion != null) {

            this.EscribirC3D(r2.Codigo);

            r2.EtiquetasV.forEach(element => {
                this.EscribirC3D(element + ":");
            });



        }




        this.Sentencia(actual.Sentencias);


        let F = actual.Final;

        this.TipoCiclo = aux1;
        // continue
        this.LVT = aux2;
        // break;
        this.LFT = aux3;
        this.EscribirC3D(L4 + ":");
        this.EscribirC3D(L3 + ":");
        let T3 = this.getTemporal();
        if (F != null) {

            let r3 = this.evaluar_expresion(actual.Final);
            // break y continue

            // break y continue

            this.EscribirC3D(r3.Codigo);
            this.EscribirC3D("Stack[" + T1 + "] = " + r3.Temporal + ";");
            this.EscribirC3D("goto " + LR + ";");

        }

        if (actual.Condicion != null) {
            r2.EtiquetasF.forEach(element => {
                this.EscribirC3D(element + ":");
            });
        }
        this.EscribirC3D(L2 + ":");
        this.ambito.padre.Salida_Funciones += this.ambito.Salida_Funciones;
        this.ambito = this.borrar_ambito(this.ambito);

    }

    evaluar_expresion(valor) {

            let res = "";
            let r = {
                Global: true,
                Temporal: "",
                Tipo: "",
                Codigo: "",
                PosInit: 0,
                EtiquetasF: [],
                EtiquetasV: [],
                Not: "",
                Ref: false,
                Variable: false,
                BooleanPrimitive: false,
                PosRef: "",
                Tam: 1,
                TipoArreglo: ""
            }

            if (Number.isInteger(valor)) {

                let T = this.getTemporal();
                res = T + " = " + valor + ";\n";
                r.Temporal = T;
                r.Codigo = res;
                r.Tipo = "integer";
                return r;
            } else if (typeof valor === "number") {

                let T = this.getTemporal();
                res = T + " = " + valor + ";\n";
                r.Temporal = T;
                r.Codigo = res;
                r.Tipo = "double";
                return r;
            } else if (typeof(valor) === 'boolean') {

                let T = this.getTemporal();

                if (valor) {
                    res = T + " = " + 1 + ";\n";

                } else {
                    res = T + " = " + 0 + ";\n";
                }


                r.Temporal = T;
                r.Codigo = res;
                r.Tipo = "boolean";
                r.BooleanPrimitive = true;
                return r;
            } else if (typeof(valor) === 'string') {

                if (valor.length == 3) {

                    let charArray = Array.from(valor);
                    //   console.log(charArray[1]);

                    if (charArray[0] == "\'" && charArray[2] == "\'") {

                        let T = this.getTemporal();
                        res = T + " = " + charArray[1].charCodeAt(0) + ";\n";
                        r.Temporal = T;
                        r.Codigo = res;
                        r.Tipo = "char";
                        return r;
                    } else {
                        let Tc = "";
                        res += "";
                        let cadena = valor;
                        let Tm = this.getTemporal();
                        res += Tm + " = H;\n";
                        let conti = false;

                        for (let i = 0; i < cadena.length; i++) {
                            if (conti) {
                                conti = false;
                                continue;
                            }
                            Tc = this.getTemporal();
                            let ascii = cadena.charCodeAt(i);

                            if (i + 1 < cadena.length) {
                                if (ascii == 92 && cadena.charCodeAt(i + 1) == 110) {
                                    res += Tc + " = " + 13 + ";\n";
                                    res += "Heap[H] = " + Tc + ";\n";
                                    res += "H = H + 1;\n";
                                    conti = true;
                                } else {
                                    res += Tc + " = " + ascii + ";\n";
                                    res += "Heap[H] = " + Tc + ";\n";
                                    res += "H = H + 1;\n";
                                }
                            } else {
                                res += Tc + " = " + ascii + ";\n";
                                res += "Heap[H] = " + Tc + ";\n";
                                res += "H = H + 1;\n";
                            }


                        }
                        r.Temporal = Tm;
                        r.Codigo = res;
                        r.Tipo = "string";
                        return r;
                    }
                } else {
                    let Tc = "";
                    res += "";
                    let cadena = valor;
                    let Tm = this.getTemporal();
                    res += Tm + " = H;\n";
                    let conti = false;
                    for (let i = 0; i < cadena.length; i++) {
                        if (conti) {
                            conti = false;
                            continue;
                        }
                        Tc = this.getTemporal();
                        let ascii = cadena.charCodeAt(i);
                        if (i + 1 < cadena.length) {
                            if (ascii == 92 && cadena.charCodeAt(i + 1) == 110) {
                                res += Tc + " = " + 13 + ";\n";
                                res += "Heap[H] = " + Tc + ";\n";
                                res += "H = H + 1;\n";
                                conti = true;
                            } else {
                                res += Tc + " = " + ascii + ";\n";
                                res += "Heap[H] = " + Tc + ";\n";
                                res += "H = H + 1;\n";
                            }
                        } else {
                            res += Tc + " = " + ascii + ";\n";
                            res += "Heap[H] = " + Tc + ";\n";
                            res += "H = H + 1;\n";
                        }
                    }
                    r.Temporal = Tm;
                    r.Codigo = res;
                    r.Tipo = "string";
                    return r;
                }

            } else if (typeof(valor) == 'object') {
                if (valor == null) {

                } else if (valor.hasOwnProperty('NOT')) {
                    let r1 = this.evaluar_expresion(valor.NOT.Valor1);
                    if (r1.Tipo == "boolean") {

                        let res = "";
                        res += r1.Codigo;

                        if (r1.Temporal != "") {
                            let L1 = this.getLabel();
                            let L2 = this.getLabel();
                            let L3 = this.getLabel();
                            res += "if(" + r1.Temporal + " == 1) goto " + L1 + ";\n";
                            res += "goto " + L2 + ";\n";
                            res += L1 + ":\n";
                            res += r1.Temporal + " = 0;\n"
                            res += "goto " + L3 + ";\n"
                            res += L2 + ":\n";
                            res += r1.Temporal + " = 1;\n"
                            res += L3 + ":\n";
                            r.Temporal = r1.Temporal;
                            r.Codigo = res;
                            r.Tipo = r1.Tipo;
                            r.TipoArreglo = r1.TipoArreglo;
                            r.BooleanPrimitive = true;
                            return r;
                        } else {
                            r.EtiquetasF = r1.EtiquetasV;
                            r.EtiquetasV = r1.EtiquetasF;
                            r.Temporal = "";
                            r.Codigo = res;
                            r.Tipo = r1.Tipo;

                            return r;
                        }



                    }
                } else if (valor.hasOwnProperty('UNARIO_MENOS')) {
                    let r1 = this.evaluar_expresion(valor.UNARIO_MENOS.Valor1);
                    let T1 = this.getTemporal();
                    let T2 = this.getTemporal();
                    let res = "";
                    res += r1.Codigo;
                    res += T1 + " = -1;\n";
                    res += T2 + " = " + r1.Temporal + " * " + T1 + ";\n";
                    r.Temporal = T2;
                    r.Codigo = res;
                    r.Tipo = r1.Tipo;
                    r.TipoArreglo = r1.TipoArreglo;
                    return r;
                } else if (valor.hasOwnProperty('Llamada_Funcion')) {
                    try {
                        let Nombre = valor.Llamada_Funcion.Nombre.toLowerCase();
                        let res = "";
                        let listaPar = [];
                        let output = "";
                        let TipoF = "";

                        valor.Llamada_Funcion.Parametros.forEach(p => {
                            let r = this.evaluar_expresion(p);
                            listaPar.push(r);
                            Nombre += "_" + r.Tipo;
                        });
                        if (this.Function_exist(Nombre)) {
                            let f = this.getFunction(Nombre);
                            TipoF = f.Tipo;
                            let tam = f.Parametros.length;
                            let Tamano_actual = this.ambito.Tam_actual;
                            let Tamano_pasado = this.ambito.Tam_actual;
                            this.ambito.TempActual = this.Temporal;
                            let res_temps = "";
                            let Tcont = this.getTemporal();
                            let TposIniStack = this.getTemporal();
                            output += "# Carga de Temporales al Stack\n";
                            res_temps += Tcont + " = " + 0 + ";\n";
                            res_temps += TposIniStack + " = P + " + Tamano_actual + ";\n";

                            let Tpos = this.getTemporal();
                            res_temps += Tpos + " = " + TposIniStack + " + " + Tcont + ";\n";

                            for (let i = this.ambito.TempIni; i <= this.ambito.TempActual; i++) {


                                res_temps += "Stack[" + Tpos + "] = T" + i + ";\n";
                                res_temps += Tpos + " = " + Tpos + " + " + 1 + ";\n";
                                Tamano_actual++;

                            }

                            output += res_temps;
                            output += "# Termina Carga de Temporales al Stack\n";


                            let Treturn = this.getTemporal();

                            this.ambito = this.new_ambito(this.ambito);

                            this.ambito.funcion_actual = Nombre;
                            this.ambito.Tam_actual++;
                            if (f.Codigo == false) {
                                this.setFunctionLabel(Nombre);
                            }
                            this.ambito.Return = Treturn;

                            this.ambito.TempIni = this.Temporal + 1;
                            output += "# Cambio Simulado de Ambito\n";
                            for (let i = 0; i < listaPar.length; i++) {

                                let T1 = this.getTemporal();
                                let T2 = this.getTemporal();

                                let s = new Simbolo();
                                s.id = f.Parametros[i].Nombre.toLowerCase();
                                if (f.Parametros[i].Tipo.hasOwnProperty('Corchetes')) {
                                    s.Tipo = "Arreglo";
                                    s.TipoArreglo = f.Parametros[i].Tipo.Tipo;
                                    f.Parametros[i].Tipo.Corchetes.forEach(e => {
                                        s.cantidad_dimensiones++;
                                    });
                                } else {
                                    s.Tipo = f.Parametros[i].Tipo.Tipo;
                                }
                                s.rol = "parametro";
                                s.ambito = "local";
                                if (!this.SimboloExist_Local(this.ambito, s.id.toLowerCase())) {

                                    //output += T1 + " = P + " + this.ambito.padre.Tam_actual + ";\n";
                                    output += T1 + " = P + " + Tamano_actual + ";\n";
                                    output += T2 + " = " + T1 + " + " + this.ambito.Tam_actual + ";\n";
                                    s.posicion = this.ambito.Tam_actual;
                                    output += listaPar[i].Codigo + "\n";
                                    this.ambito.Tam_actual++;
                                    output += "Stack[" + T2 + "] = " + listaPar[i].Temporal + ";\n";
                                    this.ambito.TS.push(s);
                                } else {
                                    this.Errores.push({
                                        error: {
                                            No: this.Errores.length,
                                            description: "Ya existe la variable",
                                            linea: 0,
                                            columna: 0,
                                            tipo: "Semantico"
                                        }
                                    })
                                }
                            }

                            output += "# Cambio Formal de Ambito\n";
                            //output += "P = P +" + this.ambito.padre.Tam_actual + ";\n";
                            output += "P = P +" + Tamano_actual + ";\n";
                            output += Treturn + " = P;\n";
                            this.ambito.Tam_actual = listaPar.length + 1;
                            // agregar parametros al nuevo ambito

                            if (f.Codigo == false) {
                                this.setFunctionCodigoTrue(f.Nombre.toLowerCase());
                                this.Sentencia(f.Sentencias);
                                let a = "";
                                a += "proc " + f.Nombre.toLowerCase() + " begin\n";

                                a += this.ambito.Salida_Funciones + "\n";
                                a += this.getFunctionLabel(Nombre) + ":\n";
                                a += "end" + "\n";

                                this.addCodigoToFuncion(Nombre, a);
                            }
                            output += "call " + f.Nombre.toLowerCase() + ";\n";

                            let TF = this.getTemporal();


                            let TFpos = this.getTemporal();
                            output += TFpos + " =  P + " + 0 + ";\n"
                            output += TF + " = Stack[" + TFpos + "];\n";
                            //console.log(this.ambito.padre.Tam_actual);



                            this.ambito = this.borrar_ambito(this.ambito);

                            //output += "P = P - " + this.ambito.Tam_actual + ";\n";
                            output += "P = P - " + Tamano_actual + ";\n";
                            res_temps = "";
                            Tcont = this.getTemporal();
                            TposIniStack = this.getTemporal();
                            output += "# Sacar de Stack a Temporales\n";
                            res_temps += Tcont + " = " + 0 + ";\n";
                            res_temps += TposIniStack + " = P + " + Tamano_pasado + ";\n";
                            Tpos = this.getTemporal();

                            res_temps += Tpos + " = " + TposIniStack + " + " + Tcont + ";\n";


                            for (let i = this.ambito.TempIni; i < this.ambito.TempActual; i++) {

                                res_temps += "T" + i + " = Stack[" + Tpos + "];\n";

                                res_temps += Tpos + " = " + Tpos + " + " + 1 + ";\n";
                            }
                            output += res_temps;
                            output += "# Termina Sacar del Stack a Tepmorales\n";

                            r.Codigo = output;
                            r.Temporal = TF;
                            r.Tipo = TipoF;

                            return r;

                        }
                    } catch (e) {
                        console.log(e)
                    }
                } else if (valor.hasOwnProperty('DECREMENTO')) {
                    let incre = valor.DECREMENTO.Valor1;
                    if (incre.hasOwnProperty('AccesoArreglo')) {
                        let Nombre = incre.AccesoArreglo.Nombre.toLowerCase();
                        let pos = this.evaluar_expresion(incre.AccesoArreglo.Posicion);
                        if (this.SimboloExist(this.ambito, Nombre)) {
                            let s = this.getSimbolo(this.ambito, Nombre);

                            let res = "";
                            if (s != null) {
                                if (s.ambito == "global") {
                                    let T1 = this.getTemporal();
                                    let T2 = this.getTemporal();
                                    let T3 = this.getTemporal();
                                    let T4 = this.getTemporal();
                                    let T5 = this.getTemporal();
                                    let T6 = this.getTemporal();
                                    let T7 = this.getTemporal();
                                    let T8 = this.getTemporal();
                                    let T9 = this.getTemporal();
                                    let LV = this.getLabel();
                                    let LF = this.getLabel();
                                    let LV2 = this.getLabel();
                                    let LS = this.getLabel();
                                    res += T1 + " = 0 + " + s.posicion + ";\n";
                                    res += pos.Codigo;
                                    res += T2 + " = Heap[" + T1 + "];\n";
                                    res += T3 + " = Heap[" + T2 + "];\n";
                                    res += "if(" + pos.Temporal + " >= 1) goto " + LV + ";\n";
                                    res += "goto " + LF + ";\n";
                                    res += LV + ":\n";
                                    res += "if(" + pos.Temporal + " <= " + T3 + ") goto " + LV2 + ";\n";
                                    res += "goto " + LF + ";\n";
                                    res += LV2 + ":\n";
                                    res += T4 + " = " + T2 + " + 1" + ";\n";
                                    res += T5 + " = " + pos.Temporal + " - 1;\n";
                                    res += T6 + " = " + T4 + " + " + T5 + ";\n";

                                    res += T7 + " = Heap[" + T6 + "];\n";
                                    res += T8 + " = 1;\n";
                                    res += T9 + " = " + T7 + " - " + T8 + ";\n";
                                    res += "goto " + LS + ";\n";
                                    res += LF + ":\n";
                                    res += "E = 2;\n";
                                    res += LS + ":\n";

                                    r.Codigo = res;
                                    r.Temporal = T9;
                                    if (s.Tipo == "Arreglo") {
                                        r.Tipo = s.TipoArreglo;
                                    } else {
                                        r.Tipo = s.Tipo;
                                    }

                                    return r;
                                } else {
                                    let T1 = this.getTemporal();
                                    let T2 = this.getTemporal();
                                    let T3 = this.getTemporal();
                                    let T4 = this.getTemporal();
                                    let T5 = this.getTemporal();
                                    let T6 = this.getTemporal();
                                    let T7 = this.getTemporal();
                                    let T8 = this.getTemporal();
                                    let T9 = this.getTemporal();
                                    let LV = this.getLabel();
                                    let LF = this.getLabel();
                                    let LV2 = this.getLabel();

                                    let LS = this.getLabel();

                                    res += T1 + " = P + " + s.posicion + ";\n";
                                    res += pos.Codigo;
                                    res += T2 + " = Stack[" + T1 + "];\n";
                                    res += T3 + " = Heap[" + T2 + "];\n";
                                    res += "if(" + pos.Temporal + " >= 1) goto " + LV + ";\n";
                                    res += "goto " + LF + ";\n";
                                    res += LV + ":\n";
                                    res += "if(" + pos.Temporal + " <= " + T3 + ") goto " + LV2 + ";\n";
                                    res += "goto " + LF + ";\n";
                                    res += LV2 + ":\n";
                                    res += T4 + " = " + T2 + " + 1" + ";\n";
                                    res += T5 + " = " + pos.Temporal + " - 1;\n";
                                    res += T6 + " = " + T4 + " + " + T5 + ";\n";

                                    res += T7 + " = Heap[" + T6 + "];\n";
                                    res += T8 + " = 1;\n";
                                    res += T9 + " = " + T7 + " - " + T8 + ";\n";
                                    res += "goto " + LS + ";\n";
                                    res += LF + ":\n";
                                    res += "E = 2;\n";
                                    res += LS + ":\n";
                                    r.Codigo = res;
                                    r.Temporal = T7;
                                    if (s.Tipo == "Arreglo") {
                                        r.Tipo = s.TipoArreglo;
                                    } else {
                                        r.Tipo = s.Tipo;
                                    }
                                    return r;
                                }

                            } else {
                                this.Errores.push({
                                    error: {
                                        No: this.Errores.length,
                                        description: "No existe la variable",
                                        linea: 0,
                                        columna: 0,
                                        tipo: "Semantico"
                                    }
                                })
                            }
                        } else {
                            this.Errores.push({
                                error: {
                                    No: this.Errores.length,
                                    description: "No existe la variable",
                                    linea: 0,
                                    columna: 0,
                                    tipo: "Semantico"
                                }
                            })
                        }


                    } else if (incre.hasOwnProperty('Identificador')) {
                        let nombre = incre.Identificador.toLowerCase();
                        if (this.SimboloExist(this.ambito, nombre)) {
                            let s = this.getSimbolo(this.ambito, nombre);
                            let res = "";
                            if (s != null) {
                                if (s.ambito == "global") {
                                    let T1 = this.getTemporal();
                                    let T2 = this.getTemporal();
                                    let T3 = this.getTemporal();
                                    let T4 = this.getTemporal();
                                    res += T1 + " = 0 + " + s.posicion + ";\n";
                                    res += T2 + " = Heap[" + T1 + "];\n";
                                    res += T3 + " = 1;\n";
                                    res += T4 + " = " + T2 + " - " + T3 + ";\n";
                                    r.Temporal = T4;
                                    r.Codigo = res;
                                    r.Tipo = s.Tipo;
                                    r.BooleanPrimitive = s.BooleanPrimitive;
                                    r.Variable = true;
                                    if (s.Tipo === "Arreglo") {
                                        r.TipoArreglo = s.TipoArreglo;
                                    }
                                    return r;
                                } else {

                                    let T1 = this.getTemporal();
                                    let T2 = this.getTemporal();
                                    let T3 = this.getTemporal();
                                    let T4 = this.getTemporal();
                                    res += T1 + " =  P + " + s.posicion + ";\n";
                                    res += T2 + " = Stack[" + T1 + "];\n";
                                    res += T3 + " = 1;\n";
                                    res += T4 + " = " + T2 + " - " + T3 + ";\n";
                                    r.Temporal = T4;
                                    r.Codigo = res;
                                    r.Tipo = s.Tipo;
                                    r.Variable = true;
                                    r.BooleanPrimitive = s.BooleanPrimitive;
                                    if (s.Tipo == "Arreglo") {
                                        r.TipoArreglo = s.TipoArreglo;
                                    }
                                    return r;
                                }
                            }
                        } else {
                            this.Errores.push({
                                error: {
                                    No: this.Errores.length,
                                    description: "No existe la variable",
                                    linea: 0,
                                    columna: 0,
                                    tipo: "Semantico"
                                }
                            })
                        }

                    } else {
                        //marcar error
                        this.Errores.push({
                            error: {
                                No: this.Errores.length,
                                description: "No existe la variable",
                                linea: 0,
                                columna: 0,
                                tipo: "Semantico"
                            }
                        })
                    }

                } else if (valor.hasOwnProperty('INCREMENTO')) {

                    let incre = valor.INCREMENTO.Valor1;
                    if (incre.hasOwnProperty('AccesoArreglo')) {

                        let Nombre = incre.AccesoArreglo.Nombre.toLowerCase();

                        let pos = this.evaluar_expresion(incre.AccesoArreglo.Posicion);
                        if (this.SimboloExist(this.ambito, Nombre)) {
                            let s = this.getSimbolo(this.ambito, Nombre);

                            let res = "";
                            if (s != null) {
                                if (s.ambito == "global") {
                                    let T1 = this.getTemporal();
                                    let T2 = this.getTemporal();
                                    let T3 = this.getTemporal();
                                    let T4 = this.getTemporal();
                                    let T5 = this.getTemporal();
                                    let T6 = this.getTemporal();
                                    let T7 = this.getTemporal();
                                    let T8 = this.getTemporal();
                                    let T9 = this.getTemporal();
                                    let LV = this.getLabel();
                                    let LF = this.getLabel();
                                    let LV2 = this.getLabel();
                                    let LS = this.getLabel();
                                    res += T1 + " = 0 + " + s.posicion + ";\n";
                                    res += pos.Codigo;
                                    res += T2 + " = Heap[" + T1 + "];\n";
                                    res += T3 + " = Heap[" + T2 + "];\n";
                                    res += "if(" + pos.Temporal + " >= 1) goto " + LV + ";\n";
                                    res += "goto " + LF + ";\n";
                                    res += LV + ":\n";
                                    res += "if(" + pos.Temporal + " <= " + T3 + ") goto " + LV2 + ";\n";
                                    res += "goto " + LF + ";\n";
                                    res += LV2 + ":\n";
                                    res += T4 + " = " + T2 + " + 1" + ";\n";
                                    res += T5 + " = " + pos.Temporal + " - 1;\n";
                                    res += T6 + " = " + T4 + " + " + T5 + ";\n";

                                    res += T7 + " = Heap[" + T6 + "];\n";
                                    res += T8 + " = 1;\n";
                                    res += T9 + " = " + T7 + " + " + T8 + ";\n";
                                    res += "goto " + LS + ";\n";
                                    res += LF + ":\n";
                                    res += "E = 2;\n";
                                    res += LS + ":\n";

                                    r.Codigo = res;
                                    r.Temporal = T9;
                                    if (s.Tipo == "Arreglo") {
                                        r.Tipo = s.TipoArreglo;
                                    } else {
                                        r.Tipo = s.Tipo;
                                    }

                                    return r;
                                } else {
                                    let T1 = this.getTemporal();
                                    let T2 = this.getTemporal();
                                    let T3 = this.getTemporal();
                                    let T4 = this.getTemporal();
                                    let T5 = this.getTemporal();
                                    let T6 = this.getTemporal();
                                    let T7 = this.getTemporal();
                                    let T8 = this.getTemporal();
                                    let T9 = this.getTemporal();
                                    let LV = this.getLabel();
                                    let LF = this.getLabel();
                                    let LV2 = this.getLabel();

                                    let LS = this.getLabel();

                                    res += T1 + " = P + " + s.posicion + ";\n";
                                    res += pos.Codigo;
                                    res += T2 + " = Stack[" + T1 + "];\n";
                                    res += T3 + " = Heap[" + T2 + "];\n";
                                    res += "if(" + pos.Temporal + " >= 1) goto " + LV + ";\n";
                                    res += "goto " + LF + ";\n";
                                    res += LV + ":\n";
                                    res += "if(" + pos.Temporal + " <= " + T3 + ") goto " + LV2 + ";\n";
                                    res += "goto " + LF + ";\n";
                                    res += LV2 + ":\n";
                                    res += T4 + " = " + T2 + " + 1" + ";\n";
                                    res += T5 + " = " + pos.Temporal + " - 1;\n";
                                    res += T6 + " = " + T4 + " + " + T5 + ";\n";

                                    res += T7 + " = Heap[" + T6 + "];\n";
                                    res += T8 + " = 1;\n";
                                    res += T9 + " = " + T7 + " + " + T8 + ";\n";
                                    res += "goto " + LS + ";\n";
                                    res += LF + ":\n";
                                    res += "E = 2;\n";
                                    res += LS + ":\n";
                                    r.Codigo = res;
                                    r.Temporal = T9;
                                    if (s.Tipo == "Arreglo") {
                                        r.Tipo = s.TipoArreglo;
                                    } else {
                                        r.Tipo = s.Tipo;
                                    }
                                    return r;
                                }

                            } else {
                                this.Errores.push({
                                    error: {
                                        No: this.Errores.length,
                                        description: "No existe la variable",
                                        linea: 0,
                                        columna: 0,
                                        tipo: "Semantico"
                                    }
                                })
                            }

                        } else {
                            this.Errores.push({
                                error: {
                                    No: this.Errores.length,
                                    description: "No existe la variable",
                                    linea: 0,
                                    columna: 0,
                                    tipo: "Semantico"
                                }
                            })
                        }


                    } else if (incre.hasOwnProperty('Identificador')) {
                        let nombre = incre.Identificador.toLowerCase();

                        if (this.SimboloExist(this.ambito, nombre)) {

                            let s = this.getSimbolo(this.ambito, nombre);
                            let res = "";
                            if (s != null) {
                                if (s.ambito == "global") {
                                    let T1 = this.getTemporal();
                                    let T2 = this.getTemporal();
                                    let T3 = this.getTemporal();
                                    let T4 = this.getTemporal();
                                    res += T1 + " = 0 + " + s.posicion + ";\n";
                                    res += T2 + " = Heap[" + T1 + "];\n";
                                    res += T3 + " = 1;\n";
                                    res += T4 + " = " + T2 + " + " + T3 + ";\n";

                                    r.Temporal = T4;
                                    r.Codigo = res;
                                    r.Tipo = s.Tipo;
                                    r.BooleanPrimitive = s.BooleanPrimitive;
                                    r.Variable = true;
                                    if (s.Tipo === "Arreglo") {
                                        r.TipoArreglo = s.TipoArreglo;
                                    }
                                    return r;
                                } else {

                                    let T1 = this.getTemporal();
                                    let T2 = this.getTemporal();
                                    let T3 = this.getTemporal();
                                    let T4 = this.getTemporal();
                                    res += T1 + " =  P + " + s.posicion + ";\n";
                                    res += T2 + " = Stack[" + T1 + "];\n";
                                    res += T3 + " = 1;\n";
                                    res += T4 + " = " + T2 + " + " + T3 + ";\n";
                                    r.Temporal = T4;
                                    r.Codigo = res;
                                    r.Tipo = s.Tipo;
                                    r.Variable = true;
                                    r.BooleanPrimitive = s.BooleanPrimitive;
                                    if (s.Tipo == "Arreglo") {
                                        r.TipoArreglo = s.TipoArreglo;
                                    }
                                    return r;
                                }
                            }
                        } else {
                            this.Errores.push({
                                error: {
                                    No: this.Errores.length,
                                    description: "No existe la variable",
                                    linea: 0,
                                    columna: 0,
                                    tipo: "Semantico"
                                }
                            })
                        }

                    } else {
                        //marcar error
                        this.Errores.push({
                            error: {
                                No: this.Errores.length,
                                description: "No existe la variable",
                                linea: 0,
                                columna: 0,
                                tipo: "Semantico"
                            }
                        })
                    }

                } else if (valor.hasOwnProperty('AccesoArreglo')) {
                    let Acceso = valor.AccesoArreglo;
                    let Nombre = Acceso.Nombre.toLowerCase();
                    let pos = this.evaluar_expresion(Acceso.Posicion);
                    if (this.SimboloExist(this.ambito, Nombre)) {
                        let s = this.getSimbolo(this.ambito, Nombre);

                        let res = "";
                        if (s != null) {
                            if (s.ambito == "global") {
                                let T1 = this.getTemporal();
                                let T2 = this.getTemporal();
                                let T3 = this.getTemporal();
                                let T4 = this.getTemporal();
                                let T5 = this.getTemporal();
                                let T6 = this.getTemporal();
                                let T7 = this.getTemporal();
                                let LV = this.getLabel();
                                let LF = this.getLabel();
                                let LV2 = this.getLabel();

                                let LS = this.getLabel();
                                res += T1 + " = 0 + " + s.posicion + ";\n";
                                res += pos.Codigo;
                                res += T2 + " = Heap[" + T1 + "];\n";
                                res += T3 + " = Heap[" + T2 + "];\n";
                                res += "if(" + pos.Temporal + " >= 1) goto " + LV + ";\n";
                                res += "goto " + LF + ";\n";
                                res += LV + ":\n";
                                res += "if(" + pos.Temporal + " <= " + T3 + ") goto " + LV2 + ";\n";
                                res += "goto " + LF + ";\n";
                                res += LV2 + ":\n";
                                res += T4 + " = " + T2 + " + 1" + ";\n";
                                res += T5 + " = " + pos.Temporal + " - 1;\n";
                                res += T6 + " = " + T4 + " + " + T5 + ";\n";

                                res += T7 + " = Heap[" + T6 + "];\n";
                                res += "goto " + LS + ";\n";
                                res += LF + ":\n";
                                res += "E = 2;\n";
                                res += LS + ":\n";
                                r.Codigo = res;
                                r.Temporal = T7;
                                if (s.Tipo == "Arreglo") {
                                    r.Tipo = s.TipoArreglo;
                                } else {
                                    r.Tipo = s.Tipo;
                                }

                                return r;
                            } else {
                                let T1 = this.getTemporal();
                                let T2 = this.getTemporal();
                                let T3 = this.getTemporal();
                                let T4 = this.getTemporal();
                                let T5 = this.getTemporal();
                                let T6 = this.getTemporal();
                                let T7 = this.getTemporal();
                                let LV = this.getLabel();
                                let LF = this.getLabel();
                                let LV2 = this.getLabel();

                                let LS = this.getLabel();

                                res += T1 + " = " + s.posicion + ";\n";
                                res += pos.Codigo;
                                res += T2 + " = Stack[" + T1 + "];\n";
                                res += T3 + " = Heap[" + T2 + "];\n";
                                res += "if(" + pos.Temporal + " >= 1) goto " + LV + ";\n";
                                res += "goto " + LF + ";\n";
                                res += LV + ":\n";
                                res += "if(" + pos.Temporal + " <= " + T3 + ") goto " + LV2 + ";\n";
                                res += "goto " + LF + ";\n";
                                res += LV2 + ":\n";
                                res += T4 + " = " + T2 + " + 1" + ";\n";
                                res += T5 + " = " + pos.Temporal + " - 1;\n";
                                res += T6 + " = " + T4 + " + " + T5 + ";\n";

                                res += T7 + " = Heap[" + T6 + "];\n";
                                res += "goto " + LS + ";\n";
                                res += LF + ":\n";
                                res += "E = 2;\n";
                                res += LS + ":\n";
                                r.Codigo = res;
                                r.Temporal = T7;
                                if (s.Tipo == "Arreglo") {
                                    r.Tipo = s.TipoArreglo;
                                } else {
                                    r.Tipo = s.Tipo;
                                }
                                return r;
                            }

                        } else {
                            //marcar error
                            this.Errores.push({
                                error: {
                                    No: this.Errores.length,
                                    description: "No existe la variable",
                                    linea: 0,
                                    columna: 0,
                                    tipo: "Semantico"
                                }
                            })
                        }
                    } else {
                        this.Errores.push({
                            error: {
                                No: this.Errores.length,
                                description: "No existe la variable",
                                linea: 0,
                                columna: 0,
                                tipo: "Semantico"
                            }
                        })
                    }



                } else if (valor.hasOwnProperty('ListaExpresiones')) {
                    let listaExpresiones = valor.ListaExpresiones;
                    let tam = listaExpresiones.length;
                    let res = "";

                    let Ttam = this.getTemporal();
                    res += Ttam + " = H;\n";
                    res += "Heap[" + Ttam + "] = " + tam + ";\n";
                    res += "H = H + 1;\n";
                    r.TipoArreglo = "";
                    //   console.log(tam);
                    for (let index = 0; index < tam; index++) {
                        let r1 = this.evaluar_expresion(listaExpresiones[index]);
                        //  console.log(r1);
                        if (r.TipoArreglo == "") {
                            r.TipoArreglo = r1.Tipo;

                        } else if (r.TipoArreglo == r1.Tipo) {
                            r.TipoArreglo = r1.Tipo;
                        } else {
                            //marcar error
                        }
                        res += r1.Codigo;
                        res += "Heap[H] = " + r1.Temporal + ";\n";
                        res += "H = H + 1;\n";
                    }
                    r.Codigo = res;
                    r.Temporal = Ttam;
                    r.Tipo = "Arreglo";
                    return r;

                } else if (valor.hasOwnProperty('Identificador')) {
                    let nombre = valor.Identificador.toLowerCase();
                    if (this.SimboloExist(this.ambito, nombre)) {
                        let s = this.getSimbolo(this.ambito, nombre);
                        let res = "";
                        if (s != null) {
                            if (s.ambito == "global") {
                                let T1 = this.getTemporal();
                                let T2 = this.getTemporal();
                                res += T1 + " = 0 + " + s.posicion + ";\n";
                                res += T2 + " = Heap[" + T1 + "];\n";

                                r.Temporal = T2;
                                r.Codigo = res;
                                r.Tipo = s.Tipo;
                                r.BooleanPrimitive = s.BooleanPrimitive;
                                r.Variable = true;
                                if (s.Tipo === "Arreglo") {
                                    r.TipoArreglo = s.TipoArreglo;
                                }
                                return r;
                            } else {

                                let T1 = this.getTemporal();

                                let T3 = this.getTemporal();

                                res += T1 + " =  P +" + s.posicion + ";\n";

                                res += T3 + " = Stack[" + T1 + "];\n";

                                r.Temporal = T3;
                                r.Codigo = res;
                                r.Tipo = s.Tipo;
                                r.Variable = true;
                                r.BooleanPrimitive = s.BooleanPrimitive;
                                if (s.Tipo == "Arreglo") {
                                    r.TipoArreglo = s.TipoArreglo;
                                }
                                return r;


                            }
                        } else {
                            this.Errores.push({
                                error: {
                                    No: this.Errores.length,
                                    description: "Es null",
                                    linea: 0,
                                    columna: 0,
                                    tipo: "Semantico"
                                }
                            })
                        }
                    } else {
                        this.Errores.push({
                            error: {
                                No: this.Errores.length,
                                description: "No existe la variable",
                                linea: 0,
                                columna: 0,
                                tipo: "Semantico"
                            }
                        })
                    }


                } else if (valor.hasOwnProperty("SUMA")) {

                    let r1 = this.evaluar_expresion(valor.SUMA.Valor1);
                    let r2 = this.evaluar_expresion(valor.SUMA.Valor2);
                    let TipoRes = this.getType(r1.Tipo, r2.Tipo, "+");

                    switch (TipoRes) {
                        case "string":
                            if (r1.Tipo == "string") {
                                if (!r1.Variable) {
                                    r.Temporal = r1.Temporal;
                                    r.Tipo = "string";
                                    r.Codigo = r1.Codigo + " \n";
                                } else {
                                    r.Tipo = "string";
                                    let labelV = this.getLabel();
                                    let labelF = this.getLabel();

                                    let labelR = this.getLabel();
                                    let TempgetValor = this.getTemporal();
                                    let TempPosIni = this.getTemporal();
                                    // String Tempaux =this.getTemporal();
                                    r.Codigo += r1.Codigo;
                                    r.Codigo += TempPosIni + " = H;\n";
                                    r.Codigo += labelR + ":\n";
                                    r.Codigo += TempgetValor + " = Heap[" + r1.Temporal + "];\n";
                                    r.Codigo += "if (" + TempgetValor + " <> -1) goto " + labelV + ";\n";
                                    r.Codigo += "goto " + labelF + ";\n";
                                    r.Codigo += labelV + ": \n";
                                    r.Codigo += "Heap[H] = " + TempgetValor + ";\n";
                                    r.Codigo += "H = H + 1; \n";
                                    r.Codigo += r1.Temporal + " = " + r1.Temporal + " + 1;\n";
                                    r.Codigo += "goto " + labelR + ";\n";
                                    r.Codigo += labelF + ": \n";
                                    r.Temporal = TempPosIni;

                                }
                            } else {
                                if (r1.Tipo == "boolean") {
                                    let T1 = this.getTemporal();
                                    let T2 = this.getTemporal();
                                    let T3 = this.getTemporal();
                                    let T4 = this.getTemporal();
                                    let T5 = this.getTemporal();
                                    let T6 = this.getTemporal();
                                    let T7 = this.getTemporal();
                                    let T8 = this.getTemporal();
                                    let T9 = this.getTemporal();
                                    let TH = this.getTemporal();
                                    let LV = this.getLabel();
                                    let LF = this.getLabel();
                                    let LS = this.getLabel();
                                    r.Codigo = r1.Codigo + " \n";
                                    r.Codigo += TH + " = H;\n";
                                    r.Codigo += "#* Convertir boolean a String *#\n";
                                    r.Codigo += "if(" + r1.Temporal + " == 1) goto " + LV + ";\n";
                                    r.Codigo += "goto " + LF + ";\n";
                                    r.Codigo += LV + ":\n";
                                    r.Codigo += "#* TRUE *#\n";
                                    r.Codigo += "#* T *#\n";
                                    r.Codigo += T1 + " = 116;\n";
                                    r.Codigo += "Heap[H] = " + T1 + ";\n";
                                    r.Codigo += "H = H + 1; \n";
                                    r.Codigo += "#* R *#\n";
                                    r.Codigo += T2 + " = 114;\n";
                                    r.Codigo += "Heap[H] = " + T2 + ";\n";
                                    r.Codigo += "H = H + 1; \n";
                                    r.Codigo += "#* U *#\n";
                                    r.Codigo += T3 + " = 117;\n";
                                    r.Codigo += "Heap[H] = " + T3 + ";\n";
                                    r.Codigo += "H = H + 1; \n";
                                    r.Codigo += "#* E *#\n";
                                    r.Codigo += T4 + " = 101;\n";
                                    r.Codigo += "Heap[H] = " + T4 + ";\n";
                                    r.Codigo += "H = H + 1; \n";
                                    r.Codigo += "goto " + LS + ";\n";
                                    r.Codigo += LF + ":\n";
                                    r.Codigo += "#* FALSE *#\n";
                                    r.Codigo += "#* TF *#\n";
                                    r.Codigo += T5 + " = 102;\n";
                                    r.Codigo += "Heap[H] = " + T5 + ";\n";
                                    r.Codigo += "H = H + 1; \n";
                                    r.Codigo += "#* a *#\n";
                                    r.Codigo += T6 + " = 97;\n";
                                    r.Codigo += "Heap[H] = " + T6 + ";\n";
                                    r.Codigo += "H = H + 1; \n";
                                    r.Codigo += "#* L *#\n";
                                    r.Codigo += T7 + " = 108;\n";
                                    r.Codigo += "Heap[H] = " + T7 + ";\n";
                                    r.Codigo += "H = H + 1; \n";
                                    r.Codigo += "#* S *#\n";
                                    r.Codigo += T8 + " = 115;\n";
                                    r.Codigo += "Heap[H] = " + T8 + ";\n";
                                    r.Codigo += "H = H + 1; \n";
                                    r.Codigo += "#* E *#\n";
                                    r.Codigo += T9 + " = 101;\n";
                                    r.Codigo += "Heap[H] = " + T9 + ";\n";
                                    r.Codigo += "H = H + 1; \n";
                                    r.Codigo += LS + ":\n";
                                    r.Temporal = TH;
                                    r.Tipo = "string";
                                } else {
                                    let T1 = this.getTemporal();
                                    r.Codigo += T1 + " = H;\n";
                                    r.Codigo += r1.Codigo + " \n";
                                    r.Codigo += "Heap[" + T1 + "] = " + r1.Temporal + ";\n";
                                    r.Codigo += "H = H + 1; \n";
                                    r.Temporal = T1;
                                    r.Tipo = "string";
                                }


                            }


                            //ahora concatenamos el segundo elemento
                            if (r2.Tipo == "string") {
                                if (!r2.Variable) {
                                    if (r.Tipo == "integer") {
                                        let T1 = this.getTemporal();
                                        let T2 = this.getTemporal();
                                        let T3 = this.getTemporal();
                                        let T4 = this.getTemporal();
                                        let T5 = this.getTemporal();
                                        r.Codigo += r2.Codigo + "\n";
                                        r.Codigo += "";

                                    } else {
                                        // console.log(r2.Tipo);
                                        r.Codigo += r2.Codigo + " \n";
                                    }

                                } else {
                                    r.Tipo = "string";
                                    let labelV = this.getLabel();
                                    let labelF = this.getLabel();

                                    let labelR = this.getLabel();
                                    let TempgetValor = this.getTemporal();
                                    // String Tempaux =this.getTemporal();
                                    r.Codigo += r2.Codigo;
                                    r.Codigo += labelR + ": \n";
                                    r.Codigo += TempgetValor + " = Heap[" + r2.Temporal + "];\n";
                                    r.Codigo += "if (" + TempgetValor + " <> -1) goto " + labelV + ";\n";
                                    r.Codigo += "goto " + labelF + ";\n";
                                    r.Codigo += labelV + ": \n";
                                    r.Codigo += "Heap[H] = " + TempgetValor + ";\n";
                                    r.Codigo += "H = H + 1;\n";
                                    r.Codigo += r2.Temporal + " = " + r2.Temporal + " + 1;\n";
                                    r.Codigo += "goto " + labelR + ";\n";
                                    r.Codigo += labelF + ": \n";

                                }
                            } else {
                                if (r2.Tipo == "boolean") {
                                    let T1 = this.getTemporal();
                                    let T2 = this.getTemporal();
                                    let T3 = this.getTemporal();
                                    let T4 = this.getTemporal();
                                    let T5 = this.getTemporal();
                                    let T6 = this.getTemporal();
                                    let T7 = this.getTemporal();
                                    let T8 = this.getTemporal();
                                    let T9 = this.getTemporal();
                                    let TH = this.getTemporal();
                                    let LV = this.getLabel();
                                    let LF = this.getLabel();
                                    let LS = this.getLabel();
                                    r.Codigo = r2.Codigo + " \n";
                                    r.Codigo += TH + " = H;\n";
                                    r.Codigo += "#* Convertir boolean a String *#\n";
                                    r.Codigo += "if(" + r2.Temporal + " == 1) goto " + LV + ";\n";
                                    r.Codigo += "goto " + LF + ";\n";
                                    r.Codigo += LV + ":\n";
                                    r.Codigo += "#* TRUE *#\n";
                                    r.Codigo += "#* T *#\n";
                                    r.Codigo += T1 + " = 116;\n";
                                    r.Codigo += "Heap[H] = " + T1 + ";\n";
                                    r.Codigo += "H = H + 1; \n";
                                    r.Codigo += "#* R *#\n";
                                    r.Codigo += T2 + " = 114;\n";
                                    r.Codigo += "Heap[H] = " + T2 + ";\n";
                                    r.Codigo += "H = H + 1; \n";
                                    r.Codigo += "#* U *#\n";
                                    r.Codigo += T3 + " = 117;\n";
                                    r.Codigo += "Heap[H] = " + T3 + ";\n";
                                    r.Codigo += "H = H + 1; \n";
                                    r.Codigo += "#* E *#\n";
                                    r.Codigo += T4 + " = 101;\n";
                                    r.Codigo += "Heap[H] = " + T4 + ";\n";
                                    r.Codigo += "H = H + 1; \n";
                                    r.Codigo += "goto " + LS + ";\n";
                                    r.Codigo += LF + ":\n";
                                    r.Codigo += "#* FALSE *#\n";
                                    r.Codigo += "#* TF *#\n";
                                    r.Codigo += T5 + " = 102;\n";
                                    r.Codigo += "Heap[H] = " + T5 + ";\n";
                                    r.Codigo += "H = H + 1; \n";
                                    r.Codigo += "#* a *#\n";
                                    r.Codigo += T6 + " = 97;\n";
                                    r.Codigo += "Heap[H] = " + T6 + ";\n";
                                    r.Codigo += "H = H + 1; \n";
                                    r.Codigo += "#* L *#\n";
                                    r.Codigo += T7 + " = 108;\n";
                                    r.Codigo += "Heap[H] = " + T7 + ";\n";
                                    r.Codigo += "H = H + 1; \n";
                                    r.Codigo += "#* S *#\n";
                                    r.Codigo += T8 + " = 115;\n";
                                    r.Codigo += "Heap[H] = " + T8 + ";\n";
                                    r.Codigo += "H = H + 1; \n";
                                    r.Codigo += "#* E *#\n";
                                    r.Codigo += T9 + " = 101;\n";
                                    r.Codigo += "Heap[H] = " + T9 + ";\n";
                                    r.Codigo += "H = H + 1; \n";
                                    r.Codigo += LS + ":\n";
                                } else {
                                    r.Codigo += r2.Codigo + " \n";
                                    r.Codigo += "Heap[H] = " + r2.Temporal + ";\n";
                                    r.Codigo += "H = H + 1;\n";
                                }


                            }
                            return r;
                        case "integer":


                            let Ti = this.getTemporal();
                            r.Codigo += r1.Codigo + " \n";
                            r.Codigo += r2.Codigo + " \n";


                            r.Codigo += Ti + " = " + r1.Temporal + " + " + r2.Temporal + ";\n";
                            r.Temporal = Ti;
                            r.Tipo = "integer";
                            r.Variable = false;
                            return r;

                        case "double":

                            let Tr = this.getTemporal();
                            r.Codigo += r1.Codigo + " \n";
                            r.Codigo += r2.Codigo + " \n";

                            r.Codigo += Tr + " = " + r1.Temporal + " + " + r2.Temporal + ";\n";
                            r.Temporal = Tr;
                            r.Tipo = "double";

                            return r;
                        default:
                            return null;
                    }

                } else if (valor.hasOwnProperty("RESTA")) {
                    let r1 = this.evaluar_expresion(valor.RESTA.Valor1);
                    let r2 = this.evaluar_expresion(valor.RESTA.Valor2);
                    let TipoRes = this.getType(r1.Tipo, r2.Tipo, "-");

                    switch (TipoRes) {
                        case "integer":


                            let Ti = this.getTemporal();
                            r.Codigo += r1.Codigo + " \n";
                            r.Codigo += r2.Codigo + " \n";

                            r.Codigo += Ti + " = " + r1.Temporal + " - " + r2.Temporal + ";\n";
                            r.Temporal = Ti;
                            r.Tipo = "integer";
                            r.Variable = false;
                            return r;

                        case "double":

                            let Tr = this.getTemporal();
                            r.Codigo += r1.Codigo + " \n";
                            r.Codigo += r2.Codigo + " \n";

                            r.Codigo += Tr + " = " + r1.Temporal + " - " + r2.Temporal + ";\n";
                            r.Temporal = Tr;
                            r.Tipo = "double";

                            return r;
                        default:
                            return null;
                    }
                } else if (valor.hasOwnProperty("MULTIPLICACION")) {
                    let r1 = this.evaluar_expresion(valor.MULTIPLICACION.Valor1);
                    let r2 = this.evaluar_expresion(valor.MULTIPLICACION.Valor2);
                    let TipoRes = this.getType(r1.Tipo, r2.Tipo, "*");

                    switch (TipoRes) {
                        case "integer":


                            let Ti = this.getTemporal();
                            r.Codigo += r1.Codigo + " \n";
                            r.Codigo += r2.Codigo + " \n";

                            r.Codigo += Ti + " = " + r1.Temporal + " * " + r2.Temporal + ";\n";
                            r.Temporal = Ti;
                            r.Tipo = "integer";
                            r.Variable = false;
                            return r;

                        case "double":

                            let Tr = this.getTemporal();
                            r.Codigo += r1.Codigo + " \n";
                            r.Codigo += r2.Codigo + " \n";

                            r.Codigo += Tr + " = " + r1.Temporal + " * " + r2.Temporal + ";\n";
                            r.Temporal = Tr;
                            r.Tipo = "double";

                            return r;
                        default:
                            return null;
                    }
                } else if (valor.hasOwnProperty("DIVISION")) {
                    let r1 = this.evaluar_expresion(valor.DIVISION.Valor1);
                    let r2 = this.evaluar_expresion(valor.DIVISION.Valor2);
                    let TipoRes = this.getType(r1.Tipo, r2.Tipo, "/");

                    switch (TipoRes) {
                        case "integer":


                            let Ti = this.getTemporal();
                            r.Codigo += r1.Codigo + " \n";
                            r.Codigo += r2.Codigo + " \n";

                            r.Codigo += Ti + " = " + r1.Temporal + " / " + r2.Temporal + ";\n";
                            r.Temporal = Ti;
                            r.Tipo = "integer";
                            r.Variable = false;
                            return r;

                        case "double":

                            let Tr = this.getTemporal();
                            r.Codigo += r1.Codigo + " \n";
                            r.Codigo += r2.Codigo + " \n";

                            r.Codigo += Tr + " = " + r1.Temporal + " / " + r2.Temporal + ";\n";
                            r.Temporal = Tr;
                            r.Tipo = "double";

                            return r;
                        default:
                            return null;
                    }
                } else if (valor.hasOwnProperty("POTENCIA")) {
                    let r1 = this.evaluar_expresion(valor.POTENCIA.Valor1);
                    let r2 = this.evaluar_expresion(valor.POTENCIA.Valor2);
                    let TipoRes = this.getType(r1.Tipo, r2.Tipo, "^^");

                    switch (TipoRes) {
                        case "integer":

                            let T1 = this.getTemporal();
                            let T2 = this.getTemporal();
                            let L1 = this.getLabel();
                            let L2 = this.getLabel();
                            r.Codigo = r1.Codigo + " \n";
                            r.Codigo += r2.Codigo + " \n";

                            r.Codigo += T1 + " = 1;\n";
                            r.Codigo += T2 + " = 1;\n";

                            r.Codigo += L1 + ": \n";
                            r.Codigo += T1 + " = " + T1 + " + 1;\n";
                            r.Codigo += T2 + " = " + T2 + " * " + r1.Temporal + ";\n";
                            r.Codigo += "if (" + T1 + " <= " + r2.Temporal + ") goto " + L1 + ";\n";
                            r.Codigo += "goto " + L2 + ";\n";

                            r.Codigo += L2 + ": \n";
                            r.Temporal = T2;
                            r.Tipo = "integer";

                            return r;

                        case "double":

                            let Tr1 = this.getTemporal();
                            let Tr2 = this.getTemporal();
                            let Lr1 = this.getLabel();
                            let Lr2 = this.getLabel();
                            r.Codigo = r1.Codigo + " \n";
                            r.Codigo += r2.Codigo + " \n";

                            r.Codigo += Tr1 + " = 1;\n";
                            r.Codigo += Tr2 + " = 1;\n";

                            r.Codigo += Lr1 + ": \n";
                            r.Codigo += Tr1 + " = " + Tr1 + " + 1;\n";
                            r.Codigo += Tr2 + " = " + Tr2 + " * " + r1.Temporal + ";\n";
                            r.Codigo += "if (" + Tr1 + " <= " + r2.Temporal + ") goto " + Lr1 + ";\n";
                            r.Codigo += "goto " + Lr2 + ";\n";

                            r.Codigo += Lr2 + ": \n";
                            r.Temporal = Tr2;
                            r.Tipo = "integer";
                        default:
                            return null;
                    }
                } else if (valor.hasOwnProperty("MODULAR")) {
                    let r1 = this.evaluar_expresion(valor.MODULAR.Valor1);
                    let r2 = this.evaluar_expresion(valor.MODULAR.Valor2);
                    let TipoRes = this.getType(r1.Tipo, r2.Tipo, "%");

                    switch (TipoRes) {
                        case "integer":


                            let Ti = this.getTemporal();
                            r.Codigo += r1.Codigo + " \n";
                            r.Codigo += r2.Codigo + " \n";

                            r.Codigo += Ti + " = " + r1.Temporal + " % " + r2.Temporal + ";\n";
                            r.Temporal = Ti;
                            r.Tipo = "integer";
                            r.Variable = false;
                            return r;

                        case "double":

                            let Tr = this.getTemporal();
                            r.Codigo += r1.Codigo + " \n";
                            r.Codigo += r2.Codigo + " \n";

                            r.Codigo += Tr + " = " + r1.Temporal + " % " + r2.Temporal + ";\n";
                            r.Temporal = Tr;
                            r.Tipo = "double";

                            return r;
                        default:
                            return null;
                    }
                } else if (valor.hasOwnProperty("IGUALDAD")) {
                    let EtiqVje = this.getLabel();
                    let EtiqFje = this.getLabel();
                    //      console.log(valor)
                    let r1 = this.evaluar_expresion(valor.IGUALDAD.Valor1);
                    let r2 = this.evaluar_expresion(valor.IGUALDAD.Valor2);

                    let TipoRes = this.getType(r1.Tipo, r2.Tipo, "===");
                    switch (TipoRes) {
                        case "boolean":
                            if (r1.Tipo == "string" && r2.Tipo == "string") {
                                r.Codigo = "";
                                r.Codigo += r1.Codigo;
                                if (!r1.variable) {
                                    r.Codigo += "Heap[H] = -1;\n";
                                    r.Codigo += "H = H + 1;\n";
                                }

                                let Tinicio1 = this.getTemporal();
                                r.Codigo += Tinicio1 + " = " + r1.Temporal + ";\n";
                                let Tinicio2 = this.getTemporal();
                                r.Codigo += Tinicio2 + " = " + r2.Temporal + ";\n";;
                                r.Codigo += "if(" + Tinicio1 + " == " + Tinicio2 + ") goto " + EtiqVje + ";\n"
                                r.Codigo += "goto " + EtiqFje + ";\n"
                                r.EtiquetasV.push(EtiqVje);
                                r.EtiquetasF.push(EtiqFje);
                                r.Temporal = " ";
                                r.Tipo = "boolean";
                            } else {
                                r.Codigo = r1.Codigo + " \n";
                                r.Codigo += r2.Codigo + " \n";
                                r.Codigo += "if (" + r1.Temporal + " == " + r2.Temporal + ") goto " + EtiqVje + ";\n";
                                r.Codigo += "goto " + EtiqFje + ";\n";
                                r.Temporal = "";
                                r.Tipo = "boolean";
                                r1.EtiquetasV.forEach(ev => {
                                    r.EtiquetasV.push(ev);
                                });

                                r2.EtiquetasV.forEach(ev => {
                                    r.EtiquetasV.push(ev);
                                });

                                r.EtiquetasV.push(EtiqVje);
                                r1.EtiquetasF.forEach(ef => {
                                    r.EtiquetasF.push(ef);
                                });
                                r2.EtiquetasF.forEach(ef => {
                                    r.EtiquetasF.push(ef);
                                });

                                r.EtiquetasF.push(EtiqFje);

                            }

                            return r;
                        default:
                            return null;
                    }
                } else if (valor.hasOwnProperty("IGUAL_IGUAL")) {

                    let EtiqVje = this.getLabel();
                    let EtiqFje = this.getLabel();
                    //      console.log(valor)
                    let r1 = this.evaluar_expresion(valor.IGUAL_IGUAL.Valor1);
                    let r2 = this.evaluar_expresion(valor.IGUAL_IGUAL.Valor2);
                    let TipoRes = this.getType(r1.Tipo, r2.Tipo, "==");
                    switch (TipoRes) {
                        case "boolean":
                            if (r1.Tipo == "string" && r2.Tipo == "string") {

                                r.Codigo = "";
                                r.Codigo += r1.Codigo;
                                if (!r1.variable) {
                                    r.Codigo += "Heap[H] = -1;\n";
                                    r.Codigo += "H = H + 1;\n";
                                }

                                let Tinicio1 = this.getTemporal();
                                r.Codigo += Tinicio1 + " = " + r1.Temporal + ";\n";
                                let TvalorActual1 = this.getTemporal();
                                r.Codigo += TvalorActual1 + " = Heap[" + r1.Temporal + "];\n";
                                let Tcont1 = this.getTemporal();

                                let LV1 = this.getLabel();
                                let LF1 = this.getLabel();
                                let LR1 = this.getLabel();
                                r.Codigo += Tcont1 + " = 0;\n";
                                r.Codigo += LR1 + ": \n";
                                r.Codigo += "if (" + TvalorActual1 + " <> -1) goto " + LV1 + ";\n";
                                r.Codigo += "goto " + LF1 + ";\n";
                                r.Codigo += LV1 + ": \n";

                                r.Codigo += Tcont1 + " = " + Tcont1 + "+ 1;\n";
                                r.Codigo += r1.Temporal + " = " + r1.Temporal + "+ 1;\n";
                                r.Codigo += TvalorActual1 + " = Heap[" + r1.Temporal + "];\n";
                                r.Codigo += "goto " + LR1 + ";\n";
                                r.Codigo += LF1 + ": \n";

                                r.Codigo += r2.Codigo;
                                if (!r2.variable) {
                                    r.Codigo += "Heap[H] = -1;\n";
                                    r.Codigo += "H = H + 1; \n";
                                }

                                let Tinicio2 = this.getTemporal();
                                r.Codigo += Tinicio2 + " = " + r2.Temporal + ";\n";;
                                let TvalorActual2 = this.getTemporal();
                                r.Codigo += TvalorActual2 + " = Heap[" + r2.Temporal + "];\n";
                                let Tcont2 = this.getTemporal();

                                let LV2 = this.getLabel();
                                let LF2 = this.getLabel();
                                let LR2 = this.getLabel();
                                r.Codigo += Tcont2 + " = 0;\n";
                                r.Codigo += LR2 + ": \n";
                                r.Codigo += "if (" + TvalorActual2 + "<> -1) goto " + LV2 + ";\n";
                                r.Codigo += "goto " + LF2 + ";\n";
                                r.Codigo += LV2 + ": \n";
                                r.Codigo += Tcont2 + " = " + Tcont2 + " + 1;\n";
                                r.Codigo += r2.Temporal + " = " + r2.Temporal + " + 1;\n";
                                r.Codigo += TvalorActual2 + " = Heap[" + r2.Temporal + "];\n";
                                r.Codigo += "goto " + LR2 + ";\n";
                                r.Codigo += LF2 + ": \n";

                                // ahora igualamos la cantidad de datos
                                let TResCont = this.getTemporal();
                                let LVCont = this.getLabel();
                                let LFCont = this.getLabel();

                                r.Codigo += "if (" + Tcont1 + " == " + Tcont2 + ") goto " + LVCont + ";\n";
                                r.Codigo += "goto " + LFCont + ";\n";
                                // hasta aca es para verificar si tienen la misma cantidad de caracteres

                                // r.Codigo += "//aqui empieza a ver los valores de la cadena \n";

                                r.Codigo += LVCont + ": \n";
                                r.Codigo += TvalorActual1 + " = Heap[" + Tinicio1 + "];\n";
                                r.Codigo += TvalorActual2 + " = Heap[" + Tinicio2 + "];\n";

                                let LV_1 = this.getLabel();
                                let LV_2 = this.getLabel();
                                let LF_2 = this.getLabel();
                                let LVVal = this.getLabel();
                                let LFVal = this.getLabel();
                                let LR_1 = this.getLabel();
                                r.Codigo += TResCont + " = 0;\n";
                                r.Codigo += LR_1 + ": \n";
                                r.Codigo += "if (" + TvalorActual1 + " <> -1) goto " + LV_1 + ";\n";
                                r.Codigo += "goto " + LF_2 + ";\n";
                                r.Codigo += LV_1 + ": \n";
                                r.Codigo += "if (" + TvalorActual2 + " <> -1) goto " + LV_2 + ";\n";;
                                r.Codigo += "goto " + LF_2 + ";\n";
                                r.Codigo += LV_2 + ": \n";
                                r.Codigo += "if (" + TvalorActual1 + " == " + TvalorActual2 + ") goto " + LVVal + ";\n";
                                r.Codigo += "goto " + LFVal + ";\n";
                                r.Codigo += LVVal + ": \n";
                                r.Codigo += Tinicio1 + " = " + Tinicio1 + " + 1;\n";
                                r.Codigo += TvalorActual1 + " = Heap[" + Tinicio1 + "];\n";
                                r.Codigo += Tinicio2 + " = " + Tinicio2 + " + 2;\n"
                                r.Codigo += TvalorActual2 + " = Heap[" + Tinicio2 + "];\n";
                                r.Codigo += "goto " + LR_1 + ";\n";
                                r.Codigo += LFVal + ": \n";
                                r.Codigo += TResCont + " = 1;\n";
                                r.Codigo += LF_2 + ": \n";
                                let LVFin = this.getLabel();
                                let LFFin = this.getLabel();
                                r.Codigo += " if(" + TResCont + " == 1) goto " + LVFin + ";\n";
                                r.Codigo += "goto " + LFFin + ";\n";

                                r.EtiquetasV.push(LVFin);
                                r.EtiquetasF.push(LFFin);
                                r.EtiquetasF.push(LFCont);

                                r.Temporal = " ";
                                r.Tipo = "boolean";
                            } else {

                                r.Codigo = r1.Codigo + " \n";
                                r.Codigo += r2.Codigo + " \n";
                                r.Codigo += "if (" + r1.Temporal + " == " + r2.Temporal + ") goto " + EtiqVje + ";\n";
                                r.Codigo += "goto " + EtiqFje + ";\n";
                                r.Temporal = "";
                                r.Tipo = "boolean";
                                r1.EtiquetasV.forEach(ev => {
                                    r.EtiquetasV.push(ev);
                                });

                                r2.EtiquetasV.forEach(ev => {
                                    r.EtiquetasV.push(ev);
                                });

                                r.EtiquetasV.push(EtiqVje);
                                r1.EtiquetasF.forEach(ef => {
                                    r.EtiquetasF.push(ef);
                                });
                                r2.EtiquetasF.forEach(ef => {
                                    r.EtiquetasF.push(ef);
                                });

                                r.EtiquetasF.push(EtiqFje);

                            }

                            return r;
                        default:
                            return null;
                    }
                } else if (valor.hasOwnProperty("NOT_EQUAL")) {
                    let EtiqVje = this.getLabel();
                    let EtiqFje = this.getLabel();
                    let r1 = this.evaluar_expresion(valor.NOT_EQUAL.Valor1);
                    let r2 = this.evaluar_expresion(valor.NOT_EQUAL.Valor2);
                    let TipoRes = this.getType(r1.Tipo, r2.Tipo, "!=");
                    switch (TipoRes) {

                        case "boolean":
                            if (r1.Tipo == "string" || r2.Tipo == "string") {

                            } else {
                                r.Codigo = r1.Codigo + " \n";
                                r.Codigo += r2.Codigo + " \n";
                                r.Codigo += "if (" + r1.Temporal + " <> " + r2.Temporal + ") goto " + EtiqVje + ";\n";
                                r.Codigo += "goto " + EtiqFje + ";\n";
                                r.Temporal = "";
                                r.Tipo = "boolean";

                                r1.EtiquetasV.forEach(ev => {
                                    r.EtiquetasV.push(ev);
                                });

                                r2.EtiquetasV.forEach(ev => {
                                    r.EtiquetasV.push(ev);
                                });

                                r.EtiquetasV.push(EtiqVje);
                                r1.EtiquetasF.forEach(ef => {
                                    r.EtiquetasF.push(ef);
                                });
                                r2.EtiquetasF.forEach(ef => {
                                    r.EtiquetasF.push(ef);
                                });

                                r.EtiquetasF.push(EtiqFje);

                            }

                            return r;
                        default:
                            return null;
                    }


                } else if (valor.hasOwnProperty("MAYOR")) {

                    let EtiqVje = this.getLabel();
                    let EtiqFje = this.getLabel();
                    let r1 = this.evaluar_expresion(valor.MAYOR.Valor1);
                    let r2 = this.evaluar_expresion(valor.MAYOR.Valor2);
                    let TipoRes = this.getType(r1.Tipo, r2.Tipo, ">");
                    switch (TipoRes) {

                        case "boolean":
                            if (r1.Tipo == "string" || r2.Tipo == "string") {

                            } else {
                                r.Codigo = r1.Codigo + " \n";
                                r.Codigo += r2.Codigo + " \n";
                                r.Codigo += "if(" + r1.Temporal + " > " + r2.Temporal + ") goto " + EtiqVje + ";\n";
                                r.Codigo += "goto " + EtiqFje + ";\n";
                                r.Temporal = "";
                                r.Tipo = "boolean";
                                r1.EtiquetasV.forEach(ev => {
                                    r.EtiquetasV.push(ev);
                                });

                                r2.EtiquetasV.forEach(ev => {
                                    r.EtiquetasV.push(ev);
                                });

                                r.EtiquetasV.push(EtiqVje);
                                r1.EtiquetasF.forEach(ef => {
                                    r.EtiquetasF.push(ef);
                                });
                                r2.EtiquetasF.forEach(ef => {
                                    r.EtiquetasF.push(ef);
                                });

                                r.EtiquetasF.push(EtiqFje);
                            }

                            return r;
                        default:
                            return null;
                    }
                } else if (valor.hasOwnProperty("MENOR")) {
                    let EtiqVje = this.getLabel();
                    let EtiqFje = this.getLabel();
                    let r1 = this.evaluar_expresion(valor.MENOR.Valor1);
                    let r2 = this.evaluar_expresion(valor.MENOR.Valor2);
                    let TipoRes = this.getType(r1.Tipo, r2.Tipo, "<");
                    switch (TipoRes) {

                        case "boolean":
                            if (r1.Tipo == "string" || r2.Tipo == "string") {

                            } else {
                                r.Codigo = r1.Codigo + " \n";
                                r.Codigo += r2.Codigo + " \n";
                                r.Codigo += "if(" + r1.Temporal + " < " + r2.Temporal + ") goto " + EtiqVje + ";\n";
                                r.Codigo += "goto " + EtiqFje + ";\n";
                                r.Temporal = "";
                                r.Tipo = "boolean";
                                r1.EtiquetasV.forEach(ev => {
                                    r.EtiquetasV.push(ev);
                                });

                                r2.EtiquetasV.forEach(ev => {
                                    r.EtiquetasV.push(ev);
                                });

                                r.EtiquetasV.push(EtiqVje);
                                r1.EtiquetasF.forEach(ef => {
                                    r.EtiquetasF.push(ef);
                                });
                                r2.EtiquetasF.forEach(ef => {
                                    r.EtiquetasF.push(ef);
                                });

                                r.EtiquetasF.push(EtiqFje);
                            }

                            return r;
                        default:
                            return null;
                    }
                } else if (valor.hasOwnProperty("MAYOR_IGUAL")) {
                    let EtiqVje = this.getLabel();
                    let EtiqFje = this.getLabel();
                    let r1 = this.evaluar_expresion(valor.MAYOR_IGUAL.Valor1);
                    let r2 = this.evaluar_expresion(valor.MAYOR_IGUAL.Valor2);
                    let TipoRes = this.getType(r1.Tipo, r2.Tipo, ">=");
                    switch (TipoRes) {

                        case "boolean":
                            if (r1.Tipo == "string" || r2.Tipo == "string") {

                            } else {
                                r.Codigo = r1.Codigo + " \n";
                                r.Codigo += r2.Codigo + " \n";
                                r.Codigo += "if(" + r1.Temporal + " >= " + r2.Temporal + ") goto " + EtiqVje + ";\n";
                                r.Codigo += "goto " + EtiqFje + ";\n";
                                r.Temporal = "";
                                r.Tipo = "boolean";
                                r1.EtiquetasV.forEach(ev => {
                                    r.EtiquetasV.push(ev);
                                });

                                r2.EtiquetasV.forEach(ev => {
                                    r.EtiquetasV.push(ev);
                                });

                                r.EtiquetasV.push(EtiqVje);
                                r1.EtiquetasF.forEach(ef => {
                                    r.EtiquetasF.push(ef);
                                });
                                r2.EtiquetasF.forEach(ef => {
                                    r.EtiquetasF.push(ef);
                                });

                                r.EtiquetasF.push(EtiqFje);
                            }

                            return r;
                        default:
                            return null;
                    }
                } else if (valor.hasOwnProperty("MENOR_IGUAL")) {
                    let EtiqVje = this.getLabel();
                    let EtiqFje = this.getLabel();
                    let r1 = this.evaluar_expresion(valor.MENOR_IGUAL.Valor1);
                    let r2 = this.evaluar_expresion(valor.MENOR_IGUAL.Valor2);
                    let TipoRes = this.getType(r1.Tipo, r2.Tipo, "<=");
                    switch (TipoRes) {

                        case "boolean":
                            if (r1.Tipo == "string" || r2.Tipo == "string") {

                            } else {
                                r.Codigo = r1.Codigo + " \n";
                                r.Codigo += r2.Codigo + " \n";
                                r.Codigo += "if(" + r1.Temporal + " <= " + r2.Temporal + ") goto " + EtiqVje + ";\n";
                                r.Codigo += "goto " + EtiqFje + ";\n";
                                r.Temporal = "";
                                r.Tipo = "boolean";

                                r1.EtiquetasV.forEach(ev => {
                                    r.EtiquetasV.push(ev);
                                });

                                r2.EtiquetasV.forEach(ev => {
                                    r.EtiquetasV.push(ev);
                                });

                                r.EtiquetasV.push(EtiqVje);
                                r1.EtiquetasF.forEach(ef => {
                                    r.EtiquetasF.push(ef);
                                });
                                r2.EtiquetasF.forEach(ef => {
                                    r.EtiquetasF.push(ef);
                                });

                                r.EtiquetasF.push(EtiqFje);
                            }

                            return r;
                        default:
                            return null;
                    }
                } else if (valor.hasOwnProperty("AND")) {

                    let EtiqFje = this.getLabel();
                    let r1 = this.evaluar_expresion(valor.AND.Valor1);
                    let r2 = this.evaluar_expresion(valor.AND.Valor2);
                    let TipoRes = this.getType(r1.Tipo, r2.Tipo, "&&");
                    switch (TipoRes) {

                        case "boolean":
                            if (r1.Tipo == "boolean" && r2.Tipo == "boolean") {
                                if (r1.Temporal == "") {
                                    r1.EtiquetasF.forEach(ef => {
                                        let a = r1.Codigo.replace(ef, EtiqFje);
                                        r1.Codigo = a;
                                    });

                                }
                                r.Codigo += "# Comienza AND\n"
                                r.Codigo += r1.Codigo + " \n";
                                let LV1 = this.getLabel();

                                let LV2 = this.getLabel();

                                if (r1.Temporal == "") {
                                    r1.EtiquetasV.forEach(ev => {
                                        r.Codigo += ev + ": \n";
                                    });
                                } else {
                                    r.Codigo += "if(" + r1.Temporal + "== 1) goto " + LV1 + ";\n";
                                    r.Codigo += "goto " + EtiqFje + ";\n";
                                    r.Codigo += LV1 + ": \n";
                                }

                                if (r2.Temporal == "") {
                                    r2.EtiquetasF.forEach(ef => {
                                        let a = r2.Codigo.replace(ef, EtiqFje);
                                        r2.Codigo = a;
                                    });
                                }
                                r.Codigo += r2.Codigo + " \n";

                                if (r2.Temporal == "") {
                                    r.EtiquetasV = r2.EtiquetasV;
                                } else {
                                    r.Codigo += "if(" + r2.Temporal + "== 1) goto " + LV2 + ";\n";
                                    r.Codigo += "goto " + EtiqFje + ";\n";
                                    r.EtiquetasV.push(LV2);
                                }
                                r.Codigo += "# TERMINA AND\n"
                                r1.EtiquetasF.forEach(ef => {
                                    r.EtiquetasF.push(ef);
                                });
                                r2.EtiquetasF.forEach(ef => {
                                    r.EtiquetasF.push(ef);
                                });


                                r.EtiquetasF.forEach(ef => {
                                    r.Codigo = r.Codigo.replace(ef + ";", EtiqFje + ";");
                                });

                                r.EtiquetasF = [EtiqFje];
                                r.Temporal = "";
                                r.Tipo = "boolean";
                                return r;
                            }

                            return r;
                        default:
                            return null;
                    }
                } else if (valor.hasOwnProperty("OR")) {
                    let r1 = this.evaluar_expresion(valor.OR.Valor1);
                    let r2 = this.evaluar_expresion(valor.OR.Valor2);
                    let TipoRes = this.getType(r1.Tipo, r2.Tipo, "||");
                    switch (TipoRes) {

                        case "boolean":
                            r.Codigo += "# Comienza OR\n"
                            let LV2 = this.getLabel();
                            if (r1.Tipo == "boolean" && r2.Tipo == "boolean") {


                                r.Codigo += r1.Codigo + " \n";
                                let LV1 = this.getLabel();
                                let LF1 = this.getLabel();

                                let LF2 = this.getLabel();
                                if (r1.Temporal == "") {
                                    r1.EtiquetasF.forEach(ef => {
                                        r.Codigo += ef + ": \n";
                                    });
                                } else {

                                    r.Codigo += "if(" + r1.Temporal + "== 1) goto " + LV2 + ";\n";
                                    r.Codigo += "goto " + LF1 + ";\n";
                                    r.Codigo += LF1 + ":\n";

                                }


                                r.Codigo += r2.Codigo + " \n";

                                if (r2.Temporal == "") {
                                    r2.EtiquetasF.forEach(ef => {
                                        r.EtiquetasF.push(ef);
                                    });
                                } else {
                                    r.Codigo += "if(" + r2.Temporal + "== 1) goto " + LV2 + ";\n";
                                    r.Codigo += "goto " + LF2 + ";\n";
                                    r.EtiquetasF.push(LF2);
                                }

                                r.Codigo += "# TERMINA OR\n"

                                r1.EtiquetasV.forEach(ef => {
                                    r.EtiquetasV.push(ef);
                                });
                                r2.EtiquetasV.forEach(ef => {
                                    r.EtiquetasV.push(ef);
                                });
                                r.EtiquetasV.forEach(ev => {
                                    let a = r.Codigo.replace(ev, LV2);
                                    r.Codigo = a;
                                });
                                r.EtiquetasV.push(LV2);

                                r.Temporal = "";
                                r.Tipo = "boolean";
                                return r;


                            }
                        default:
                            return null;
                    }

                } else if (valor.hasOwnProperty("XOR")) {
                    let EtiqVje = this.getLabel();
                    let EtiqFje = this.getLabel();
                    let r1 = this.evaluar_expresion(valor.XOR.Valor1);
                    let r2 = this.evaluar_expresion(valor.XOR.Valor2);
                    let TipoRes = this.getType(r1.Tipo, r2.Tipo, "^");

                    switch (TipoRes) {

                        case "boolean":

                            if (r1.Tipo == "boolean" && r2.Tipo == "boolean") {
                                let T1 = this.getTemporal();
                                r.Codigo = r1.Codigo + " \n";
                                if (r1.Temporal == "") {

                                    r1.EtiquetasV.forEach(ev => {
                                        r.Codigo += ev + ": \n";
                                    });

                                    r.Codigo += T1 + " = 1; \n";
                                    let LS1 = this.getLabel();
                                    r.Codigo += "goto " + LS1 + "; \n";
                                    r1.EtiquetasF.forEach(ef => {
                                        r.Codigo += ef + ": \n";
                                    });
                                    r.Codigo += T1 + " = 0; \n";
                                    r.Codigo += LS1 + ":\n";
                                } else {
                                    r.Codigo += T1 + " = " + r1.Temporal + " ;\n";
                                }

                                let T2 = this.getTemporal();
                                r.Codigo += r2.Codigo + " \n";
                                if (r2.Temporal == "") {

                                    r2.EtiquetasV.forEach(ev => {
                                        r.Codigo += ev + ": \n";
                                    });

                                    r.Codigo += T2 + " = 1; \n";
                                    let LS2 = this.getLabel();
                                    r.Codigo += "goto " + LS2 + "; \n";
                                    r2.EtiquetasF.forEach(ef => {
                                        r.Codigo += ef + ": \n";
                                    });
                                    r.Codigo += T2 + " = 0; \n";
                                    r.Codigo += LS2 + ":\n";

                                } else {
                                    r.Codigo += T2 + " = " + r2.Temporal + " ;\n";
                                }


                                let LV = this.getLabel();
                                let LF = this.getLabel();
                                r.Codigo += "if(" + T1 + " <> " + T2 + ") goto " + LV + ";\n";
                                r.Codigo += "goto " + LF + "; \n";


                                r.EtiquetasV.push(LV);
                                r.EtiquetasF.push(LF);
                                r.Temporal = "";
                                r.Tipo = "boolean";
                                return r;



                            }
                        default:
                            return null;
                    }

                } else if (valor.hasOwnProperty("Casteo")) {

                    let cast = valor.Casteo;
                    let Valor = this.evaluar_expresion(cast.Valor);

                    if (Valor != null) {
                        if (cast.Tipo.toLowerCase() == "integer" && Valor.Tipo == "double") {
                            r.Tipo = "integer";
                            r.Codigo = Valor.Codigo;
                            r.TipoArreglo = Valor.TipoArreglo;
                            r.Temporal = Valor.Temporal;
                            console.log(r)
                            return r;
                        } else if (cast.Tipo.toLowerCase() == "char" && Valor.Tipo == "integer") {
                            r.Tipo = "char";
                            r.Codigo = Valor.Codigo;
                            r.TipoArreglo = Valor.TipoArreglo;
                            r.Temporal = Valor.Temporal;
                            return r;
                        } else if (cast.Tipo.toLowerCase() == "char" && Valor.Tipo == "double") {
                            r.Tipo = "char";
                            r.Codigo = Valor.Codigo;
                            r.TipoArreglo = Valor.TipoArreglo;
                            r.Temporal = Valor.Temporal;
                            return r;
                        } else if (cast.Tipo.toLowerCase() == "integer" && Valor.Tipo == "integer") {
                            r.Tipo = "integer";
                            r.Codigo = Valor.Codigo;
                            r.TipoArreglo = Valor.TipoArreglo;
                            r.Temporal = Valor.Temporal;
                            console.log(r)
                            return r;
                        } else if (cast.Tipo.toLowerCase() == "char" && Valor.Tipo == "char") {
                            r.Tipo = "char";
                            r.Codigo = Valor.Codigo;
                            r.TipoArreglo = Valor.TipoArreglo;
                            r.Temporal = Valor.Temporal;
                            return r;
                        } else if (cast.Tipo.toLowerCase() == "double" && Valor.Tipo == "double") {
                            r.Tipo = "double";
                            r.Codigo = Valor.Codigo;
                            r.TipoArreglo = Valor.TipoArreglo;
                            r.Temporal = Valor.Temporal;
                            return r;
                        }

                    } else {

                        this.Errores.push({
                            error: {
                                No: this.Errores.length,
                                description: JSON.stringify(valor),
                                linea: 0,
                                columna: 0,
                                tipo: "Semantico"
                            }
                        })
                    }


                } else {

                    this.Errores.push({
                        error: {
                            No: this.Errores.length,
                            description: JSON.stringify(valor),
                            linea: 0,
                            columna: 0,
                            tipo: "Semantico"
                        }
                    })

                }


                return r;
            }
        }
        /**
         * 
         */
    getTipoVariable(valor) {

        if (typeof valor === 'object') {
            return valor.Tipo;
        } else if (Number.isInteger(valor)) {
            return "integer";
        } else if (typeof valor === "number") {
            return "double";
        } else if (typeof(valor) === 'boolean') {
            return "boolean";
        } else if (typeof(valor) === 'string') {
            return "string";
        } else {
            this.Errores.push({
                error: {
                    No: this.Errores.length,
                    description: "variable no valida",
                    linea: 0,
                    columna: 0,
                    tipo: "Semantico"
                }
            })
            return null;
        }
    }

    getType(Type, Type1, Sign) {

        if (Sign == "+") { // Caso Suma
            if (Type == "integer" && Type1 == "double") {
                return "double";
            } else if (Type == "double" && Type1 == "integer") {
                return "double";
            } else if (Type == "double" && Type1 == "char") {
                return "double";
            } else if (Type == "char" && Type1 == "double") {
                return "double";
            } else if (Type == "double" && Type1 == "double") {
                return "double";
            } else if (Type == "integer" && Type1 == "char") {
                return "integer";
            } else if (Type == "char" && Type1 == "integer") {
                return "integer";
            } else if (Type == "integer" && Type1 == "integer") {
                return "integer";
            } else if (Type == "char" && Type1 == "char") {
                return "char";
            } else if (Type == "string" && Type1 == "integer") {
                return "string";
            } else if (Type == "integer" && Type1 == "string") {
                return "string";
            } else if (Type == "string" && Type1 == "char") {
                return "string";
            } else if (Type == "char" && Type1 == "string") {
                return "string";
            } else if (Type == "string" && Type1 == "double") {
                return "string";
            } else if (Type == "double" && Type1 == "string") {
                return "string";
            } else if (Type == "string" && Type1 == "boolean") {
                return "string";
            } else if (Type == "boolean" && Type1 == "string") {
                return "string";
            } else if (Type == "string" && Type1 == "string") {
                return "string";
            }

        } else if (Sign == "-") // Caso Resta
        {
            if (Type == "integer" && Type1 == "double") {
                return "double";
            } else if (Type == "double" && Type1 == "integer") {
                return "double";
            } else if (Type == "double" && Type1 == "char") {
                return "double";
            } else if (Type == "char" && Type1 == "double") {
                return "double";
            } else if (Type == "double" && Type1 == "double") {
                return "double";
            } else if (Type == "integer" && Type1 == "char") {
                return "integer";
            } else if (Type == "char" && Type1 == "integer") {
                return "integer";
            } else if (Type == "integer" && Type1 == "integer") {
                return "integer";
            } else if (Type == "char" && Type1 == "char") {
                return "integer";
            }

        } else if (Sign == "*") // Caso Multiplicacion
        {
            if (Type == "integer" && Type1 == "double") {
                return "double";
            } else if (Type == "double" && Type1 == "integer") {
                return "double";
            } else if (Type == "double" && Type1 == "char") {
                return "double";
            } else if (Type == "char" && Type1 == "double") {
                return "double";
            } else if (Type == "double" && Type1 == "double") {
                return "double";
            } else if (Type == "integer" && Type1 == "char") {
                return "integer";
            } else if (Type == "char" && Type1 == "integer") {
                return "integer";
            } else if (Type == "integer" && Type1 == "integer") {
                return "integer";
            } else if (Type == "char" && Type1 == "char") {
                return "integer";
            }

        } else if (Sign == "/") // Caso Division
        {

            if (Type == "integer" && Type1 == "double") {
                return "double";
            } else if (Type == "double" && Type1 == "integer") {
                return "double";
            } else if (Type == "double" && Type1 == "char") {
                return "double";
            } else if (Type == "char" && Type1 == "double") {
                return "double";
            } else if (Type == "double" && Type1 == "double") {
                return "double";
            } else if (Type == "integer" && Type1 == "char") {
                return "integer";
            } else if (Type == "char" && Type1 == "integer") {
                return "integer";
            } else if (Type == "integer" && Type1 == "integer") {
                return "integer";
            } else if (Type == "char" && Type1 == "char") {
                return "integer";
            }

        } else if (Sign == "%") // Caso Modular
        {

            if (Type == "integer" && Type1 == "double") {
                return "double";
            } else if (Type == "double" && Type1 == "integer") {
                return "double";
            } else if (Type == "double" && Type1 == "char") {
                return "double";
            } else if (Type == "char" && Type1 == "double") {
                return "double";
            } else if (Type == "double" && Type1 == "double") {
                return "double";
            } else if (Type == "integer" && Type1 == "char") {
                return "integer";
            } else if (Type == "char" && Type1 == "integer") {
                return "integer";
            } else if (Type == "integer" && Type1 == "integer") {
                return "integer";
            } else if (Type == "char" && Type1 == "char") {
                return "integer";
            }

        } else if (Sign == "^^") // Caso POW() <------
        {
            if (Type == "integer" && Type1 == "double") {
                return "double";
            } else if (Type == "double" && Type1 == "integer") {
                return "double";
            } else if (Type == "double" && Type1 == "char") {
                return "double";
            } else if (Type == "char" && Type1 == "double") {
                return "double";
            } else if (Type == "double" && Type1 == "double") {
                return "double";
            } else if (Type == "integer" && Type1 == "char") {
                return "integer";
            } else if (Type == "char" && Type1 == "integer") {
                return "integer";
            } else if (Type == "integer" && Type1 == "integer") {
                return "integer";
            } else if (Type == "char" && Type1 == "char") {
                return "integer";
            }

        } else if (Sign == "||" || Sign == "^" || Sign == "&&") {
            if (Type == "boolean" && Type1 == "boolean") {
                return "boolean";
            }
        } else if ((Sign == "!") && (Type == "boolean") && (Type1 == "")) {
            return "boolean";
        } else if (Sign == "==" || Sign == "===" || Sign == "!=" || Sign == "<" || Sign == ">" || Sign == "<=" || Sign == ">=") {
            return "boolean";
        }
        this.Errores.push({
            error: {
                No: this.Errores.length,
                description: "Error de operacion",
                linea: 0,
                columna: 0,
                tipo: "Semantico"
            }
        })
        return "error -1";


    }


}




module.exports = main;