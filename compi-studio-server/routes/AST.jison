
%{
var Errores = {
    Errores:[]
};
var noError = 1;
var countNodo=0;
var raizArbol;
var Nodo = class Nodo {

    constructor(id, valor) {
        this.id = id;
        this.hijos = [];
        this.etiqueta = valor;
    }

    add_hijo(id, valor) {
        var hijo = new Nodo(id, valor);
        this.hijos.push(hijo);
    }
    push_hijo(valor) {
        
        this.hijos.push(valor);
    }
};
%}
/* lexical grammar */
%lex
%options case-insensitive
%%



//******************************> Palabras Reservadas

/* COMENTARIOS */
"//"([^\n])* "\n"				/* Comentario simple */
[/][*][^*]*[*]+([^/*][^*]*[*]+)*[/]       // comentario multiple líneas

"null" return 'rnull'
"import" return 'rimport'
"true" return 'rtrue'
"switch" return 'rswitch'
"continue" return 'rcontinue'
"private" return 'rprivate'
"define" return 'rdefine'
"try" return 'rtry'
"integer" return 'rinteger'
"var" return 'rvar'
"false" return 'rfalse'
"case" return 'rcase'
"return" return 'rreturn'
"void" return 'rvoid'
"as" return 'ras'
"catch" return 'rcatch'
"double" return 'rdouble'
"const" return 'rconst'
"if" return 'rif'
"default" return 'rdefault'
"print" return 'rprint'
"for" return 'rfor'
"strc" return 'rstrc'
"throw" return 'rthrow'
"char" return 'rchar'
"global" return 'rglobal'
"else" return 'relse'
"break" return 'rbreak'
"public" return 'rpublic'
"while" return 'rwhile'
"do" return 'rdo'
"boolean" return 'rboolean'
"string" return 'rstring'

"{" return 'allave'
"}" return 'cllave'
"(" return 'apar'
")" return 'cpar'
":" return 'dospuntos'
"[" return 'acor'
"]" return 'ccor'
";" return 'puntocoma'
"===" return 'igualdad'
"==" return 'igualIgual'
"=" return 'igual'
"++" return 'incremento'
"+" return 'mas'
"--" return 'decremento'
"-"?[0-9]+("."[0-9]+)\b            return 'Decimal'
[0-9]+\b                        return 'Numero'
"-" return 'menos'
"*" return 'por'
"/" return 'div'
"%" return 'mod'
"," return 'coma'
"." return 'punto'
">=" return 'mayorIgual'
"<=" return 'menorIgual'
">" return 'mayor'
"<" return 'menor'
"!=" return 'notEqual'
"!" return 'not'
"^^" return 'pot'
"^" return 'xor'
"&&" return 'and'
"||" return 'or'


//EXPRESIONES REGULARES
([a-zA-Z]|"_")([0-9A-Za-znÑ]|"_"|"."[0-9A-Za-znÑ])*("."[jJ])          return 'Archivo'
([a-zA-Z]|"_")([0-9A-Za-znÑ]|"_")*          return 'Identificador'

("\"")([^\"]*)("\"") return 'cadena'
(\')([^\'])(\') return 'caracter'


\s+                  /* skip whitespace */
<<EOF>>               return 'EOF'
.                     {
    Errores.Errores.push({
       error: {
        No: noError,
        description: yytext,
        linea:yylineno,
        columna:0,
        tipo:"Lexico"
    } 
});
    noError++;
    }
/lex

/* operator associations and precedence */
%right igual
%left apar cpar
%left or
%left xor
%left and
%nonassoc notEqual igualIgual igualdad
%nonassoc menor mayor menorIgual mayorIgual

%left mas menos
%left por div 
%right pot mod
%left incremento decremento
%right UMINUS  not
%left acor ccor




%start START

%% /* language grammar */

START: HEAD EOF {
    countNodo++;
    var raiz = new Nodo(countNodo,"S");
    raiz.push_hijo($1);
    return raiz;
	}
    |EOF{
        countNodo++;
    var raiz = new Nodo(countNodo,"NT_S");
    return raiz;
    }
;

HEAD:
    IMPORTS BODY
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_HEAD");
        padre.push_hijo($1);
        padre.push_hijo($2);
        $$= padre; 
    }
    |IMPORTS{
        countNodo++;
        var padre = new Nodo(countNodo,"NT_HEAD");
        padre.push_hijo($1);
        
        $$= padre; 
    }
    |BODY{
        countNodo++;
        var padre = new Nodo(countNodo,"NT_HEAD");
        padre.push_hijo($1);
        
        $$= padre; 
        
    } 
    ;

BODY: BODY BODY2
    {
        $1.push_hijo($2); 
        $$ = $1; 
    }
    | BODY2{
        countNodo++;
        var padre = new Nodo(countNodo,"NT_BODY");
        padre.push_hijo($1);
        $$= padre; 
    };

BODY2: rvar Identificador dospuntos igual EXPRESION PUNTO_COMA_OPCIONAL
    {
       countNodo++;
        var padre = new Nodo(countNodo,"NT_DECLARACION_TIPO_2");
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($5);
        $$= padre; 
    }
    | rconst Identificador dospuntos igual EXPRESION PUNTO_COMA_OPCIONAL
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_DECLARACION_TIPO_3");
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($5);
        $$= padre; 
    }
    | rglobal Identificador dospuntos igual EXPRESION PUNTO_COMA_OPCIONAL
    {
       countNodo++;
        var padre = new Nodo(countNodo,"NT_DECLARACION_TIPO_4");
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($5);
        $$= padre; 
    }
|TYPE LIST_IDENTIFICADORES  DECLARACION_OPT
    {
        
       countNodo++;
        var padre = new Nodo(countNodo,"NT_DECLARACION");
        padre.push_hijo($1);
        padre.push_hijo($2);
        padre.push_hijo($3);
        
        $$= padre; 
         
        
        
    }
    |rvoid Identificador apar LISTA_EXPRESIONES_DEF cpar BLOQUE_SENTENCIAS{
   countNodo++;
        var padre = new Nodo(countNodo,"NT_FUNCION");
        countNodo++;
        padre.add_hijo(countNodo,$1);
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($4);
        padre.push_hijo($6);
        
        $$= padre; 
}
|rvoid Identificador apar cpar BLOQUE_SENTENCIAS{
    countNodo++;
        var padre = new Nodo(countNodo,"NT_FUNCION");
        countNodo++;
        padre.add_hijo(countNodo,$1);
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($5);
        
        $$= padre; 
}
 | error puntocoma {
    Errores.Errores.push({
       error: {
        No: noError,
        description: yytext,
        linea:yylineno,
        columna:0,
        tipo:"Sintactico"
    } 
});
    noError++;
    $$={
        }
    };

LISTA_EXPRESIONES_DEF : LISTA_EXPRESIONES_DEF coma  PARAMETRO_DEF
    {
          $1.push_hijo($3); 
        $$ = $1; 
    }
    |PARAMETRO_DEF
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_LISTA_EXPRESIONES_DEF");
        padre.push_hijo($1);
        
        $$= padre; 
    };


PARAMETRO_DEF: TYPE Identificador  
    {
       countNodo++;
        var padre = new Nodo(countNodo,"NT_PARAMETRO_DEF");
        padre.push_hijo($1);
        countNodo++;
        padre.add_hijo(countNodo,$2);
        
        $$= padre; 
    };

LISTA_EXPRESIONES_LLAMADA : LISTA_EXPRESIONES_LLAMADA coma  PARAMETRO_LLAMADA
    {
           $1.push_hijo($3); 
        $$ = $1; 
    }
    |PARAMETRO_LLAMADA
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_LISTA_EXPRESIONES");
        countNodo++;
        padre.push_hijo($1);
        $$= padre; 
    };


PARAMETRO_LLAMADA:  EXPRESION
    {
        $$= $1; 
    };

PUNTO_COMA_OPCIONAL: puntocoma
|{};

DECLARACION_OPT: igual EXPRESION  PUNTO_COMA_OPCIONAL{
    countNodo++;
        var padre = new Nodo(countNodo,"NT_DECLARACION_OPT");
        countNodo++;
        padre.add_hijo(countNodo,$1);
        padre.push_hijo($2);
        $$= padre; 
}
|apar LISTA_EXPRESIONES_DEF cpar BLOQUE_SENTENCIAS{
        countNodo++;
        var padre = new Nodo(countNodo,"NT_DECLARACION_OPT");
        padre.push_hijo($2);
        padre.push_hijo($4);
        $$= padre; 
    }
    |apar cpar BLOQUE_SENTENCIAS{
        countNodo++;
        var padre = new Nodo(countNodo,"NT_DECLARACIO_OPT");
        padre.push_hijo($3);
        $$= padre; 
    };

IMPORTS: IMPORTS coma IMPORT
    { 
        $1.push_hijo($3); 
        $$ = $1; 
    }
	| rimport IMPORT
    { 
        countNodo++;
        var padre = new Nodo(countNodo,"NT_imports");
        padre.push_hijo($2);
        $$= padre; 
    }
    ;

IMPORT: Archivo{
   countNodo++;
        var padre = new Nodo(countNodo,$1);
        $$= padre; 
}
;


VISIBILIDAD: rpublic{
        countNodo++;
        var padre = new Nodo(countNodo,$1);
        $$= padre; 
    }
    |rprivate{
        countNodo++;
        var padre = new Nodo(countNodo,$1);
        $$= padre; 
    };

TIPOF: rinteger{
        countNodo++;
        var padre = new Nodo(countNodo,$1);
        $$= padre; 
    }
    | rdouble{
        countNodo++;
        var padre = new Nodo(countNodo,$1);
        $$= padre; 
    }
    | rchar{
        countNodo++;
        var padre = new Nodo(countNodo,$1);
        $$= padre; 
    }
    | rboolean{
        countNodo++;
        var padre = new Nodo(countNodo,$1);
        $$= padre; 
    }
    | rvoid{
        countNodo++;
        var padre = new Nodo(countNodo,$1);
        $$= padre; 
    };


/************************          TIPO DE DATO               *****************************************/
TYPE:TIPO LIST_COR_DEF
    {
       countNodo++;
        var padre = new Nodo(countNodo,"NT_TIPO");
        padre.push_hijo($1);
        padre.push_hijo($2);
        $$= padre; 
    }
    |TIPO
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_TIPO");
        padre.push_hijo($1);
        $$= padre; 
    };

TIPO: rinteger{
        countNodo++;
        var padre = new Nodo(countNodo,$1);
        $$= padre; 
    }
    | rdouble{
        countNodo++;
        var padre = new Nodo(countNodo,$1);
        $$= padre; 
    }
    | rchar{
        countNodo++;
        var padre = new Nodo(countNodo,$1);
        $$= padre; 
    }
    | rboolean{
        countNodo++;
        var padre = new Nodo(countNodo,$1);
        $$= padre; 
    }
    | rstring{
        countNodo++;
        var padre = new Nodo(countNodo,$1);
        $$= padre; 
    }
    ;

LIST_IDENTIFICADORES: LIST_IDENTIFICADORES coma Identificador{
        countNodo++;
        $1.add_hijo(countNodo,$3);
        
        $$ = $1; 
    }
    |Identificador
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_LISTA_IDENTIFICADORES");
        countNodo++;
        padre.add_hijo(countNodo,$1);
        $$= padre; 
    };



LIST_COR_DEF:LIST_COR_DEF acor ccor
    {
        
        countNodo++;
        $1.add_hijo(countNodo,"[]");
        $$= $1; 
    }
    |acor ccor
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_LISTA_COR_DEF");
        countNodo++;
        padre.add_hijo(countNodo,"[]");
        $$= padre; 
    };

LIST_COR:LIST_COR acor EXPRESION ccor
    {
        $1.push_hijo($3); 
        $$ = $1; 
    }
    |acor EXPRESION ccor
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_LISTA_COR");
        padre.push_hijo($2);
        $$= padre; 
    };

/**              listallamada   ***/
LISTA_LLAMADA: LISTA_LLAMADA punto LLAMADA
    {
        $1.push_hijo($3);
        $$=$1;
    }
    |LLAMADA
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_LISTA_LLAMADAS");
        padre.push_hijo($1);
        $$= padre; 
    };

LLAMADA:Identificador acor EXPRESION ccor
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_ACCESO_ARREGLO");
        countNodo++;
        padre.add_hijo(countNodo,$1);
        padre.push_hijo($3);
        $$= padre; 
    }
    |Identificador
    {
        countNodo++;
        var padre = new Nodo(countNodo,$1);
        $$= padre; 
    };

/**************************      SENTENCIAS           ***********************************/

BLOQUE_SENTENCIAS: allave BLOQUE_SENTENCIAS2
    {
        $$= $2;
    }
    ;

BLOQUE_SENTENCIAS2: SENTENCIAS cllave
    {
        
        
        $$= $1; 
    }
    |cllave
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_SENTENCIAS");
        $$= padre; 
    };

SENTENCIAS: SENTENCIAS SENTENCIA
    {
        
        $1.push_hijo($2); 
        $$ = $1;
    }
    | SENTENCIA
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_SENTENCIAS");
        padre.push_hijo($1);
        $$= padre; 
    };

SENTENCIA: 
     DECLARACION{
        $$=$1;
    }
    | rcontinue  PUNTO_COMA_OPCIONAL
    {
        countNodo++;
        var padre = new Nodo(countNodo,"CONTINUE");
        padre.push_hijo($2);
        $$= padre; 
    }
    | rbreak PUNTO_COMA_OPCIONAL
        {
         countNodo++;
        var padre = new Nodo(countNodo,"BREAK");
        
        $$= padre; 
        }
    | rreturn EXPRESION PUNTO_COMA_OPCIONAL
    {
        countNodo++;
        var padre = new Nodo(countNodo,"RETURN");
        padre.push_hijo($2);
        $$= padre; 
    }  
    |LLAMADA incremento PUNTO_COMA_OPCIONAL{
        countNodo++;
        var padre = new Nodo(countNodo,"INCREMENTO");
        padre.push_hijo($1);
        $$= padre; 
    }
    |LLAMADA decremento PUNTO_COMA_OPCIONAL{
        countNodo++;
        var padre = new Nodo(countNodo,"DECREMENTO");
        padre.push_hijo($1);
        $$= padre; 
    }
    | LLAMADA_FUNCION  PUNTO_COMA_OPCIONAL{
        $$=$1;
    }
    | rprint apar EXPRESION cpar PUNTO_COMA_OPCIONAL
    {
       countNodo++;
        var padre = new Nodo(countNodo,"PRINT");
        padre.push_hijo($3);
        $$= padre; 
    }
    
    |IF{
        $$=$1;
    }
    |WHILE{
        $$=$1;   
    }
    |DOWHILE{
        $$=$1;   
    }
    |FOR{
        $$=$1;
    }
    |SWITCH{
        $$=$1;
    }
    |ASIGNACION{
        $$= $1; 
    }
    | rvar Identificador dospuntos igual EXPRESION PUNTO_COMA_OPCIONAL
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_DECLARACION_TIPO_2");
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($5);
        $$= padre; 
    }
    | rconst Identificador dospuntos igual EXPRESION PUNTO_COMA_OPCIONAL
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_DECLARACION_TIPO_3");
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($5);
        $$= padre; 
    }
    | rglobal Identificador dospuntos igual EXPRESION PUNTO_COMA_OPCIONAL
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_DECLARACION_TIPO_4");
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($5);
        $$= padre; 
    }
     | error puntocoma {
    Errores.Errores.push({
       error: {
        No: noError,
        description: yytext,
        linea:yylineno,
        columna:0,
        tipo:"Sintactico"
    } 
});
    noError++;
    $$={
        }
    };   



LLAMADA_FUNCION:  Identificador apar LISTA_EXPRESIONES cpar{
        countNodo++;
        var padre = new Nodo(countNodo,"NT_LLAMADA_FUNCION");
        countNodo++;
        padre.add_hijo(countNodo,$1);
        padre.push_hijo($3);
        $$= padre; 
    }
    |Identificador apar cpar{
        countNodo++;
        var padre = new Nodo(countNodo,"NT_LLAMADA_FUNCION");
        countNodo++;
        padre.add_hijo(countNodo,$1);
        $$= padre; 
    };

LISTA_EXPRESIONES: LISTA_EXPRESIONES coma EXPRESION
    {
        $1.push_hijo($3);
        $$=$1;
    }
    |EXPRESION
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_LISTA_EXPRESIONES");
        
        padre.push_hijo($1);
        $$= padre; 
    };



/**************************      IF           ***********************************/
IF:rif apar EXPRESION cpar BLOQUE_SENTENCIAS relse ELSE   
        {
            countNodo++;
        var padre = new Nodo(countNodo,"NT_IF");
        
        padre.push_hijo($3);
        padre.push_hijo($5);
        padre.push_hijo($7);
        $$= padre; 
        }
    |rif apar EXPRESION cpar BLOQUE_SENTENCIAS 
        {
                countNodo++;
        var padre = new Nodo(countNodo,"NT_IF");
        
        padre.push_hijo($3);
        padre.push_hijo($5);
        $$= padre;
        };


ELSE: IF
    {
            countNodo++;
        var padre = new Nodo(countNodo,"NT_ELSE");
        
        padre.push_hijo($1);
        $$= padre;
    }
    | BLOQUE_SENTENCIAS
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_ELSE");
        
        padre.push_hijo($1);
        $$= padre;
    }
    ;

/**************************      WHILE           ***********************************/


        WHILE: rwhile apar EXPRESION cpar BLOQUE_SENTENCIAS
        {
          countNodo++;
        var padre = new Nodo(countNodo,"NT_WHILE");
        
        padre.push_hijo($3);
        padre.push_hijo($5);
        $$= padre;
        };

/**************************      DOWHILE           ***********************************/

DOWHILE: rdo BLOQUE_SENTENCIAS rwhile apar EXPRESION cpar puntocoma
    {
      countNodo++;
        var padre = new Nodo(countNodo,"NT_DO_WHILE");
        
        padre.push_hijo($2);
        padre.push_hijo($5);
        $$= padre;
    };


/**************************             FOR            ***********************************/

FOR: rfor apar DECLARACION_FOR puntocoma EXPRESION puntocoma EXPRESION cpar BLOQUE_SENTENCIAS
    
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_FOR");
        
        padre.push_hijo($3);
        padre.push_hijo($5);
        padre.push_hijo($7);
        padre.push_hijo($9);
        $$= padre;
    }
    
    |rfor apar DECLARACION_FOR puntocoma  puntocoma EXPRESION cpar BLOQUE_SENTENCIAS
    {
            countNodo++;
        var padre = new Nodo(countNodo,"NT_FOR");
        
        padre.push_hijo($3);
        padre.push_hijo($6);
        padre.push_hijo($8);
        $$= padre;
    }
    |rfor apar DECLARACION_FOR puntocoma EXPRESION puntocoma  cpar BLOQUE_SENTENCIAS
    {
             countNodo++;
        var padre = new Nodo(countNodo,"NT_FOR");
        
        padre.push_hijo($3);
        padre.push_hijo($5);
        padre.push_hijo($8);
        $$= padre;
    }
    |rfor apar  puntocoma EXPRESION puntocoma EXPRESION cpar BLOQUE_SENTENCIAS
    {
             countNodo++;
        var padre = new Nodo(countNodo,"NT_FOR");
        
        padre.push_hijo($4);
        padre.push_hijo($6);
        padre.push_hijo($8);
        $$= padre;
    }
     |rfor apar DECLARACION_FOR puntocoma  puntocoma  cpar BLOQUE_SENTENCIAS
    {
             countNodo++;
        var padre = new Nodo(countNodo,"NT_FOR");
        
        padre.push_hijo($3);
        padre.push_hijo($7);
        $$= padre;
    }
    |rfor apar  puntocoma EXPRESION  puntocoma  cpar BLOQUE_SENTENCIAS
    {
             countNodo++;
        var padre = new Nodo(countNodo,"NT_FOR");
        
        padre.push_hijo($4);
        padre.push_hijo($7);
        $$= padre;
    }
    |rfor apar  puntocoma puntocoma EXPRESION  cpar BLOQUE_SENTENCIAS
    {
             countNodo++;
        var padre = new Nodo(countNodo,"NT_FOR");
        
        padre.push_hijo($5);
        padre.push_hijo($7);
        
        $$= padre;
    }
    |rfor apar  puntocoma puntocoma  cpar BLOQUE_SENTENCIAS
    {
             countNodo++;
        var padre = new Nodo(countNodo,"NT_FOR");
        
        padre.push_hijo($6);
        $$= padre;
    };

/**************************             SWITCH            ***********************************/

SWITCH: rswitch apar EXPRESION cpar allave LCASE DEFAULT cllave{
    countNodo++;
        var padre = new Nodo(countNodo,"NT_SWITCH");
        
        padre.push_hijo($3);
        padre.push_hijo($6);
        padre.push_hijo($7);
        $$= padre;
}
;

DEFAULT: rdefault dospuntos SENTENCIAS
    {
         countNodo++;
        var padre = new Nodo(countNodo,"NT_DEFAULT");
        
        countNodo++;
        padre.add_hijo(countNodo,$1);
        padre.push_hijo($3);
        $$= padre;
    }
    |rdefault dospuntos 
    {
         countNodo++;
        var padre = new Nodo(countNodo,"NT_DEFAULT");
        
        countNodo++;
        padre.add_hijo(countNodo,$1);
        
        $$= padre;
    }
    |{
        countNodo++;
        var padre = new Nodo(countNodo,"NT_DEFAULT");
        
        $$= padre;
    };

LCASE: LCASE CASE
    {
        $1.push_hijo($2);
        $$=$1;
    }
    |CASE
    {
         countNodo++;
        var padre = new Nodo(countNodo,"NT_ListaCases");
        
        padre.push_hijo($1);
        
        $$= padre;
    };

CASE: rcase EXPRESION dospuntos SENTENCIAS 
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_CASE");
        
        padre.push_hijo($2);
        padre.push_hijo($4);
        
        $$= padre;
    }
    |rcase EXPRESION dospuntos  
    {
      countNodo++;
        var padre = new Nodo(countNodo,"NT_CASE");
        
        padre.push_hijo($2);
        
        
        $$= padre;
    };


DECLARACION: TYPE LIST_IDENTIFICADORES igual EXPRESION  PUNTO_COMA_OPCIONAL
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_DECLARACION");
        padre.push_hijo($1);
        padre.push_hijo($2);
        countNodo++;
        padre.add_hijo(countNodo,$3);
        padre.push_hijo($4);
        
        $$= padre;
    }
    |TYPE LIST_IDENTIFICADORES  PUNTO_COMA_OPCIONAL
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_DECLARACION");
        padre.push_hijo($1);
        padre.push_hijo($2);
        
        $$= padre;
    };


DECLARACION_FOR: TYPE LIST_IDENTIFICADORES igual EXPRESION  
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_DECLARACION");
        padre.push_hijo($1);
        padre.push_hijo($2);

        padre.push_hijo($4);
        
        $$= padre;
    }
    |TYPE LIST_IDENTIFICADORES 
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_DECLARACION");
        padre.push_hijo($1);
        padre.push_hijo($2);
        
        $$= padre;
    };


ASIGNACION: LLAMADA igual EXPRESION  PUNTO_COMA_OPCIONAL
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_ASIGNACION");
        padre.push_hijo($1);
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($3);
        
        $$= padre;
    };

/**************************      EXPRESION               ***********************************/
    EXPRESION: apar EXPRESION cpar
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        padre.push_hijo($2);
        
        $$= padre;
    }
    |Identificador acor EXPRESION ccor
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        countNodo++;
        padre.add_hijo(countNodo,$1);
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($3);
        countNodo++;
        padre.add_hijo(countNodo,$4);
        $$= padre;
    }
    |apar TIPO cpar EXPRESION
    {
         countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        countNodo++;
        padre.add_hijo(countNodo,$1);
        padre.push_hijo($2);
        countNodo++;
        padre.add_hijo(countNodo,$3);
        padre.push_hijo($4);
        $$= padre;
    }
    |EXPRESION decremento 
    {
         countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        padre.push_hijo($1);
        countNodo++;
        padre.add_hijo(countNodo,$2);
        
        $$= padre;
    }
    |EXPRESION incremento
    {
         countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        padre.push_hijo($1);
        countNodo++;
        padre.add_hijo(countNodo,$2);
        
        $$= padre;
    }
        |not EXPRESION
    {
         countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        countNodo++;
        padre.add_hijo(countNodo,$1);
        padre.push_hijo($2);
        
        $$= padre;
    }
    | menos EXPRESION %prec UMINUS
    {
         countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        countNodo++;
        padre.add_hijo(countNodo,$1);
        padre.push_hijo($2);
        $$= padre;
    }
    |EXPRESION xor EXPRESION
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        padre.push_hijo($1);
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($3);
        $$= padre;
    }
    |EXPRESION or EXPRESION
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        padre.push_hijo($1);
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($3);
        $$= padre;
    }
    |EXPRESION and EXPRESION
    {
      countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        padre.push_hijo($1);
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($3);
        $$= padre;
    }
    |EXPRESION menorIgual EXPRESION
    {
       countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        padre.push_hijo($1);
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($3);
        $$= padre;
    }
    |EXPRESION mayorIgual EXPRESION
    {
       countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        padre.push_hijo($1);
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($3);
        $$= padre;
    }
    |EXPRESION menor EXPRESION
    {
      countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        padre.push_hijo($1);
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($3);
        $$= padre;
    }
    |EXPRESION mayor EXPRESION
    {
  countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        padre.push_hijo($1);
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($3);
        $$= padre;
    }
    |EXPRESION notEqual EXPRESION
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        padre.push_hijo($1);
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($3);
        $$= padre;
    }
    |EXPRESION igualdad EXPRESION
    {
       countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        padre.push_hijo($1);
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($3);
        $$= padre;
    }
    |EXPRESION igualIgual EXPRESION
    {
       countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        padre.push_hijo($1);
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($3);
        $$= padre;
    }
    |EXPRESION mod EXPRESION
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        padre.push_hijo($1);
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($3);
        $$= padre;
    }
    |EXPRESION pot EXPRESION
    {
    countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        padre.push_hijo($1);
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($3);
        $$= padre;
    }
    |EXPRESION div EXPRESION
    {
        countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        padre.push_hijo($1);
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($3);
        $$= padre;
    }
    |EXPRESION por EXPRESION
    {
      countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        padre.push_hijo($1);
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($3);
        $$= padre;
    }
    |EXPRESION mas EXPRESION
    {
       countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        padre.push_hijo($1);
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($3);
        $$= padre;
    }
    |LLAMADA_FUNCION{
        $$=$1;
    }
    |EXPRESION menos EXPRESION
    {
      countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        padre.push_hijo($1);
        countNodo++;
        padre.add_hijo(countNodo,$2);
        padre.push_hijo($3);
        $$= padre;
    }
    |allave LISTA_EXPRESIONES  cllave
    {
         countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        countNodo++;
        padre.add_hijo(countNodo,$1);
        padre.push_hijo($2);
        countNodo++;
        padre.add_hijo(countNodo,$3);
        $$= padre;
    }
      | caracter{
         countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        countNodo++;
        padre.add_hijo(countNodo,$1);
        $$= padre;
    }
    |Decimal  
    {
         countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        countNodo++;
        padre.add_hijo(countNodo,$1);
        
        $$= padre;
    }
    |Numero
    {
         countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        countNodo++;
        padre.add_hijo(countNodo,$1);
        
        $$= padre;
    }
  
    |cadena
    {
         countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        countNodo++;
        padre.add_hijo(countNodo,$1);
        
        $$= padre;
    }
    
    | Identificador{
         countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        countNodo++;
        padre.add_hijo(countNodo,$1);
        
        $$= padre;
    }
    | rtrue{
         countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        countNodo++;
        padre.add_hijo(countNodo,$1);
        
        $$= padre;
    }
    | rfalse{
        countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        countNodo++;
        padre.add_hijo(countNodo,$1);
      
        $$= padre;
    }
    |rnull{
         countNodo++;
        var padre = new Nodo(countNodo,"NT_EXPRESION");
        countNodo++;
        padre.add_hijo(countNodo,$1);
        
        $$= padre;

    }
    ;

