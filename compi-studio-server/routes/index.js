var express = require('express');
var router = express.Router();
const bodyParser = require('body-parser');
var Optimizacion = require('./optimizacion');
var Main = require('./main');
var Optimize = new Optimizacion();
var main = new Main();
const fs = require("fs");
const path = require('path');
/* GET home page. */


router.get('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
});

router.get('/prueba', function(req, res, next) {
    res.send("hola");
});

router.get('/AST', function(req, res) {

    res.sendFile(path.join(__dirname, "\\SalidasDot\\ast.png"));

});

router.post('/Translate', (req, res) => {

    try {
        var salida = main.Analizar(req.body.Entrada);
        //console.log(salida);
        res.send(salida);

    } catch (e) {
        res.send({
            Error: e
        });
    }

});

router.post('/Optimize', (req, res) => {

    try {
        var salida = Optimize.Analizar(req.body.Entrada);

        res.send(salida);
    } catch (e) {
        res.send({
            Error: e
        });
    }
});

router.post('/Prueba', (req, res) => {
    try {
        var salida = main.exec(req.body.Entrada);

        res.send(JSON.stringify(salida));
    } catch (e) {
        res.send({
            Error: e
        });
    }
});


module.exports = router;