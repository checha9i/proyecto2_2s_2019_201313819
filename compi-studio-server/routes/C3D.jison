
%{
var Errores = {
    Errores:[]
};
var noError = 1;
var countNodo=0;
var raizArbol;
var Nodo = class Nodo {

    constructor(id, valor) {
        this.id = id;
        this.hijos = [];
        this.etiqueta = valor;
    }

    add_hijo(id, valor) {
        let hijo = new Nodo(id, valor);
        this.hijos.push(hijo);
    }
};
%}
/* lexical grammar */
%lex
%options case-insensitive
%%



//******************************> Palabras Reservadas

/* COMENTARIOS */

"#*"([^("*#")])*"*#"			/* Comentario multilinea */
"#"([^\n])* "\n"				/* Comentario simple */
"H" return 'H'
"P" return 'P'
"Stack" return 'Stack'
"Heap" return 'Heap'
"var" return 'rvar'
"call" return 'call'
"goto" return 'goto'
"if" return 'if'
"proc" return 'proc'
"begin" return 'begin'
"end" return 'end'
"E" return 'E'
"print" return 'print'
"[" return 'acor'
"]" return 'ccor'
"(" return 'apar'
")" return 'cpar'
";" return 'puntocoma'
":" return 'dospuntos'
"==" return 'igualIgual'
"=" return 'igual'
"+" return 'mas'
"-"?[0-9]+("."[0-9]+)\b            return 'Decimal'
[0-9]+\b                        return 'Numero'
"-" return 'menos'
"*" return 'por'
"/" return 'div'
"%" return 'mod'
"," return 'coma'
"." return 'punto'
"<>" return 'notEqual'
">=" return 'mayorIgual'
"<=" return 'menorIgual'
">" return 'mayor'
"<" return 'menor'



//EXPRESIONES REGULARES


([Tt])([0-9])+          return 'Temporal'
([Ll])([0-9])+          return 'Label'
([a-zA-Z]|"_")([0-9A-Za-znÑ]|"_")*          return 'Identificador'
("\"")"%i"("\"") return 'porEntero'
("\"")"%c"("\"") return 'porCaracter'
("\"")"%d"("\"") return 'porDecimal'




\s+                  /* skip whitespace */
<<EOF>>               return 'EOF'
.                     {
    Errores.Errores.push({
       error: {
        No: noError,
        description: yytext,
        linea:yylineno,
        columna:0,
        tipo:"Lexico"
    } 
});
    noError++;
    }
/lex

/* operator associations and precedence */
%right igual
%left apar cpar

%left or
%left xor
%left and
%nonassoc notEqual igualIgual
%nonassoc menor mayor menorIgual mayorIgual

%left mas menos
%left por div 
%right pot mod
%left incremento decremento
%right UMINUS 
%left acor ccor




%start START

%% /* language grammar */

START: HEAD EOF {

    $1.push(Errores);
		return $1;
	}
    |EOF
;

HEAD: HSENTENCIAS{
       $$=$1;
   };

HSENTENCIAS: HSENTENCIAS HSENTENCIA{
    $1.push($2);
    $$=$1;
}
|HSENTENCIA{
    $$=[$1];
};

HSENTENCIA: FUNCION{
    $$=$1;
}
|DECLARACION{
    $$=$1
}
|IF{
    $$=$1;
}
|ASIGNACION{
    $$=$1;
}
|GOTO{
    $$=$1;
}
|CALL_FUNCION{
    $$=$1;
}
|PRINT{
    $$=$1;
}
|Label dospuntos{
    $$={
        Label:$1,
        Codigo:$1+":\n",
                    linea:@1.first_line,
                    columna:@1.first_column
    }
};

LISTATEMPS: LISTATEMPS coma Temporal{
        $1.push($3);
        $$=$1;
    
}
|Temporal{
    $$ =[$1];
};

DECLARACION: rvar LISTATEMPS puntocoma{
    $$={
        Declaracion:{
            Temporales:$2,
            Codigo: "var "+$2.Codigo+";"
        }
    }
}
    |rvar Stack acor ccor puntocoma{
     $$={
        Declaracion:{
            Stack:null,
            Codigo: "var Stack[];"
        }
    }   
    }
    |rvar Heap acor ccor puntocoma{
     $$={
        Declaracion:{
            Heap:null,
            Codigo: "var Stack[];"
        }
    }   
    }
    |rvar E igual EXPRESION puntocoma{
     $$={
        Declaracion:{
            E:null,
            Valor: $4,
            Codigo: "var E = "+$4+";\n"
        }
    }   
    }
    |rvar E puntocoma{
     $$={
        Declaracion:{
            E:null,
            Codigo: "var E;\n"
        }
    }   
    }
    |rvar P igual EXPRESION puntocoma{
     $$={
        Declaracion:{
            P:null,
            Valor: $4,
            Codigo: "var P = "+$4+";\n"
        }
    }   
    }
    |rvar P puntocoma{
     $$={
        Declaracion:{
            P:null,
            Codigo: "var P;\n"
        }
    }   
    }
    |rvar H igual EXPRESION puntocoma{
     $$={
        Declaracion:{
            H:null,
            Valor: $4,
            Codigo: "var H = "+$4+";\n"
        }
    }   
    }
    |rvar H puntocoma{
     $$={
        Declaracion:{
            H:null,
            Codigo: "var H;\n"
        }
    }   
    };

FUNCION :proc Identificador begin SENTENCIAS end{
    $$={
        Funcion:{
            Nombre:$2,
            proc:"proc "+$2+" begin\n",
            Codigo: $4.Codigo,
            end: "end\n",
            Sentencias:$4

        }
    }
}
|proc Identificador begin end{
    $$={
        Funcion:{
            Nombre:$2,
            proc:"proc "+$2+" begin\n",
            Codigo: $4.Codigo,
            end: "end\n",
            Sentencias:[]

        }
    }
}
;

SENTENCIAS: SENTENCIAS SENTENCIA{
    $1.push($2);
    $$=$1;
}
|SENTENCIA{
    $$ = [$1];
};

SENTENCIA: IF{
    $$=$1;
}
|ASIGNACION{
    $$=$1;
}
|GOTO{
    $$=$1;
}
|CALL_FUNCION{
    $$=$1;
}
|PRINT{
    $$=$1;
}
|Label dospuntos{
    $$={
        Label:$1,
        Codigo:$1+":\n",
                    linea:@1.first_line,
                    columna:@1.first_column
    }
};

PRINT: print apar porCaracter coma EXPRESION cpar puntocoma{
    $$= {
        print:{
            Tipo:$3,
            Valor:$5,
            Codigo:"print(\"%c\", "+$5.Codigo+");\n"
        }
    }
}
|print apar porEntero coma EXPRESION cpar puntocoma{
    $$= {
        print:{
            Tipo:$3,
            Valor:$5,
            Codigo:"print(\"%i\", "+$5.Codigo+");\n"
        }
    }
}
|print apar porDecimal coma EXPRESION cpar puntocoma{
    $$= {
        print:{
            Tipo:$3,
            Valor:$5,
            Codigo:"print(\"%d\", "+$5.Codigo+");\n"
        }
    }
};

CALL_FUNCION: call Identificador puntocoma{
    $$={
        call:{
            Nombre: $2,
            Codigo: "call "+$2+";\n",
            linea:@1.first_line,
                    columna:@1.first_column
        }
    }
};


IF: if apar EXPRESION cpar  goto Label puntocoma{
    $$={
        IF:{
            Label: $6,
            Valor: $3,
            Codigo: $1 +"("+ $3.Codigo + ") goto "+$6+"; \n",
                    linea:@1.first_line,
                    columna:@1.first_column
        }
    }
};

GOTO: goto Label puntocoma{
    $$={
        Goto:{
            Label:$2,
            Codigo: "goto "+$2+"; \n",
                    linea:@1.first_line,
                    columna:@1.first_column
        }
    }
};

ASIGNACION: Temporal igual EXPRESION  puntocoma
    {
        $$={
            Asignacion:{
                Temporal: $1,
                Valor: $3,
                Codigo: $1 +" = "+ $3.Codigo + "; \n",
                    linea:@1.first_line,
                    columna:@1.first_column
            }
        }
    }
    |P igual EXPRESION  puntocoma
    {
        $$={
            Asignacion:{
                P: $1,
                Valor: $3,
                Codigo: $1 +" = "+ $3.Codigo + "; \n",
                    linea:@1.first_line,
                    columna:@1.first_column
            }
        }
    }
    |H igual EXPRESION  puntocoma
    {
        $$={
            Asignacion:{
                H: $1,
                Valor: $3,
                Codigo: $1 +" = "+ $3.Codigo + "; \n",
                    linea:@1.first_line,
                    columna:@1.first_column
            }
        }
    }
     |E igual EXPRESION  puntocoma
    {
        $$={
            Asignacion:{
                E: $1,
                Valor: $3,
                Codigo: $1 +" = "+ $3.Codigo + "; \n",
                    linea:@1.first_line,
                    columna:@1.first_column
            }
        }
    }
    |Stack acor EXPRESION ccor igual EXPRESION  puntocoma
    {
        $$={
            Asignacion:{
                Stack: $1,
                Pos: $3,
                Valor: $6,
                Codigo: "Stack["+$3.Codigo+"] = "+ $6.Codigo + "; \n",
                    linea:@1.first_line,
                    columna:@1.first_column
            }
        }
    }
    |Heap acor EXPRESION ccor igual EXPRESION  puntocoma
    {
        $$={
            Asignacion:{
                Heap: $1,
                Pos: $3,
                Valor: $6,
                Codigo: "Heap["+$3.Codigo+"] = "+ $6.Codigo + "; \n",
                    linea:@1.first_line,
                    columna:@1.first_column
            }
        }
    }
    ;


/**************************      EXPRESION               ***********************************/
    EXPRESION: menos EXPRESION %prec UMINUS
    {
         $$= {
            Codigo:" - "+$2.Codigo,
            Valor1:$2,
            Operador:"-"

        }
    }
  
    |EXPRESION menorIgual EXPRESION
    {
         $$= {
            Codigo:$1.Codigo +" <= "+$3.Codigo,
            Valor1:$1,
            Valor2:$3,
            Operador:"<="

        }
    }
    |EXPRESION mayorIgual EXPRESION
    {
     $$= {
            Codigo:$1.Codigo +" >= "+$3.Codigo,
            Valor1:$1,
            Valor2:$3,
            Operador:">="

        }
    }
    |EXPRESION menor EXPRESION
    {
         $$= {
            Codigo:$1.Codigo +" < "+$3.Codigo,
            Valor1:$1,
            Valor2:$3,
            Operador:"<"

        }
    }
    |EXPRESION mayor EXPRESION
    {
         $$= {
            Codigo:$1.Codigo +" > "+$3.Codigo,
            Valor1:$1,
            Valor2:$3,
            Operador:">"

        }
    }
    |EXPRESION notEqual EXPRESION
    {
        $$= {
            Codigo:$1.Codigo +" <> "+$3.Codigo,
            Valor1:$1,
            Valor2:$3,
            Operador:"<>"

        }
    }
    |EXPRESION igualIgual EXPRESION
    {
         $$= {
            Codigo:$1.Codigo +" == "+$3.Codigo,
            Valor1:$1,
            Valor2:$3,
            Operador:"=="

        }
    }
    |EXPRESION mod EXPRESION
    {
        $$= {
            Codigo:$1.Codigo +" % "+$3.Codigo,
            Valor1:$1,
            Valor2:$3,
            Operador:"%"

        }
    }
    
    |EXPRESION div EXPRESION
    {
         $$= {
            Codigo:$1.Codigo +" / "+$3.Codigo,
            Valor1:$1,
            Valor2:$3,
            Operador:"/"

        }
    }
    |EXPRESION por EXPRESION
    {
        $$= {
            Codigo:$1.Codigo +" * "+$3.Codigo,
            Valor1:$1,
            Valor2:$3,
            Operador:"*"

        }
    }
    |EXPRESION mas EXPRESION
    {
        $$= {
            Codigo:$1.Codigo +" + "+$3.Codigo,
            Valor1:$1,
            Valor2:$3,
            Operador:"+"
        }
    }
    |EXPRESION menos EXPRESION
    {
        $$= {
            Codigo:$1.Codigo +" - "+$3.Codigo,
            Valor1:$1,
            Valor2:$3,
            Operador:"-"
        }
    }
    
     
    |Decimal  
    {
        $$ = {
            Numero:$1,
                Codigo:$1    
        };
    }
    |Numero
    {
        $$ = {
            Numero:$1,
                Codigo:$1    
        };
    }
  
    | Temporal{
        $$ ={
            Temporal:$1,
                Codigo:$1
            
        };
    }
    | H{
        $$ = {
                H:null,
                Codigo:"H"
            
            
        };
    }
    | P{
        $$ = {
            P:null,
                Codigo:"P"
            
            
        };
    }
     | E{
        $$ = {
            E:null,

                Codigo:"E"
            
            
        };
    }
    |Stack acor EXPRESION ccor{
        $$ = {
            Codigo:"Stack["+$3+"]",
            Stack:$3
        }
    }
    |Heap acor EXPRESION ccor{
        $$ = {
            Codigo:"Heap["+$3+"]",
            Heap:$3
        }
    }
    ;

