
%{
var Errores = {
    Errores:[]
};
var noError = 1;
var countNodo=0;
var raizArbol;
var Nodo = class Nodo {

    constructor(id, valor) {
        this.id = id;
        this.hijos = [];
        this.etiqueta = valor;
    }

    add_hijo(id, valor) {
        let hijo = new Nodo(id, valor);
        this.hijos.push(hijo);
    }
};
%}
/* lexical grammar */
%lex
%options case-insensitive
%%



//******************************> Palabras Reservadas

/* COMENTARIOS */
"//"([^\n])* "\n"				/* Comentario simple */
[/][*][^*]*[*]+([^/*][^*]*[*]+)*[/]       // comentario multiple líneas

"null" return 'rnull'
"import" return 'rimport'
"true" return 'rtrue'
"switch" return 'rswitch'
"continue" return 'rcontinue'
"private" return 'rprivate'
"define" return 'rdefine'
"try" return 'rtry'
"integer" return 'rinteger'
"var" return 'rvar'
"false" return 'rfalse'
"case" return 'rcase'
"return" return 'rreturn'
"void" return 'rvoid'
"as" return 'ras'
"catch" return 'rcatch'
"double" return 'rdouble'
"const" return 'rconst'
"if" return 'rif'
"default" return 'rdefault'
"print" return 'rprint'
"for" return 'rfor'
"strc" return 'rstrc'
"throw" return 'rthrow'
"char" return 'rchar'
"global" return 'rglobal'
"else" return 'relse'
"break" return 'rbreak'
"public" return 'rpublic'
"while" return 'rwhile'
"do" return 'rdo'
"boolean" return 'rboolean'
"string" return 'rstring'

"{" return 'allave'
"}" return 'cllave'
"(" return 'apar'
")" return 'cpar'
":" return 'dospuntos'
"[" return 'acor'
"]" return 'ccor'
";" return 'puntocoma'
"===" return 'igualdad'
"==" return 'igualIgual'
"=" return 'igual'
"++" return 'incremento'
"+" return 'mas'
"--" return 'decremento'
"-"?[0-9]+("."[0-9]+)\b            return 'Decimal'
[0-9]+\b                        return 'Numero'
"-" return 'menos'
"*" return 'por'
"/" return 'div'
"%" return 'mod'
"," return 'coma'
"." return 'punto'
">=" return 'mayorIgual'
"<=" return 'menorIgual'
">" return 'mayor'
"<" return 'menor'
"!=" return 'notEqual'
"!" return 'not'
"^^" return 'pot'
"^" return 'xor'
"&&" return 'and'
"||" return 'or'


//EXPRESIONES REGULARES
([a-zA-Z]|"_")([0-9A-Za-znÑ]|"_"|"."[0-9A-Za-znÑ])*("."[jJ])          return 'Archivo'
([a-zA-Z]|"_")([0-9A-Za-znÑ]|"_")*          return 'Identificador'

("\"")(([^\"])|"\\\"")*("\"") return 'cadena'
(\')([^\']|"\\\'")(\') return 'caracter'


\s+                  /* skip whitespace */
<<EOF>>               return 'EOF'
.                     {
    Errores.Errores.push({
       error: {
        No: noError,
        description: yytext,
        linea:yylineno,
        columna:0,
        tipo:"Lexico"
    } 
});
    noError++;
    }
/lex

/* operator associations and precedence */
%right igual

%left apar cpar

%left or
%left xor
%left and
%nonassoc notEqual igualIgual igualdad
%nonassoc menor mayor menorIgual mayorIgual

%left mas menos
%left por div 
%right pot mod
%left incremento decremento
%right UMINUS not
%left acor ccor




%start START

%% /* language grammar */

START: HEAD EOF {

    $1.push(Errores);
		return $1;
	}
    |EOF
;

HEAD:
    IMPORTS PUNTO_COMA_OPCIONAL BODY
    {
        $$ = [
            $1,
            $3
        ];
    }
    |IMPORTS PUNTO_COMA_OPCIONAL {
        $$ = [
            $1,
            []
        ];
    }
    |BODY{
        $$ = [
            [],
            $1
        ];
    } 
    ;

BODY: BODY BODY2
    {
        $1.push($2); 
        $$ = $1; 
    }
    | BODY2{
        $$ = [$1]; 
    };

BODY2: rvar Identificador dospuntos igual EXPRESION PUNTO_COMA_OPCIONAL
    {
        $$ = {
            Declaracion_Tipo2:{
                Tipo : "var",
                Nombre : $2,
                Valor: $5,
                linea:@1.first_line,
                columna:@1.first_column
            }
        };
    }
    | rconst Identificador dospuntos igual EXPRESION PUNTO_COMA_OPCIONAL
    {
        $$ = {
            Declaracion_Tipo3:{
                Tipo : "const",
                Nombre : $2,
                Valor: $5,
                linea:@1.first_line,
                columna:@1.first_column
            }
        };
    }
    | rglobal Identificador dospuntos igual EXPRESION PUNTO_COMA_OPCIONAL
    {
        $$ = {
            Declaracion_Tipo4:{
                Tipo : "global",
                Nombre : $2,
                Valor: $5,
                linea:@1.first_line,
                columna:@1.first_column
            }
        };
    }
|TYPE LIST_IDENTIFICADORES  DECLARACION_OPT
    {
        
        if($3.hasOwnProperty('Declaracion_Tipo1')){
            $$ = {
                Declaracion_Tipo1:{
                    Tipo : $1,
                    LIST_IDENTIFICADORES : $2,
                    Valor: $3.Declaracion_Tipo1.Valor,
                    linea:@1.first_line,
                    columna:@1.first_column
                }
            };
        }else if($3.hasOwnProperty('Funcion')){
            
    $$ = {
        Funcion: {
            Visibilidad: "private",
            Tipo: $1.Tipo,
            Nombre: $2[0],
            Parametros: $3.Funcion.Parametros,
            Sentencias: $3.Funcion.Sentencias.Sentencias,
            linea:@1.first_line,
            columna:@1.first_column
        }
    };           
        }
         
        
        
    }
    |rvoid Identificador apar LISTA_EXPRESIONES_DEF cpar BLOQUE_SENTENCIAS{
    $$ = {
        Funcion: {
            Visibilidad: "private",
            Tipo: "void",
            Nombre: $2,
            Parametros: $4,
            Sentencias: $6.Sentencias,
            linea:@1.first_line,
            columna:@1.first_column
        }
    };
}
|rvoid Identificador apar cpar BLOQUE_SENTENCIAS{
    $$ = {
        Funcion: {
            Visibilidad: "private",
            Tipo: "void",
            Nombre: $2,
            Parametros: [],
            Sentencias: $5.Sentencias,
            linea:@1.first_line,
            columna:@1.first_column
        }
    };
}
| error puntocoma {
    Errores.Errores.push({
       error: {
        No: noError,
        description: yytext,
        linea:yylineno,
        columna:0,
        tipo:"Sintactico"
    } 
});
    noError++;
    $$ = {};
    };

LISTA_EXPRESIONES_DEF : LISTA_EXPRESIONES_DEF coma  PARAMETRO_DEF
    {
          $1.push($3); 
        $$ = $1; 
    }
    |PARAMETRO_DEF
    {
        $$=[$1];
    };


PARAMETRO_DEF: TYPE Identificador  
    {
        $$={
            Tipo:$1,
            Nombre:$2
        };
    };

LISTA_EXPRESIONES_LLAMADA : LISTA_EXPRESIONES_LLAMADA coma  PARAMETRO_LLAMADA
    {
           $1.push($3); 
        $$ = $1; 
    }
    |PARAMETRO_LLAMADA
    {
        $$=[$1];
    };


PARAMETRO_LLAMADA:  EXPRESION
    {
        $$=$1;
    };

PUNTO_COMA_OPCIONAL: puntocoma
|{};

DECLARACION_OPT: igual EXPRESION  PUNTO_COMA_OPCIONAL{
    $$={
        Declaracion_Tipo1:{
            Valor:$2
        }
    };
}
|apar LISTA_EXPRESIONES_DEF cpar BLOQUE_SENTENCIAS{
        $$={
            Funcion:{
                Parametros: $2,
                Sentencias: $4
            }
        };
    }
    |apar cpar BLOQUE_SENTENCIAS{
        $$={
            Funcion:{
                Parametros: [],
                Sentencias: $3
            }
        };
    };

IMPORTS: IMPORTS coma IMPORT 
    { 
        $1.push($3); 
        $$ = $1; 
    }
	| rimport IMPORT
    { 
        $$ = [$2]; 
    }
    ;

IMPORT: Archivo{
    $$ = {
            Import: {
                Nombre: $1,
                linea:@1.first_line,
                columna:@1.first_column
            },
            linea:@1.first_line,
            columna:@1.first_column
        };
}
;


VISIBILIDAD: rpublic{
        $$ = "public";
    }
    |rprivate{
        $$ = "private";
    };

TIPOF: rinteger{
        $$ = "integer";
    }
    | rdouble{
        $$ = "double";
    }
    | rchar{
        $$ = "char";
    }
    | rboolean{
        $$ = "boolean";
    }
    | rvoid{
        $$ = "void";
    };


/************************          TIPO DE DATO               *****************************************/
TYPE:TIPO LIST_COR_DEF
    {
     $$ = {
         Tipo:$1,
         Corchetes: $2,
            linea:@1.first_line,
            columna:@1.first_column
     }
    }
    |TIPO
    {
     $$ = {
         Tipo:$1,
            linea:@1.first_line,
            columna:@1.first_column
     }
    };

TIPO: rinteger{
        $$ = "integer";
    }
    | rdouble{
        $$ = "double";
    }
    | rchar{
        $$ = "char";
    }
    | rboolean{
        $$ = "boolean";
    }
    | rstring{
        $$ = "string";
    }
    ;

LIST_IDENTIFICADORES: LIST_IDENTIFICADORES coma Identificador{
        $1.push($3); 
        $$ = $1; 
    }
    |Identificador
    {
        $$ = [$1]; 
    };



LIST_COR_DEF:LIST_COR_DEF acor ccor
    {
        $1.push(1); 
        $$ = $1; 
    }
    |acor ccor
    {
        $$ = [1];
    };

LIST_COR:LIST_COR acor EXPRESION ccor
    {
        $1.push($3); 
        $$ = $1; 
    }
    |acor EXPRESION ccor
    {
        $$ = [$2];
    };

/**              listallamada   ***/
LISTA_LLAMADA: LISTA_LLAMADA punto LLAMADA
    {
        $1.push($3);
        $$=$1;
    }
    |LLAMADA
    {
        $$=[$1];
    };

LLAMADA:Identificador acor EXPRESION ccor
    {
        $$={
            Nombre:$1,
            Posicion:$3
        }
    }
    |Identificador
    {
        $$={Nombre:$1}
    };

/**************************      SENTENCIAS           ***********************************/

BLOQUE_SENTENCIAS: allave BLOQUE_SENTENCIAS2
    {
        $$= $2;
    }
    ;

BLOQUE_SENTENCIAS2: SENTENCIAS cllave
    {
        
        $$={
            Sentencias:$1
        }
    }
    |cllave
    {
        $$={
            Sentencias:[]
        }
    }
  ;

SENTENCIAS: SENTENCIAS SENTENCIA
    {
        
        $1.push($2); 
        $$ = $1;
    }
    | SENTENCIA
    {
        $$ = [$1];
    };

SENTENCIA: 
     DECLARACION{
        $$=$1;
    }
    | rcontinue  PUNTO_COMA_OPCIONAL
    {
        $$={
            Continue:"Continue"
        }
    }
    | rbreak PUNTO_COMA_OPCIONAL
        {
            $$={
                Break:"Break"
            }
        
        }
    | rreturn EXPRESION PUNTO_COMA_OPCIONAL
    {
        $$={
            Return:{
                Valor:$2
            }
        }
    }  
    |LLAMADA incremento PUNTO_COMA_OPCIONAL{
        $$={
            Incremento:$1
        }
    }
    |LLAMADA decremento PUNTO_COMA_OPCIONAL{
        $$={
            Decremento:$1
        }
    }
    | LLAMADA_FUNCION  PUNTO_COMA_OPCIONAL{
        $$=$1;
    }
    | rprint apar EXPRESION cpar PUNTO_COMA_OPCIONAL
    {
        $$={
            Print: {
                Valor:$3
            }
        }
    }
    
    |IF{
        $$=$1;
    }
    |WHILE{
        $$=$1;   
    }
    |DOWHILE{
        $$=$1;   
    }
    |FOR{
        $$=$1;
    }
    |SWITCH{
        $$=$1;
    }
    |ASIGNACION{
     $$==$1;
    }
    | rvar Identificador dospuntos igual EXPRESION PUNTO_COMA_OPCIONAL
    {
        $$ = {
            Declaracion_Tipo2:{
                Tipo : "var",
                Nombre : $2,
                Valor: $5,
                linea:@1.first_line,
                columna:@1.first_column
            }
        };
    }
    | rconst Identificador dospuntos igual EXPRESION PUNTO_COMA_OPCIONAL
    {
        $$ = {
            Declaracion_Tipo3:{
                Tipo : "const",
                Nombre : $2,
                Valor: $5,
                linea:@1.first_line,
                columna:@1.first_column
            }
        };
    }
    | rglobal Identificador dospuntos igual EXPRESION PUNTO_COMA_OPCIONAL
    {
        $$ = {
            Declaracion_Tipo4:{
                Tipo : "global",
                Nombre : $2,
                Valor: $5,
                linea:@1.first_line,
                columna:@1.first_column
            }
        };
    }
      | error puntocoma {
    Errores.Errores.push({
       error: {
        No: noError,
        description: yytext,
        linea:yylineno,
        columna:0,
        tipo:"Sintactico"
    } 
});
    noError++;
    $$={
        }
    };



LLAMADA_FUNCION:  Identificador apar LISTA_EXPRESIONES cpar{
        $$={
            Llamada_Funcion:{
                    Nombre: $1,
                    Parametros:$3
            }
        }
    }
    |Identificador apar cpar{
        $$={
            Llamada_Funcion:{
                    Nombre: $1,
                    Parametros:[]
            }
        }
    };

LISTA_EXPRESIONES: LISTA_EXPRESIONES coma EXPRESION
    {
        $1.push($3);
        $$=$1;
    }
    |EXPRESION
    {
        $$=[$1]
    };



/**************************      IF           ***********************************/
IF:rif apar EXPRESION cpar BLOQUE_SENTENCIAS relse ELSE   
        {
            $$={
                If:{
                    Condicion:$3,
                    Sentencias:$5.Sentencias,
                    Else:$7,
                    linea:@1.first_line,
                    columna:@1.first_column
                }
            }; 
        }
    |rif apar EXPRESION cpar BLOQUE_SENTENCIAS 
        {
            $$={
                If:{
                    Condicion:$3,
                    Sentencias:$5.Sentencias,
                    linea:@1.first_line,
                    columna:@1.first_column
                }
            }; 
        };


ELSE: IF
    {
        $$= {
            ELSE:$1
        };
    }
    | BLOQUE_SENTENCIAS
    {
        $$= {
            ELSE:$1
        };
    }
    ;

/**************************      WHILE           ***********************************/


        WHILE: rwhile apar EXPRESION cpar BLOQUE_SENTENCIAS
        {
            $$={
                While:{
                    Condicion:$3,
                    Sentencias:$5.Sentencias,
                    linea:@1.first_line,
                    columna:@1.first_column
                }
            }
        };

/**************************      DOWHILE           ***********************************/

DOWHILE: rdo BLOQUE_SENTENCIAS rwhile apar EXPRESION cpar PUNTO_COMA_OPCIONAL
    {
        $$={
            DoWhile:{
                Condicion:$5,
                Sentencias:$2.Sentencias,
                    linea:@1.first_line,
                    columna:@1.first_column
            }
        }
    };


/**************************             FOR            ***********************************/

FOR: rfor apar DECLARACION_FOR puntocoma EXPRESION puntocoma EXPRESION cpar BLOQUE_SENTENCIAS
    {
            $$={
                For:{
                    Declaracion: $3,
                    Condicion: $5,
                    Final: $7,
                    Sentencias:$9.Sentencias,
                    linea:@1.first_line,
                    columna:@1.first_column
                }
            };
    }
    |rfor apar DECLARACION_FOR puntocoma  puntocoma EXPRESION cpar BLOQUE_SENTENCIAS
    {
            $$={
                For:{
                    Declaracion: $3,
                    Condicion: null,
                    Final: $6,
                    Sentencias:$8.Sentencias,
                    linea:@1.first_line,
                    columna:@1.first_column
                }
            };
    }
    |rfor apar DECLARACION_FOR puntocoma EXPRESION puntocoma  cpar BLOQUE_SENTENCIAS
    {
            $$={
                For:{
                    Declaracion: $3,
                    Condicion: $5,
                    Final: null,
                    Sentencias:$8.Sentencias,
                    linea:@1.first_line,
                    columna:@1.first_column
                }
            };
    }
    |rfor apar  puntocoma EXPRESION puntocoma EXPRESION cpar BLOQUE_SENTENCIAS
    {
            $$={
                For:{
                    Declaracion: null,
                    Condicion: $4,
                    Final: $6,
                    Sentencias:$8.Sentencias,
                    linea:@1.first_line,
                    columna:@1.first_column
                }
            };
    }
     |rfor apar DECLARACION_FOR puntocoma  puntocoma  cpar BLOQUE_SENTENCIAS
    {
            $$={
                For:{
                    Declaracion: $3,
                    Condicion: null,
                    Final: null,
                    Sentencias:$7.Sentencias,
                    linea:@1.first_line,
                    columna:@1.first_column
                }
            };
    }
    |rfor apar  puntocoma EXPRESION  puntocoma  cpar BLOQUE_SENTENCIAS
    {
            $$={
                For:{
                    Declaracion: null,
                    Condicion: $4,
                    Final: null,
                    Sentencias:$7.Sentencias,
                    linea:@1.first_line,
                    columna:@1.first_column
                }
            };
    }
    |rfor apar  puntocoma puntocoma EXPRESION  cpar BLOQUE_SENTENCIAS
    {
            $$={
                For:{
                    Declaracion: null,
                    Condicion: null,
                    Final: $5,
                    Sentencias:$7.Sentencias,
                    linea:@1.first_line,
                    columna:@1.first_column
                }
            };
    }
    |rfor apar  puntocoma puntocoma  cpar BLOQUE_SENTENCIAS
    {
            $$={
                For:{
                    Declaracion: null,
                    Condicion: null,
                    Final: null,
                    Sentencias:$6.Sentencias,
                    linea:@1.first_line,
                    columna:@1.first_column
                }
            };
    };

/**************************             SWITCH            ***********************************/

SWITCH: rswitch apar EXPRESION cpar allave LCASE DEFAULT cllave{
    $$={
        Switch:{
            Condicion: $3,
            ListaCases: $6,
            Default: $7.Default,
                    linea:@1.first_line,
                    columna:@1.first_column
        }
    }
}
;

DEFAULT: rdefault dospuntos SENTENCIAS
    {
        $$={
            Default:{
                Sentencias:$3,
                    linea:@1.first_line,
                    columna:@1.first_column
            }
        }
    }
    |rdefault dospuntos 
    {
        $$={
            Default:{
                Sentencias:[],
                    linea:@1.first_line,
                    columna:@1.first_column
            }
        }
    }
    |{
        $$={
            Default:{
                Sentencias:[],
                    linea:@1.first_line,
                    columna:@1.first_column
            }
        }
    };

LCASE: LCASE CASE
    {
        $1.push($2);
        $$=$1;
    }
    |CASE
    {
        $$=[$1];
    };

CASE: rcase EXPRESION dospuntos SENTENCIAS 
    {
        $$={
            Case:{
                Valor: $2,
                Sentencias: $4,
                    linea:@1.first_line,
                    columna:@1.first_column
            }
        }
    }
    |rcase EXPRESION dospuntos  
    {
        $$={
            Case:{
                Valor: $2,
                Sentencias: [],
                    linea:@1.first_line,
                    columna:@1.first_column
            }
        }
    };


DECLARACION: TYPE LIST_IDENTIFICADORES igual EXPRESION  PUNTO_COMA_OPCIONAL
    {
        $$={
            Declaracion_Tipo1:{
                Nombre: $2,
                Tipo: $1,
                Valor: $4,
                    linea:@1.first_line,
                    columna:@1.first_column
            }
        }
    }
    |TYPE LIST_IDENTIFICADORES  PUNTO_COMA_OPCIONAL
    {
        $$={
            Declaracion_Tipo1:{
                Nombre: $2,
                Tipo: $1,
                Valor: null,
                    linea:@1.first_line,
                    columna:@1.first_column
            }
        }
    };

DECLARACION_FOR: TYPE LIST_IDENTIFICADORES igual EXPRESION  
    {
        $$={
            Declaracion_Tipo1:{
                Nombre: $2,
                Tipo: $1,
                Valor: $4,
                    linea:@1.first_line,
                    columna:@1.first_column
            }
        }
    }
    |TYPE LIST_IDENTIFICADORES  
    {
        $$={
            Declaracion_Tipo1:{
                Nombre: $2,
                Tipo: $1,
                Valor: null,
                    linea:@1.first_line,
                    columna:@1.first_column
            }
        }
    };


ASIGNACION: LLAMADA igual EXPRESION  PUNTO_COMA_OPCIONAL
    {
        $$={
            Asignacion:{
                Nombre: $1,
                Valor: $3,
                    linea:@1.first_line,
                    columna:@1.first_column
            }
        }
    };

/**************************      EXPRESION               ***********************************/
    EXPRESION: apar EXPRESION cpar
    {
        $$=$2;
    }
    |Identificador acor EXPRESION ccor
    {
        $$={
            AccesoArreglo:{
                Nombre:$1,
                Posicion:$3
            }
        };
    }
    |LLAMADA_FUNCION{
        $$=$1;
    }
    |apar TIPO cpar EXPRESION
    {
        $$={
            Casteo:{
                Tipo:$2,
                Valor:$4
            }
        };
    }
    |EXPRESION decremento 
    {
        $$= {
            DECREMENTO:{
                Valor1: $1
            }
        }
    }
    |EXPRESION incremento
    {
        $$= {
            INCREMENTO:{
                Valor1: $1
            }
        }
    }
        |not EXPRESION
    {
        $$= {
            NOT:{
                Valor1: $2
            }
        }
    }
    | menos EXPRESION %prec UMINUS
    {
        $$= {
            UNARIO_MENOS:{
                Valor1: $2
            }
        }
    }
    |EXPRESION xor EXPRESION
    {
        $$= {
            XOR:{
                Valor1: $1,
                Valor2: $3
            }
        }
    }
    |EXPRESION or EXPRESION
    {
        $$= {
            OR:{
                Valor1: $1,
                Valor2: $3
            }
        }
    }
    |EXPRESION and EXPRESION
    {
        $$= {
            AND:{
                Valor1: $1,
                Valor2: $3
            }
        }
    }
    |EXPRESION menorIgual EXPRESION
    {
        $$= {
            MENOR_IGUAL:{
                Valor1: $1,
                Valor2: $3
            }
        }
    }
    |EXPRESION mayorIgual EXPRESION
    {
        $$= {
            MAYOR_IGUAL:{
                Valor1: $1,
                Valor2: $3
            }
        }
    }
    |EXPRESION menor EXPRESION
    {
        $$= {
            MENOR:{
                Valor1: $1,
                Valor2: $3
            }
        }
    }
    |EXPRESION mayor EXPRESION
    {
        $$= {
            MAYOR:{
                Valor1: $1,
                Valor2: $3
            }
        }
    }
    |EXPRESION notEqual EXPRESION
    {
        $$= {
            NOT_EQUAL:{
                Valor1: $1,
                Valor2: $3
            }
        }
    } 
    |EXPRESION igualdad EXPRESION
    {
        $$= {
            IGUALDAD:{
                Valor1: $1,
                Valor2: $3
            }
        }
    }
    |EXPRESION igualIgual EXPRESION
    {
        $$= {
            IGUAL_IGUAL:{
                Valor1: $1,
                Valor2: $3
            }
        }
    }
    |EXPRESION mod EXPRESION
    {
        $$= {
            MODULAR:{
                Valor1: $1,
                Valor2: $3
            }
        }
    }
    |EXPRESION pot EXPRESION
    {
        $$= {
            POTENCIA:{
                Valor1: $1,
                Valor2: $3
            }
        }
    }
    |EXPRESION div EXPRESION
    {
        $$= {
            DIVISION:{
                Valor1: $1,
                Valor2: $3
            }
        }
    }
    |EXPRESION por EXPRESION
    {
        $$= {
            MULTIPLICACION:{
                Valor1: $1,
                Valor2: $3
            }
        }
    }
    |EXPRESION mas EXPRESION
    {
        $$= {
            SUMA:{
                Valor1: $1,
                Valor2: $3
            }
        }
    }
    |EXPRESION menos EXPRESION
    {
        $$= {
            RESTA:{
                Valor1: $1,
                Valor2: $3
            }
        }
    }
    |allave LISTA_EXPRESIONES  cllave
    {
        $$= {
            ListaExpresiones: $2
        }
    }
      | caracter{
        $$ =$1;
    }
    |Decimal  
    {
        $$ = parseFloat($1);
    }
    |Numero
    {
        $$ = parseInt($1);
    }
  
    |cadena
    {
        $$ = $1.slice(1,$1.length-1);
    }
    
    | Identificador{
        $$ = {
            Identificador:$1
        };
    }
    | rtrue{
        $$ = true;
    }
    | rfalse{
        $$ = false;
    }
    |rnull{
        $$ = null;
    }
    ;