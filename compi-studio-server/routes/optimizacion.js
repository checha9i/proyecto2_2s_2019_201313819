var C3D = require('./C3D').parser;

class Optimizacion {
    constructor() {
        this.raiz = null;
        this.Salida = "";
        this.Optimizaciones = [];
    }
    exec(input) {
        return C3D.parse(input);
    }

    Clear_Variables() {
        this.raiz = null;
        this.Salida = "";
        this.Optimizaciones = [];
    }

    Analizar(input) {
        try {

            this.Clear_Variables();
            this.Optimizar(input);

        } catch (e) {
            console.log(e);
        }

        let r = {
            Salida: this.Salida,
            Optimizaciones: this.Optimizaciones
        };

        return r;
    }


    print(input) {
        this.Salida += input + "\n";
    }

    Optimizar(actual) {
        var a = null;
        let Salida = "";
        Salida = this.Salida;

        try {
            a = this.exec(actual);

            this.Clear_Variables();
            this.Regla_8_18(a);

        } catch (e) {
            console.log(e);
        }

    }

    new_Optimizacion(Tipo, Linea, Columna, Valor) {
        let Optimizacion = { Regla: Tipo, Linea: Linea, Columna: Columna, Descripcion: Valor, Nombre: "" }
        if (Linea == undefined) {
            Optimizacion.Linea = 0
        }
        if (Columna == undefined) {
            Optimizacion.Columna = 0
        }
        if (Tipo >= 8 && Tipo <= 18) {
            Optimizacion.Nombre = "Simplificación algebraica y por fuerza"
        } else if (Tipo >= 2 && Tipo <= 5) {
            Optimizacion.Nombre = "Eliminacion de codigo inalcanzable"
        } else if (Tipo == 1) {
            Optimizacion.Nombre = "Eliminación de instrucciones redundantes de carga y almacenamiento"
        }

        this.Optimizaciones.push(Optimizacion);
    }

    Regla_8_18(actual) {
        actual.map(item => {
            if (item.hasOwnProperty('Declaracion')) {
                let Declaracion = item.Declaracion;
                if (Declaracion.hasOwnProperty('Valor')) {
                    if (Declaracion.hasOwnProperty('E')) {
                        let salida = "";
                        let Valor = Declaracion.Valor;
                        if (!Valor.hasOwnProperty('Valor1')) {
                            salida += "var E = " + Valor.Codigo + ";";
                        } else {
                            console.log("Declaracion tiene otra cosa aparte de codigo");
                        }

                        this.print(salida);
                    } else if (Declaracion.hasOwnProperty('P')) {
                        let salida = ""
                        let Valor = Declaracion.Valor;
                        if (!Valor.hasOwnProperty('Valor1')) {
                            salida += "var P = " + Valor.Codigo + ";";
                        } else {
                            console.log("Declaracion tiene otra cosa aparte de codigo");
                        }

                        this.print(salida);
                    } else if (Declaracion.hasOwnProperty('H')) {
                        let salida = ""
                        let Valor = Declaracion.Valor;
                        if (!Valor.hasOwnProperty('Valor1')) {
                            salida += "var H = " + Valor.Codigo + ";";
                        } else {
                            console.log("Declaracion tiene otra cosa aparte de codigo");
                        }

                        this.print(salida);
                    }
                } else {
                    if (Declaracion.hasOwnProperty('Stack')) {
                        let salida = ""
                        salida += "var Stack[];";
                        this.print(salida);
                    } else if (Declaracion.hasOwnProperty('Heap')) {
                        let salida = ""
                        salida += "var Heap[];";
                        this.print(salida);
                    } else if (Declaracion.hasOwnProperty('E')) {
                        let salida = ""
                        salida += "var E;";
                        this.print(salida);
                    } else if (Declaracion.hasOwnProperty('P')) {
                        let salida = ""
                        salida += "var P;";
                        this.print(salida);
                    } else if (Declaracion.hasOwnProperty('H')) {
                        let salida = ""
                        salida += "var H;";
                        this.print(salida);
                    } else if (Declaracion.hasOwnProperty('Temporales')) {
                        let salida = "";
                        let Temporales = Declaracion.Temporales;
                        salida += "var ";
                        let prim = true;
                        Temporales.map(T => {
                            if (prim) {
                                salida += T;
                                prim = false;
                            } else {
                                salida += ", " + T
                            }
                        });
                        salida += ";";
                        this.print(salida);
                    } else {

                    }
                }
            } else if (item.hasOwnProperty('call')) {
                this.print("call " + item.call.Nombre + ";");
            } else if (item.hasOwnProperty('Goto')) {
                this.print("goto " + item.Goto.Label + ";");
            } else if (item.hasOwnProperty('Funcion')) {
                let Funcion = item.Funcion;
                this.print("proc " + Funcion.Nombre + " begin")
                this.print(this.Funcion(Funcion.Sentencias))
                this.print("end");
            } else if (item.hasOwnProperty('Label')) {
                this.print(item.Label + ":");
            } else if (item.hasOwnProperty('Errores')) {
                if (item.Errores.length > 0) {
                    console.log("Hay errores");
                }
            } else {
                console.log(item);
            }
        });
    }

    Funcion(actual) {
        let res = "";
        let imprimir = [true];
        for (let i = 0; i < actual.length; i++) {
            if (imprimir.length == 1 && !imprimir[0]) {
                imprimir.shift();
                imprimir.unshift(true);
                continue;
            } else if (imprimir.length > 1 && !imprimir[0]) {
                imprimir.shift();
                continue;
            } else if (imprimir.length > 1) {
                imprimir.shift();
            } else {
                imprimir.push(true)
            }
            if (actual[i].hasOwnProperty('call')) {
                res += "call " + actual[i].call.Nombre + ";\n";
            } else if (actual[i].hasOwnProperty('Goto')) {
                let Goto = actual[i].Goto;
                if (i + 1 < actual.length && (!actual[i + 1].hasOwnProperty('Label'))) {
                    for (let j = i + 1; j < actual.length; j++) {
                        if (actual[j].hasOwnProperty('Label') && actual[j].Label == Goto.Label) {
                            this.new_Optimizacion(2, Goto.linea, Goto.columna, "goto L1;\n<instrucciones>\nL1:");
                            break;
                        }
                        if (actual[j].hasOwnProperty('Label')) {
                            res += "goto " + actual[i].Goto.Label + ";\n";
                            //this.new_Optimizacion(2, Goto.linea, Goto.columna, "goto L1;\n<instrucciones>\nL2:");
                            break;
                        } else {
                            imprimir.unshift(false);
                        }
                    }

                    continue;
                } else {

                    res += "goto " + actual[i].Goto.Label + ";\n";
                }



            } else if (actual[i].hasOwnProperty('Label')) {
                res += actual[i].Label + ":\n";
            } else if (actual[i].hasOwnProperty('print')) {
                res += actual[i].print.Codigo + "\n";
            } else if (actual[i].hasOwnProperty('IF')) {
                //REGLA 4 Y 5
                let iff = actual[i].IF;

                if (iff.Valor.hasOwnProperty('Operador') && iff.Valor.Operador == "==") {
                    let Valor1 = iff.Valor.Valor1;
                    let Valor2 = iff.Valor.Valor2;

                    if (Valor1.hasOwnProperty('Numero') && Valor2.hasOwnProperty('Numero')) {
                        let Temp = iff.Label;
                        if (Valor1.Numero == Valor2.Numero) {
                            if (actual[i + 1].hasOwnProperty('Goto')) {

                                res += "goto " + Temp + ";\n"
                                this.new_Optimizacion(4, iff.linea, iff.columna, "if(1==1) goto L1;\ngoto L2;");
                                imprimir.pop();
                                imprimir.unshift(false);
                            } else {
                                res += actual[i].IF.Codigo + "\n";
                            }
                        } else {
                            if (actual[i + 1].hasOwnProperty('Goto')) {
                                let act2 = actual[i + 1].Goto;
                                res += "goto " + act2.Label + ";\n"
                                this.new_Optimizacion(5, iff.linea, iff.columna, "if(1==0) goto L1;\ngoto L2;");
                                imprimir.pop();
                                imprimir.unshift(false);
                            } else {
                                res += actual[i].IF.Codigo;
                            }
                        }


                    } else {
                        //regla 3
                        if (i + 1 < actual.length && actual[i + 1].hasOwnProperty('Goto') && (i + 2 < actual.length && actual[i + 2].hasOwnProperty('Label') && actual[i + 2].Label == iff.Label)) {
                            let cod = actual[i].IF.Codigo.replace(iff.Label, actual[i + 1].Goto.Label).replace("==", "<>");
                            res += cod;
                            this.new_Optimizacion(3, iff.linea, iff.columna, "if(a==10) goto L1;\ngoto L2;\nL1:\n<instrucciones>\nL2:");
                            imprimir.unshift(false, false);
                        } else {
                            res += actual[i].IF.Codigo;
                        }
                    }
                } else if (iff.Valor.hasOwnProperty('Operador') && iff.Valor.Operador == "<>") {
                    let Valor1 = iff.Valor.Valor1;
                    let Valor2 = iff.Valor.Valor2;
                    //regla 3
                    if (i + 1 < actual.length && actual[i + 1].hasOwnProperty('Goto') && (i + 2 < actual.length && actual[i + 2].hasOwnProperty('Label') && actual[i + 2].Label == iff.Label)) {
                        let cod = actual[i].IF.Codigo.replace(iff.Label, actual[i + 1].Goto.Label).replace("<>", "==");
                        res += cod;
                        this.new_Optimizacion(3, iff.linea, iff.columna, "if(a==10) goto L1;\ngoto L2;\nL1:\n<instrucciones>\nL2:");
                        imprimir.unshift(false, false);
                    } else {
                        res += actual[i].IF.Codigo;
                    }
                } else if (iff.Valor.hasOwnProperty('Operador') && iff.Valor.Operador == "<") {
                    let Valor1 = iff.Valor.Valor1;
                    let Valor2 = iff.Valor.Valor2;
                    //regla 3
                    if (i + 1 < actual.length && actual[i + 1].hasOwnProperty('Goto') && (i + 2 < actual.length && actual[i + 2].hasOwnProperty('Label') && actual[i + 2].Label == iff.Label)) {
                        let cod = actual[i].IF.Codigo.replace(iff.Label, actual[i + 1].Goto.Label).replace("<", ">=");
                        res += cod;
                        this.new_Optimizacion(3, iff.linea, iff.columna, "if(a==10) goto L1;\ngoto L2;\nL1:\n<instrucciones>\nL2:");
                        imprimir.unshift(false, false);
                    } else {
                        res += actual[i].IF.Codigo;
                    }
                } else if (iff.Valor.hasOwnProperty('Operador') && iff.Valor.Operador == ">") {
                    let Valor1 = iff.Valor.Valor1;
                    let Valor2 = iff.Valor.Valor2;
                    //regla 3
                    if (i + 1 < actual.length && actual[i + 1].hasOwnProperty('Goto') && (i + 2 < actual.length && actual[i + 2].hasOwnProperty('Label') && actual[i + 2].Label == iff.Label)) {
                        let cod = actual[i].IF.Codigo.replace(iff.Label, actual[i + 1].Goto.Label).replace(">", "<=");
                        res += cod;
                        this.new_Optimizacion(3, iff.linea, iff.columna, "if(a==10) goto L1;\ngoto L2;\nL1:\n<instrucciones>\nL2:");
                        imprimir.unshift(false, false);
                    } else {
                        res += actual[i].IF.Codigo;
                    }
                } else if (iff.Valor.hasOwnProperty('Operador') && iff.Valor.Operador == ">=") {
                    let Valor1 = iff.Valor.Valor1;
                    let Valor2 = iff.Valor.Valor2;
                    //regla 3
                    if (i + 1 < actual.length && actual[i + 1].hasOwnProperty('Goto') && (i + 2 < actual.length && actual[i + 2].hasOwnProperty('Label') && actual[i + 2].Label == iff.Label)) {
                        let cod = actual[i].IF.Codigo.replace(iff.Label, actual[i + 1].Goto.Label).replace(">=", "<");
                        res += cod;
                        this.new_Optimizacion(3, iff.linea, iff.columna, "if(a==10) goto L1;\ngoto L2;\nL1:\n<instrucciones>\nL2:");
                        imprimir.unshift(false, false);
                    } else {
                        res += actual[i].IF.Codigo;
                    }
                } else if (iff.Valor.hasOwnProperty('Operador') && iff.Valor.Operador == "<=") {
                    let Valor1 = iff.Valor.Valor1;
                    let Valor2 = iff.Valor.Valor2;
                    //regla 3
                    if (i + 1 < actual.length && actual[i + 1].hasOwnProperty('Goto') && (i + 2 < actual.length && actual[i + 2].hasOwnProperty('Label') && actual[i + 2].Label == iff.Label)) {
                        let cod = actual[i].IF.Codigo.replace(iff.Label, actual[i + 1].Goto.Label).replace("<=", ">");
                        res += cod;
                        this.new_Optimizacion(3, iff.linea, iff.columna, "if(a==10) goto L1;\ngoto L2;\nL1:\n<instrucciones>\nL2:");
                        imprimir.unshift(false, false);
                    } else {
                        res += actual[i].IF.Codigo;
                    }
                } else {
                    res += actual[i].IF.Codigo;
                }
            } else if (actual[i].hasOwnProperty('Asignacion')) {
                let a = actual[i].Asignacion;
                if (a.hasOwnProperty('Temporal')) {
                    let temp = a.Temporal;

                    //REGLA 8 y 12
                    if (a.Valor.hasOwnProperty('Operador') && (a.Valor.Operador == "+" || a.Valor.Operador == "-")) {
                        if (a.Valor.Valor1.hasOwnProperty('Numero') && a.Valor.Valor1.Numero == "0") {
                            let Valor2 = a.Valor.Valor2;
                            if (Valor2.hasOwnProperty('Temporal')) {
                                if (temp == Valor2.Temporal) {

                                    if (a.Valor.Operador == "+") {
                                        this.new_Optimizacion(8, a.linea, a.columna, "x = x + 0");
                                    } else {
                                        this.new_Optimizacion(9, a.linea, a.columna, "x = x - 0");
                                    }
                                } else {
                                    res += temp + " = " + Valor2.Temporal + ";\n";
                                    if (a.Valor.Operador == "+") {
                                        this.new_Optimizacion(12, a.linea, a.columna, "x = y + 0");
                                    } else {
                                        this.new_Optimizacion(13, a.linea, a.columna, "x = y - 0");
                                    }

                                }

                            } else if (Valor2.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor2.Numero + ";\n";
                                if (a.Valor.Operador == "+") {
                                    this.new_Optimizacion(12, a.linea, a.columna, "x = y + 0");
                                } else {
                                    this.new_Optimizacion(13, a.linea, a.columna, "x = y - 0");
                                }
                            } else if (Valor2.hasOwnProperty('H')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = H;\n";
                                if (a.Valor.Operador == "+") {
                                    this.new_Optimizacion(12, a.linea, a.columna, "x = y + 0");
                                } else {
                                    this.new_Optimizacion(13, a.linea, a.columna, "x = y - 0");
                                }


                            } else if (Valor2.hasOwnProperty('P')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = P;\n";
                                if (a.Valor.Operador == "+") {
                                    this.new_Optimizacion(12, a.linea, a.columna, "x = y + 0");
                                } else {
                                    this.new_Optimizacion(13, a.linea, a.columna, "x = y - 0");
                                }
                            } else if (Valor2.hasOwnProperty('E')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = E;\n";
                                if (a.Valor.Operador == "+") {
                                    this.new_Optimizacion(12, a.linea, a.columna, "x = y + 0");
                                } else {
                                    this.new_Optimizacion(13, a.linea, a.columna, "x = y - 0");
                                }
                            }
                        } else if (a.Valor.hasOwnProperty('Valor2') && a.Valor.Valor2.hasOwnProperty('Numero') && a.Valor.Valor2.Numero == "0") {
                            let Valor1 = a.Valor.Valor1;
                            if (Valor1.hasOwnProperty('Temporal')) {
                                if (temp == Valor1.Temporal) {
                                    if (a.Valor.Operador == "+") {
                                        this.new_Optimizacion(8, a.linea, a.columna, "x = x + 0");
                                    } else {
                                        this.new_Optimizacion(9, a.linea, a.columna, "x = x - 0");
                                    }
                                } else {
                                    res += temp + " = " + Valor1.Temporal + ";\n";
                                    if (a.Valor.Operador == "+") {
                                        this.new_Optimizacion(12, a.linea, a.columna, "x = y + 0");
                                    } else {
                                        this.new_Optimizacion(13, a.linea, a.columna, "x = y - 0");
                                    }
                                }

                            } else if (Valor1.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor2.Numero + ";\n";
                                if (a.Valor.Operador == "+") {
                                    this.new_Optimizacion(12, a.linea, a.columna, "x = y + 0");
                                } else {
                                    this.new_Optimizacion(13, a.linea, a.columna, "x = y - 0");
                                }
                            } else if (Valor1.hasOwnProperty('H')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = H;\n";
                                if (a.Valor.Operador == "+") {
                                    this.new_Optimizacion(12, a.linea, a.columna, "x = y + 0");
                                } else {
                                    this.new_Optimizacion(13, a.linea, a.columna, "x = y - 0");
                                }
                            } else if (Valor1.hasOwnProperty('P')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = P;\n";
                                if (a.Valor.Operador == "+") {
                                    this.new_Optimizacion(12, a.linea, a.columna, "x = y + 0");
                                } else {
                                    this.new_Optimizacion(13, a.linea, a.columna, "x = y - 0");
                                }
                            } else if (Valor1.hasOwnProperty('E')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = E;\n";
                                if (a.Valor.Operador == "+") {
                                    this.new_Optimizacion(12, a.linea, a.columna, "x = y + 0");
                                } else {
                                    this.new_Optimizacion(13, a.linea, a.columna, "x = y - 0");
                                }
                            }
                        } else {

                            if (a.Valor.hasOwnProperty('Valor2')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                if (Valor1.hasOwnProperty('P')) {
                                    if (Valor2.hasOwnProperty('Numero')) {
                                        res += temp + " = P " + a.Valor.Operador + " " + Valor2.Numero + ";\n";
                                    } else if (Valor2.hasOwnProperty('Temporal')) {
                                        res += temp + " = P " + a.Valor.Operador + " " + Valor2.Temporal + ";\n";
                                    } else if (Valor2.hasOwnProperty('H')) {
                                        res += temp + " = P " + a.Valor.Operador + " H;\n";
                                    } else if (Valor2.hasOwnProperty('E')) {
                                        res += temp + " = P " + a.Valor.Operador + " E;\n";
                                    } else if (Valor2.hasOwnProperty('P')) {
                                        res += temp + " = P " + a.Valor.Operador + " P;\n";
                                    }
                                } else if (Valor1.hasOwnProperty('H')) {
                                    if (Valor2.hasOwnProperty('Numero')) {
                                        res += temp + " = H " + a.Valor.Operador + " " + Valor2.Numero + ";\n";
                                    } else if (Valor2.hasOwnProperty('Temporal')) {
                                        res += temp + " = H " + a.Valor.Operador + " " + Valor2.Temporal + ";\n";
                                    } else if (Valor2.hasOwnProperty('H')) {
                                        res += temp + " = H " + a.Valor.Operador + " H;\n";
                                    } else if (Valor2.hasOwnProperty('E')) {
                                        res += temp + " = H " + a.Valor.Operador + " E;\n";
                                    } else if (Valor2.hasOwnProperty('P')) {
                                        res += temp + " = H " + a.Valor.Operador + " P;\n";
                                    }
                                } else if (Valor1.hasOwnProperty('Numero')) {
                                    if (Valor2.hasOwnProperty('Numero')) {
                                        res += temp + " = " + Valor1.Numero + " " + a.Valor.Operador + " " + Valor2.Numero + ";\n";
                                    } else if (Valor2.hasOwnProperty('Temporal')) {
                                        res += temp + " = " + Valor1.Numero + " " + a.Valor.Operador + " " + Valor2.Temporal + ";\n";
                                    } else if (Valor2.hasOwnProperty('H')) {
                                        res += temp + " = " + Valor1.Numero + " " + a.Valor.Operador + " H;\n";
                                    } else if (Valor2.hasOwnProperty('E')) {
                                        res += temp + " = " + Valor1.Numero + " " + a.Valor.Operador + " E;\n";
                                    } else if (Valor2.hasOwnProperty('P')) {
                                        res += temp + " = " + Valor1.Numero + " " + a.Valor.Operador + " P;\n";
                                    }
                                } else if (Valor1.hasOwnProperty('Temporal')) {
                                    if (Valor2.hasOwnProperty('Numero')) {

                                        res += temp + " = " + Valor1.Temporal + " " + a.Valor.Operador + " " + Valor2.Numero + ";\n";
                                    } else if (Valor2.hasOwnProperty('Temporal')) {
                                        res += temp + " = " + Valor1.Temporal + " " + a.Valor.Operador + " " + Valor2.Temporal + ";\n";
                                    } else if (Valor2.hasOwnProperty('H')) {
                                        res += temp + " = " + Valor1.Temporal + " " + a.Valor.Operador + " H;\n";
                                    } else if (Valor2.hasOwnProperty('E')) {
                                        res += temp + " = " + Valor1.Temporal + " " + a.Valor.Operador + " E;\n";
                                    } else if (Valor2.hasOwnProperty('P')) {
                                        res += temp + " = " + Valor1.Temporal + " " + a.Valor.Operador + " P;\n";
                                    } else if (Valor2.hasOwnProperty('Heap')) {
                                        res += temp + " = " + Valor1.Temporal + " " + a.Valor.Operador + " P;\n";
                                    }
                                } else {
                                    console.log(a);
                                }
                            }


                        }
                        /***
                         * 
                         *  TERMINA LAS REGLAS 8, 9 , 12, 13
                         * 
                         * 
                         * 
                         */
                    } else if (a.Valor.hasOwnProperty('Operador') && a.Valor.Operador == "*") {
                        /*************
                         * 
                         * EMPIEZAN LAS REGLAS 
                         *  10, 14,16,17
                         * 
                         * 
                         * 
                         * 
                         */

                        //10 y 14
                        if (a.Valor.Valor1.hasOwnProperty('Numero') && a.Valor.Valor1.Numero == "1") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor2.hasOwnProperty('Temporal')) {
                                if (temp == Valor2.Temporal) {
                                    this.new_Optimizacion(10, a.linea, a.columna, "x = x * 1");
                                } else {
                                    res += temp + " = " + Valor2.Temporal + ";\n";
                                    this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");
                                }

                            } else if (Valor2.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor2.Numero + ";\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");

                            } else if (Valor2.hasOwnProperty('H')) {
                                res += temp + " = H;\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");



                            } else if (Valor2.hasOwnProperty('P')) {
                                res += temp + " = P;\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");
                            } else if (Valor2.hasOwnProperty('E')) {
                                res += temp + " = E;\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");

                            }

                        } else if (a.Valor.Valor2.hasOwnProperty('Numero') && a.Valor.Valor2.Numero == "1") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor1.hasOwnProperty('Temporal')) {
                                if (temp == Valor1.Temporal) {
                                    this.new_Optimizacion(10, a.linea, a.columna, "x = x * 1");
                                } else {
                                    res += temp + " = " + Valor1.Temporal + ";\n";
                                    this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");
                                }

                            } else if (Valor1.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor1.Numero + ";\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");

                            } else if (Valor1.hasOwnProperty('H')) {
                                res += temp + " = H;\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");




                            } else if (Valor1.hasOwnProperty('P')) {
                                res += temp + " = P;\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");
                            } else if (Valor1.hasOwnProperty('E')) {
                                res += temp + " = E;\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");

                            }

                        } //10 y 14
                        // 16
                        else if (a.Valor.Valor1.hasOwnProperty('Numero') && a.Valor.Valor1.Numero == "2") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor2.hasOwnProperty('Temporal')) {
                                res += temp + " = " + Valor2.Temporal + " + " + Valor2.Temporal + ";\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");


                            } else if (Valor2.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor2.Numero + " + " + Valor2.Numero + ";\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");

                            } else if (Valor2.hasOwnProperty('H')) {
                                res += temp + " = H + H;\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");



                            } else if (Valor2.hasOwnProperty('P')) {
                                res += temp + " = P + P;\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");
                            } else if (Valor2.hasOwnProperty('E')) {
                                res += temp + " = E + E;\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");

                            }

                        } else if (a.Valor.Valor2.hasOwnProperty('Numero') && a.Valor.Valor2.Numero == "2") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor1.hasOwnProperty('Temporal')) {
                                res += temp + " = " + Valor1.Temporal + " + " + Valor1.Temporal + ";\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");


                            } else if (Valor1.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor1.Numero + " + " + Valor1.Numero + ";\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");

                            } else if (Valor1.hasOwnProperty('H')) {
                                res += temp + " = H + H;\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");




                            } else if (Valor1.hasOwnProperty('P')) {
                                res += temp + " = P + P;\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");
                            } else if (Valor1.hasOwnProperty('E')) {
                                res += temp + " = E + E;\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");

                            }
                            //terminan optimizaciones
                        } //16
                        // 17
                        else if (a.Valor.Valor1.hasOwnProperty('Numero') && a.Valor.Valor1.Numero == "0") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor2.hasOwnProperty('Temporal')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");


                            } else if (Valor2.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");

                            } else if (Valor2.hasOwnProperty('H')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");



                            } else if (Valor2.hasOwnProperty('P')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");
                            } else if (Valor2.hasOwnProperty('E')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");

                            }

                        } else if (a.Valor.Valor2.hasOwnProperty('Numero') && a.Valor.Valor2.Numero == "0") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor1.hasOwnProperty('Temporal')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");


                            } else if (Valor1.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");

                            } else if (Valor1.hasOwnProperty('H')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");




                            } else if (Valor1.hasOwnProperty('P')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");
                            } else if (Valor1.hasOwnProperty('E')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");

                            }
                            //terminan optimizaciones
                        } else {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            if (Valor1.hasOwnProperty('P')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = P * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = P * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = P * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = P * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = P * P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('H')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = H * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = H * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = H * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = H * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = H * P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('Numero')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = " + Valor1.Numero + " * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = " + Valor1.Numero + " * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = " + Valor1.Numero + " * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = " + Valor1.Numero + " * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = " + Valor1.Numero + " * P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('Temporal')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = " + Valor1.Temporal + " * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = " + Valor1.Temporal + " * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = " + Valor1.Temporal + " * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = " + Valor1.Temporal + " * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = " + Valor1.Temporal + " * P;\n";
                                } else if (Valor2.hasOwnProperty('Heap')) {
                                    res += temp + " = " + Valor1.Temporal + " * P;\n";
                                }
                            }
                        }




                    } else if (a.Valor.hasOwnProperty('Operador') && a.Valor.Operador == "/") {
                        /*************
                         * 
                         * EMPIEZAN LAS REGLAS 
                         *  11,15,18
                         * 
                         * 
                         * 
                         * 
                         */

                        //11
                        if (a.Valor.Valor1.hasOwnProperty('Numero') && a.Valor.Valor1.Numero == "1") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 11 y 15

                            if (Valor2.hasOwnProperty('Temporal')) {
                                if (temp == Valor2.Temporal) {
                                    this.new_Optimizacion(11, a.linea, a.columna, "x = x / 1");
                                } else {
                                    res += temp + " = " + Valor2.Temporal + ";\n";
                                    this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");
                                }

                            } else if (Valor2.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor2.Numero + ";\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");

                            } else if (Valor2.hasOwnProperty('H')) {
                                res += temp + " = H;\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");



                            } else if (Valor2.hasOwnProperty('P')) {
                                res += temp + " = P;\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");
                            } else if (Valor2.hasOwnProperty('E')) {
                                res += temp + " = E;\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");

                            }

                        } else if (a.Valor.Valor2.hasOwnProperty('Numero') && a.Valor.Valor2.Numero == "1") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor1.hasOwnProperty('Temporal')) {
                                if (temp == Valor1.Temporal) {
                                    this.new_Optimizacion(11, a.linea, a.columna, "x = x / 1");
                                } else {
                                    res += temp + " = " + Valor1.Temporal + ";\n";
                                    this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");
                                }

                            } else if (Valor1.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor1.Numero + ";\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");

                            } else if (Valor1.hasOwnProperty('H')) {
                                res += temp + " = H;\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");




                            } else if (Valor1.hasOwnProperty('P')) {
                                res += temp + " = P;\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");
                            } else if (Valor1.hasOwnProperty('E')) {
                                res += temp + " = E;\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");

                            }

                        } //10 y 14
                        // 17
                        else if (a.Valor.Valor1.hasOwnProperty('Numero') && a.Valor.Valor1.Numero == "0") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor2.hasOwnProperty('Temporal')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(18, a.linea, a.columna, "x = 0 / y");


                            } else if (Valor2.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(18, a.linea, a.columna, "x = 0 / y");

                            } else if (Valor2.hasOwnProperty('H')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(18, a.linea, a.columna, "x = 0 / y");



                            } else if (Valor2.hasOwnProperty('P')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(18, a.linea, a.columna, "x = 0 / y");
                            } else if (Valor2.hasOwnProperty('E')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(18, a.linea, a.columna, "x = 0 / y");

                            }

                        } else {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            if (Valor1.hasOwnProperty('P')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = P * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = P * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = P * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = P * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = P * P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('H')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = H * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = H * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = H * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = H * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = H * P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('Numero')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = " + Valor1.Numero + " * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = " + Valor1.Numero + " * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = " + Valor1.Numero + " * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = " + Valor1.Numero + " * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = " + Valor1.Numero + " * P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('Temporal')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = " + Valor1.Temporal + " * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = " + Valor1.Temporal + " * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = " + Valor1.Temporal + " * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = " + Valor1.Temporal + " * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = " + Valor1.Temporal + " * P;\n";
                                } else if (Valor2.hasOwnProperty('Heap')) {
                                    res += temp + " = " + Valor1.Temporal + " * P;\n";
                                }
                            }
                        }




                    } else {


                        if (a.Valor.hasOwnProperty('Operador')) {
                            let Operador = a.Valor.Operador;
                            if (a.Valor.hasOwnProperty('Valor1')) {
                                if (a.Valor.hasOwnProperty('Valor2')) {
                                    let Valor1 = a.Valor.Valor1;
                                    let Valor2 = a.Valor.Valor2;

                                    if (Valor1.hasOwnProperty('P')) {
                                        if (Valor2.hasOwnProperty('Numero')) {
                                            res += temp + " = P " + Operador + " " + Valor2.Numero + ";\n";
                                        } else if (Valor2.hasOwnProperty('Temporal')) {
                                            res += temp + " = P " + Operador + " " + Valor2.Temporal + ";\n";
                                        } else if (Valor2.hasOwnProperty('H')) {
                                            res += temp + " = P " + Operador + "  H;\n";
                                        } else if (Valor2.hasOwnProperty('E')) {
                                            res += temp + " = P " + Operador + " E;\n";
                                        } else if (Valor2.hasOwnProperty('P')) {
                                            res += temp + " = P " + Operador + " P;\n";
                                        } else if (Valor2.hasOwnProperty('Stack')) {
                                            res += temp + " = P " + Operador + " P;\n";
                                        }
                                    } else if (Valor1.hasOwnProperty('H')) {
                                        if (Valor2.hasOwnProperty('Numero')) {
                                            res += temp + " = H " + Operador + " " + Valor2.Numero + ";\n";
                                        } else if (Valor2.hasOwnProperty('Temporal')) {
                                            res += temp + " = H " + Operador + " " + Valor2.Temporal + ";\n";
                                        } else if (Valor2.hasOwnProperty('H')) {
                                            res += temp + " = H " + Operador + " H;\n";
                                        } else if (Valor2.hasOwnProperty('E')) {
                                            res += temp + " = H " + Operador + " E;\n";
                                        } else if (Valor2.hasOwnProperty('P')) {
                                            res += temp + " = H " + Operador + " P;\n";
                                        }
                                    } else if (Valor1.hasOwnProperty('Numero')) {
                                        if (Valor2.hasOwnProperty('Numero')) {
                                            res += temp + " = " + Valor1.Numero + " " + Operador + " " + Valor2.Numero + ";\n";
                                        } else if (Valor2.hasOwnProperty('Temporal')) {
                                            res += temp + " = " + Valor1.Numero + " " + Operador + " " + Valor2.Temporal + ";\n";
                                        } else if (Valor2.hasOwnProperty('H')) {
                                            res += temp + " = " + Valor1.Numero + " " + Operador + " H;\n";
                                        } else if (Valor2.hasOwnProperty('E')) {
                                            res += temp + " = " + Valor1.Numero + " " + Operador + " E;\n";
                                        } else if (Valor2.hasOwnProperty('P')) {
                                            res += temp + " = " + Valor1.Numero + " " + Operador + " P;\n";
                                        }
                                    } else if (Valor1.hasOwnProperty('Temporal')) {
                                        if (Valor2.hasOwnProperty('Numero')) {
                                            res += temp + " = " + Valor1.Temporal + " " + Operador + " " + Valor2.Numero + ";\n";
                                        } else if (Valor2.hasOwnProperty('Temporal')) {
                                            res += temp + " = " + Valor1.Temporal + " " + Operador + " " + Valor2.Temporal + ";\n";
                                        } else if (Valor2.hasOwnProperty('H')) {
                                            res += temp + " = " + Valor1.Temporal + " " + Operador + " H;\n";
                                        } else if (Valor2.hasOwnProperty('E')) {
                                            res += temp + " = " + Valor1.Temporal + " " + Operador + " E;\n";
                                        } else if (Valor2.hasOwnProperty('P')) {
                                            res += temp + " = " + Valor1.Temporal + " " + Operador + " P;\n";
                                        }
                                    }

                                } else {
                                    // unario
                                    let Valor1 = a.Valor.Valor1;
                                    if (Valor1.hasOwnProperty('Numero')) {
                                        res += temp + " = " + Operador + " " + Valor1.Numero + ";\n";
                                    } else if (Valor1.hasOwnProperty('Temporal')) {
                                        res += temp + " = " + Operador + " " + Valor1.Temporal + ";\n";
                                    } else if (Valor1.hasOwnProperty('H')) {
                                        res += temp + " = " + Operador + " H;\n";
                                    } else if (Valor1.hasOwnProperty('E')) {
                                        res += temp + " = " + Operador + " E;\n";
                                    } else if (Valor1.hasOwnProperty('P')) {
                                        res += temp + " = " + Operador + " P;\n";
                                    }

                                }

                            }


                        } else {
                            // aqui van solo las copias
                            // forma T1 = T2;

                            if (a.Valor.hasOwnProperty('Numero')) {
                                res += temp + " = " + a.Valor.Numero + ";\n";
                            } else if (a.Valor.hasOwnProperty('Temporal')) {
                                if (i + 1 < actual.length && (actual.length - (i + 1)) >= 1) {
                                    let b = a.Valor.Temporal;
                                    if (i + 5 < actual.length) {
                                        let notExist = true;
                                        let cont = 0;
                                        for (let j = i + 1; j < i + 6; j++) {
                                            if (actual[j].hasOwnProperty('Asignacion') && actual[j].Asignacion.Temporal == temp) {
                                                notExist = false;
                                                break;
                                            } else if (actual[j].hasOwnProperty('Asignacion') && actual[j].Asignacion.Temporal == b) {
                                                let asigB = actual[j].Asignacion;
                                                if (asigB.Valor.hasOwnProperty('Temporal') && !asigB.Valor.hasOwnProperty('Operador') && asigB.Valor.Temporal == temp) {
                                                    imprimir.unshift(false);
                                                    notExist = true;

                                                    break;
                                                }
                                            } else if (actual[j].hasOwnProperty('Label')) {
                                                notExist = false;
                                                break;
                                            }
                                            notExist = false;
                                            cont++;
                                        }
                                        if (!notExist) {
                                            res += temp + " = " + a.Valor.Temporal + ";\n";
                                        } else {
                                            res += temp + " = " + a.Valor.Temporal + ";\n";
                                            for (let truess = 0; truess < cont; truess++) {
                                                imprimir.unshift(true);
                                            }
                                            this.new_Optimizacion(1, a.linea, a.columna, "T1 = b;\n b = T1;")
                                        }
                                    } else if (i + 4 < actual.length) {
                                        let notExist = true;
                                        let cont = 0;
                                        for (let j = i + 1; j < i + 5; j++) {
                                            if (actual[j].hasOwnProperty('Asignacion') && actual[j].Asignacion.Temporal == temp) {
                                                notExist = false;
                                                break;
                                            } else if (actual[j].hasOwnProperty('Asignacion') && actual[j].Asignacion.Temporal == b) {
                                                let asigB = actual[j].Asignacion;
                                                if (asigB.Valor.hasOwnProperty('Temporal') && !asigB.Valor.hasOwnProperty('Operador') && asigB.Valor.Temporal == temp) {
                                                    imprimir.unshift(false);
                                                    notExist = true;

                                                    break;
                                                }
                                            } else if (actual[j].hasOwnProperty('Label')) {
                                                notExist = false;
                                                break;
                                            }
                                            notExist = false;
                                            cont++;
                                        }
                                        if (!notExist) {
                                            res += temp + " = " + a.Valor.Temporal + ";\n";
                                        } else {
                                            res += temp + " = " + a.Valor.Temporal + ";\n";
                                            for (let truess = 0; truess < cont; truess++) {
                                                imprimir.unshift(true);
                                            }
                                            this.new_Optimizacion(1, a.linea, a.columna, "T1 = b;\n b = T1;")
                                        }
                                    } else if (i + 3 < actual.length) {
                                        let notExist = true;
                                        let cont = 0;
                                        for (let j = i + 1; j < i + 4; j++) {
                                            if (actual[j].hasOwnProperty('Asignacion') && actual[j].Asignacion.Temporal == temp) {
                                                notExist = false;
                                                break;
                                            } else if (actual[j].hasOwnProperty('Asignacion') && actual[j].Asignacion.Temporal == b) {
                                                let asigB = actual[j].Asignacion;
                                                if (asigB.Valor.hasOwnProperty('Temporal') && !asigB.Valor.hasOwnProperty('Operador') && asigB.Valor.Temporal == temp) {
                                                    imprimir.unshift(false);
                                                    notExist = true;

                                                    break;
                                                }
                                            } else if (actual[j].hasOwnProperty('Label')) {
                                                notExist = false;
                                                break;
                                            }
                                            notExist = false;
                                            cont++;
                                        }
                                        if (!notExist) {
                                            res += temp + " = " + a.Valor.Temporal + ";\n";
                                        } else {
                                            res += temp + " = " + a.Valor.Temporal + ";\n";
                                            for (let truess = 0; truess < cont; truess++) {
                                                imprimir.unshift(true);
                                            }
                                            this.new_Optimizacion(1, a.linea, a.columna, "T1 = b;\n b = T1;")
                                        }
                                    } else if (i + 2 < actual.length) {
                                        let notExist = true;
                                        let cont = 0;
                                        for (let j = i + 1; j < i + 3; j++) {
                                            if (actual[j].hasOwnProperty('Asignacion') && actual[j].Asignacion.Temporal == temp) {
                                                notExist = false;
                                                break;
                                            } else if (actual[j].hasOwnProperty('Asignacion') && actual[j].Asignacion.Temporal == b) {
                                                let asigB = actual[j].Asignacion;
                                                if (asigB.Valor.hasOwnProperty('Temporal') && !asigB.Valor.hasOwnProperty('Operador') && asigB.Valor.Temporal == temp) {
                                                    imprimir.unshift(false);
                                                    notExist = true;

                                                    break;
                                                }
                                            } else if (actual[j].hasOwnProperty('Label')) {
                                                notExist = false;
                                                break;
                                            }
                                            notExist = false;
                                            cont++;
                                        }
                                        if (!notExist) {
                                            res += temp + " = " + a.Valor.Temporal + ";\n";
                                        } else {
                                            res += temp + " = " + a.Valor.Temporal + ";\n";
                                            for (let truess = 0; truess < cont; truess++) {
                                                imprimir.unshift(true);
                                            }
                                            this.new_Optimizacion(1, a.linea, a.columna, "T1 = b;\n b = T1;")
                                        }
                                    } else if (i + 1 < actual.length) {
                                        let notExist = true;
                                        let cont = 0;
                                        for (let j = i + 1; j < i + 2; j++) {
                                            if (actual[j].hasOwnProperty('Asignacion') && actual[j].Asignacion.Temporal == temp) {
                                                notExist = false;
                                                break;
                                            } else if (actual[j].hasOwnProperty('Asignacion') && actual[j].Asignacion.Temporal == b) {
                                                let asigB = actual[j].Asignacion;
                                                if (asigB.Valor.hasOwnProperty('Temporal') && !asigB.Valor.hasOwnProperty('Operador') && asigB.Valor.Temporal == temp) {
                                                    imprimir.unshift(false);
                                                    notExist = true;

                                                    break;
                                                }
                                            } else if (actual[j].hasOwnProperty('Label')) {
                                                notExist = false;
                                                break;
                                            }
                                            notExist = false;
                                            cont++;
                                        }
                                        if (!notExist) {
                                            res += temp + " = " + a.Valor.Temporal + ";\n";
                                        } else {
                                            res += temp + " = " + a.Valor.Temporal + ";\n";
                                            for (let truess = 0; truess < cont; truess++) {
                                                imprimir.unshift(true);
                                            }
                                            this.new_Optimizacion(1, a.linea, a.columna, "T1 = b;\n b = T1;")
                                        }
                                    } else {
                                        res += temp + " = " + a.Valor.Temporal + ";\n";
                                    }
                                } else {
                                    res += temp + " = " + a.Valor.Temporal + ";\n";
                                }

                            } else if (a.Valor.hasOwnProperty('H')) {
                                res += temp + " = H;\n";
                            } else if (a.Valor.hasOwnProperty('E')) {
                                res += temp + " = E;\n";
                            } else if (a.Valor.hasOwnProperty('P')) {
                                res += temp + " = P;\n";
                            } else if (a.Valor.hasOwnProperty('Stack')) {
                                let Valor1 = a.Valor.Stack;
                                if (Valor1.hasOwnProperty('Numero')) {
                                    res += temp + " = Stack[" + Valor1.Numero + "];\n";
                                } else if (Valor1.hasOwnProperty('Temporal')) {
                                    res += temp + " = Stack[" + Valor1.Temporal + "];\n";
                                } else if (Valor1.hasOwnProperty('H')) {
                                    res += temp + " = Stack[" + " H];\n";
                                } else if (Valor1.hasOwnProperty('E')) {
                                    res += temp + " = Stack[" + " E];\n";
                                } else if (Valor1.hasOwnProperty('P')) {
                                    res += temp + " = Stack[" + " P];\n";
                                }
                            } else if (a.Valor.hasOwnProperty('Heap')) {
                                let Valor1 = a.Valor.Heap;
                                if (Valor1.hasOwnProperty('Numero')) {
                                    res += temp + " = Heap[" + Valor1.Numero + "];\n";
                                } else if (Valor1.hasOwnProperty('Temporal')) {
                                    res += temp + " = Heap[" + Valor1.Temporal + "];\n";
                                } else if (Valor1.hasOwnProperty('H')) {
                                    res += temp + " = Heap[" + " H];\n";
                                } else if (Valor1.hasOwnProperty('E')) {
                                    res += temp + " = Heap[" + " E];\n";
                                } else if (Valor1.hasOwnProperty('P')) {
                                    res += temp + " = Heap[" + " P];\n";
                                }
                            } else {
                                console.log(a.Valor)
                            }

                        }

                    }
                } else if (a.hasOwnProperty('H')) {

                    let temp = "H";

                    //REGLA 8 y 12
                    if (a.Valor.hasOwnProperty('Operador') && a.Valor.Operador == "+") {
                        if (a.Valor.Valor1.hasOwnProperty('Numero') && a.Valor.Valor1.Numero == "0") {
                            let Valor2 = a.Valor.Valor2;
                            if (Valor2.hasOwnProperty('Temporal')) {

                                res += temp + " = " + Valor2.Temporal + ";\n";
                                this.new_Optimizacion(12, a.linea, a.columna, "x = y " + a.Valor.Operador + "0");


                            } else if (Valor2.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor2.Numero + ";\n";
                                this.new_Optimizacion(12, a.linea, a.columna, "x = y " + a.Valor.Operador + "0");
                            } else if (Valor2.hasOwnProperty('H')) {
                                this.new_Optimizacion(8, a.linea, a.columna, "x = x " + a.Valor.Operador + "0");




                            } else if (Valor2.hasOwnProperty('P')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = P;\n";
                                this.new_Optimizacion(12, a.linea, a.columna, "x = y " + a.Valor.Operador + "0");
                            } else if (Valor2.hasOwnProperty('E')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = E;\n";
                                this.new_Optimizacion(12, a.linea, a.columna, "x = y " + a.Valor.Operador + "0");
                            }
                        } else if (a.Valor.hasOwnProperty('Valor2') && a.Valor.Valor2.hasOwnProperty('Numero') && a.Valor.Valor2.Numero == "0") {
                            let Valor1 = a.Valor.Valor1;
                            if (Valor1.hasOwnProperty('Temporal')) {
                                res += temp + " = " + Valor1.Numero + ";\n";
                                this.new_Optimizacion(12, a.linea, a.columna, "x = y " + a.Valor.Operador + "0");


                            } else if (Valor1.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor2.Numero + ";\n";
                                this.new_Optimizacion(12, a.linea, a.columna, "x = y " + a.Valor.Operador + "0");
                            } else if (Valor1.hasOwnProperty('H')) {
                                this.new_Optimizacion(8, a.linea, a.columna, "x = x " + a.Valor.Operador + "0");
                            } else if (Valor1.hasOwnProperty('P')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = P;\n";
                                this.new_Optimizacion(12, a.linea, a.columna, "x = x " + a.Valor.Operador + "0");
                            } else if (Valor1.hasOwnProperty('E')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = E;\n";
                                this.new_Optimizacion(12, a.linea, a.columna, "x = x " + a.Valor.Operador + "0");
                            }
                        } else {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            if (Valor1.hasOwnProperty('P')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = P + " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = P + " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = P + H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = P + E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = P + P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('H')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = H + " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = H + " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = H + H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = H + E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = H + P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('Numero')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = " + Valor1.Numero + " + " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = " + Valor1.Numero + " + " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = " + Valor1.Numero + " + H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = " + Valor1.Numero + " + E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = " + Valor1.Numero + " + P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('Temporal')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = " + Valor1.Temporal + " + " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = " + Valor1.Temporal + " + " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = " + Valor1.Temporal + " + H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = " + Valor1.Temporal + " + E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = " + Valor1.Temporal + " + P;\n";
                                } else if (Valor2.hasOwnProperty('Heap')) {
                                    res += temp + " = " + Valor1.Temporal + " + P;\n";
                                }
                            }

                        }
                    } else if (a.Valor.hasOwnProperty('Operador') && a.Valor.Operador == "*") {
                        /*************
                         * 
                         * EMPIEZAN LAS REGLAS 
                         *  10, 14,16,17
                         * 
                         * 
                         * 
                         * 
                         */

                        //10 y 14
                        if (a.Valor.Valor1.hasOwnProperty('Numero') && a.Valor.Valor1.Numero == "1") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor2.hasOwnProperty('Temporal')) {
                                res += temp + " = " + Valor2.Temporal + ";\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");

                            } else if (Valor2.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor2.Numero + ";\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");

                            } else if (Valor2.hasOwnProperty('H')) {

                                this.new_Optimizacion(10, a.linea, a.columna, "x = x * 1");

                            } else if (Valor2.hasOwnProperty('P')) {
                                res += temp + " = P;\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");
                            } else if (Valor2.hasOwnProperty('E')) {
                                res += temp + " = E;\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");

                            }

                        } else if (a.Valor.Valor2.hasOwnProperty('Numero') && a.Valor.Valor2.Numero == "1") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor1.hasOwnProperty('Temporal')) {
                                res += temp + " = " + Valor1.Temporal + ";\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");


                            } else if (Valor1.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor1.Numero + ";\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");

                            } else if (Valor1.hasOwnProperty('H')) {
                                this.new_Optimizacion(10, a.linea, a.columna, "x = x * 1");




                            } else if (Valor1.hasOwnProperty('P')) {
                                res += temp + " = P;\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");
                            } else if (Valor1.hasOwnProperty('E')) {
                                res += temp + " = E;\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");

                            }

                        } //10 y 14
                        // 16
                        else if (a.Valor.Valor1.hasOwnProperty('Numero') && a.Valor.Valor1.Numero == "2") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor2.hasOwnProperty('Temporal')) {
                                res += temp + " = " + Valor2.Temporal + " + " + Valor2.Temporal + ";\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");


                            } else if (Valor2.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor2.Numero + " + " + Valor2.Numero + ";\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");

                            } else if (Valor2.hasOwnProperty('H')) {
                                res += temp + " = H + H;\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");



                            } else if (Valor2.hasOwnProperty('P')) {
                                res += temp + " = P + P;\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");
                            } else if (Valor2.hasOwnProperty('E')) {
                                res += temp + " = E + E;\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");

                            }

                        } else if (a.Valor.Valor2.hasOwnProperty('Numero') && a.Valor.Valor2.Numero == "2") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor1.hasOwnProperty('Temporal')) {
                                res += temp + " = " + Valor1.Temporal + " + " + Valor1.Temporal + ";\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");


                            } else if (Valor1.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor1.Numero + " + " + Valor1.Numero + ";\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");

                            } else if (Valor1.hasOwnProperty('H')) {
                                res += temp + " = H + H;\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");




                            } else if (Valor1.hasOwnProperty('P')) {
                                res += temp + " = P + P;\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");
                            } else if (Valor1.hasOwnProperty('E')) {
                                res += temp + " = E + E;\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");

                            }
                            //terminan optimizaciones
                        } //16
                        // 17
                        else if (a.Valor.Valor1.hasOwnProperty('Numero') && a.Valor.Valor1.Numero == "0") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor2.hasOwnProperty('Temporal')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");


                            } else if (Valor2.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");

                            } else if (Valor2.hasOwnProperty('H')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");



                            } else if (Valor2.hasOwnProperty('P')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");
                            } else if (Valor2.hasOwnProperty('E')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");

                            }

                        } else if (a.Valor.Valor2.hasOwnProperty('Numero') && a.Valor.Valor2.Numero == "0") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor1.hasOwnProperty('Temporal')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");


                            } else if (Valor1.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");

                            } else if (Valor1.hasOwnProperty('H')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");




                            } else if (Valor1.hasOwnProperty('P')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");
                            } else if (Valor1.hasOwnProperty('E')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");

                            }
                            //terminan optimizaciones
                        } else {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            if (Valor1.hasOwnProperty('P')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = P * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = P * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = P * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = P * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = P * P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('H')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = H * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = H * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = H * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = H * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = H * P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('Numero')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = " + Valor1.Numero + " * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = " + Valor1.Numero + " * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = " + Valor1.Numero + " * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = " + Valor1.Numero + " * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = " + Valor1.Numero + " * P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('Temporal')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = " + Valor1.Temporal + " * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = " + Valor1.Temporal + " * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = " + Valor1.Temporal + " * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = " + Valor1.Temporal + " * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = " + Valor1.Temporal + " * P;\n";
                                } else if (Valor2.hasOwnProperty('Heap')) {
                                    res += temp + " = " + Valor1.Temporal + " * P;\n";
                                }
                            }
                        }




                    } else if (a.Valor.hasOwnProperty('Operador') && a.Valor.Operador == "/") {
                        /*************
                         * 
                         * EMPIEZAN LAS REGLAS 
                         *  11,15,18
                         * 
                         * 
                         * 
                         * 
                         */

                        //11
                        if (a.Valor.Valor1.hasOwnProperty('Numero') && a.Valor.Valor1.Numero == "1") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 11 y 15

                            if (Valor2.hasOwnProperty('Temporal')) {
                                res += temp + " = " + Valor2.Temporal + ";\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");


                            } else if (Valor2.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor2.Numero + ";\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");

                            } else if (Valor2.hasOwnProperty('H')) {
                                this.new_Optimizacion(11, a.linea, a.columna, "x = x / 1");



                            } else if (Valor2.hasOwnProperty('P')) {
                                res += temp + " = P;\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");
                            } else if (Valor2.hasOwnProperty('E')) {
                                res += temp + " = E;\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");

                            }

                        } else if (a.Valor.Valor2.hasOwnProperty('Numero') && a.Valor.Valor2.Numero == "1") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor1.hasOwnProperty('Temporal')) {
                                res += temp + " = " + Valor1.Temporal + ";\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");


                            } else if (Valor1.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor1.Numero + ";\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");

                            } else if (Valor1.hasOwnProperty('H')) {
                                this.new_Optimizacion(11, a.linea, a.columna, "x = x / 1");




                            } else if (Valor1.hasOwnProperty('P')) {
                                res += temp + " = P;\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");
                            } else if (Valor1.hasOwnProperty('E')) {
                                res += temp + " = E;\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");

                            }

                        } //10 y 14
                        // 17
                        else if (a.Valor.Valor1.hasOwnProperty('Numero') && a.Valor.Valor1.Numero == "0") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor2.hasOwnProperty('Temporal')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(18, a.linea, a.columna, "x = 0 / y");


                            } else if (Valor2.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(18, a.linea, a.columna, "x = 0 / y");

                            } else if (Valor2.hasOwnProperty('H')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(18, a.linea, a.columna, "x = 0 / y");



                            } else if (Valor2.hasOwnProperty('P')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(18, a.linea, a.columna, "x = 0 / y");
                            } else if (Valor2.hasOwnProperty('E')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(18, a.linea, a.columna, "x = 0 / y");

                            }

                        } else {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            if (Valor1.hasOwnProperty('P')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = P * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = P * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = P * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = P * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = P * P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('H')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = H * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = H * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = H * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = H * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = H * P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('Numero')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = " + Valor1.Numero + " * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = " + Valor1.Numero + " * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = " + Valor1.Numero + " * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = " + Valor1.Numero + " * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = " + Valor1.Numero + " * P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('Temporal')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = " + Valor1.Temporal + " * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = " + Valor1.Temporal + " * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = " + Valor1.Temporal + " * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = " + Valor1.Temporal + " * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = " + Valor1.Temporal + " * P;\n";
                                } else if (Valor2.hasOwnProperty('Heap')) {
                                    res += temp + " = " + Valor1.Temporal + " * P;\n";
                                }
                            }
                        }




                    } else {

                        if (a.Valor.hasOwnProperty('Operador')) {
                            let Operador = a.Valor.Operador;
                            if (a.Valor.hasOwnProperty('Valor1')) {
                                if (a.Valor.hasOwnProperty('Valor2')) {
                                    let Valor1 = a.Valor.Valor1;
                                    let Valor2 = a.Valor.Valor2;

                                    if (Valor1.hasOwnProperty('P')) {
                                        if (Valor2.hasOwnProperty('Numero')) {
                                            res += temp + " = P " + Operador + " " + Valor2.Numero + ";\n";
                                        } else if (Valor2.hasOwnProperty('Temporal')) {
                                            res += temp + " = P " + Operador + " " + Valor2.Temporal + ";\n";
                                        } else if (Valor2.hasOwnProperty('H')) {
                                            res += temp + " = P " + Operador + "  H;\n";
                                        } else if (Valor2.hasOwnProperty('E')) {
                                            res += temp + " = P " + Operador + " E;\n";
                                        } else if (Valor2.hasOwnProperty('P')) {
                                            res += temp + " = P " + Operador + " P;\n";
                                        } else if (Valor2.hasOwnProperty('Stack')) {
                                            res += temp + " = P " + Operador + " P;\n";
                                        }
                                    } else if (Valor1.hasOwnProperty('H')) {
                                        if (Valor2.hasOwnProperty('Numero')) {
                                            res += temp + " = H " + Operador + " " + Valor2.Numero + ";\n";
                                        } else if (Valor2.hasOwnProperty('Temporal')) {
                                            res += temp + " = H " + Operador + " " + Valor2.Temporal + ";\n";
                                        } else if (Valor2.hasOwnProperty('H')) {
                                            res += temp + " = H " + Operador + " H;\n";
                                        } else if (Valor2.hasOwnProperty('E')) {
                                            res += temp + " = H " + Operador + " E;\n";
                                        } else if (Valor2.hasOwnProperty('P')) {
                                            res += temp + " = H " + Operador + " P;\n";
                                        }
                                    } else if (Valor1.hasOwnProperty('Numero')) {
                                        if (Valor2.hasOwnProperty('Numero')) {
                                            res += temp + " = " + Valor1.Numero + " " + Operador + " " + Valor2.Numero + ";\n";
                                        } else if (Valor2.hasOwnProperty('Temporal')) {
                                            res += temp + " = " + Valor1.Numero + " " + Operador + " " + Valor2.Temporal + ";\n";
                                        } else if (Valor2.hasOwnProperty('H')) {
                                            res += temp + " = " + Valor1.Numero + " " + Operador + " H;\n";
                                        } else if (Valor2.hasOwnProperty('E')) {
                                            res += temp + " = " + Valor1.Numero + " " + Operador + " E;\n";
                                        } else if (Valor2.hasOwnProperty('P')) {
                                            res += temp + " = " + Valor1.Numero + " " + Operador + " P;\n";
                                        }
                                    } else if (Valor1.hasOwnProperty('Temporal')) {
                                        if (Valor2.hasOwnProperty('Numero')) {
                                            res += temp + " = " + Valor1.Temporal + " " + Operador + " " + Valor2.Numero + ";\n";
                                        } else if (Valor2.hasOwnProperty('Temporal')) {
                                            res += temp + " = " + Valor1.Temporal + " " + Operador + " " + Valor2.Temporal + ";\n";
                                        } else if (Valor2.hasOwnProperty('H')) {
                                            res += temp + " = " + Valor1.Temporal + " " + Operador + " H;\n";
                                        } else if (Valor2.hasOwnProperty('E')) {
                                            res += temp + " = " + Valor1.Temporal + " " + Operador + " E;\n";
                                        } else if (Valor2.hasOwnProperty('P')) {
                                            res += temp + " = " + Valor1.Temporal + " " + Operador + " P;\n";
                                        }
                                    }

                                } else {
                                    // unario
                                    let Valor1 = a.Valor.Valor1;
                                    if (Valor1.hasOwnProperty('Numero')) {
                                        res += temp + " = " + Operador + " " + Valor1.Numero + ";\n";
                                    } else if (Valor1.hasOwnProperty('Temporal')) {
                                        res += temp + " = " + Operador + " " + Valor1.Temporal + ";\n";
                                    } else if (Valor1.hasOwnProperty('H')) {
                                        res += temp + " = " + Operador + " H;\n";
                                    } else if (Valor1.hasOwnProperty('E')) {
                                        res += temp + " = " + Operador + " E;\n";
                                    } else if (Valor1.hasOwnProperty('P')) {
                                        res += temp + " = " + Operador + " P;\n";
                                    }

                                }

                            }


                        } else {
                            // aqui van solo las copias
                            if (a.Valor.hasOwnProperty('Numero')) {
                                res += temp + " = " + a.Valor.Numero + ";\n";
                            } else if (a.Valor.hasOwnProperty('Temporal')) {
                                res += temp + " = " + a.Valor.Temporal + ";\n";
                            } else if (a.Valor.hasOwnProperty('H')) {
                                res += temp + " = H;\n";
                            } else if (a.Valor.hasOwnProperty('E')) {
                                res += temp + " = E;\n";
                            } else if (a.Valor.hasOwnProperty('P')) {
                                res += temp + " = P;\n";
                            } else if (a.Valor.hasOwnProperty('Stack')) {
                                let Valor1 = a.Valor.Stack;
                                if (Valor1.hasOwnProperty('Numero')) {
                                    res += temp + " = Stack[" + Valor1.Numero + "];\n";
                                } else if (Valor1.hasOwnProperty('Temporal')) {
                                    res += temp + " = Stack[" + Valor1.Temporal + "];\n";
                                } else if (Valor1.hasOwnProperty('H')) {
                                    res += temp + " = Stack[" + " H];\n";
                                } else if (Valor1.hasOwnProperty('E')) {
                                    res += temp + " = Stack[" + " E];\n";
                                } else if (Valor1.hasOwnProperty('P')) {
                                    res += temp + " = Stack[" + " P];\n";
                                }
                            } else if (a.Valor.hasOwnProperty('Heap')) {
                                let Valor1 = a.Valor.Heap;
                                if (Valor1.hasOwnProperty('Numero')) {
                                    res += temp + " = Heap[" + Valor1.Numero + "];\n";
                                } else if (Valor1.hasOwnProperty('Temporal')) {
                                    res += temp + " = Heap[" + Valor1.Temporal + "];\n";
                                } else if (Valor1.hasOwnProperty('H')) {
                                    res += temp + " = Heap[" + " H];\n";
                                } else if (Valor1.hasOwnProperty('E')) {
                                    res += temp + " = Heap[" + " E];\n";
                                } else if (Valor1.hasOwnProperty('P')) {
                                    res += temp + " = Heap[" + " P];\n";
                                }
                            } else {
                                console.log(a.Valor)
                            }

                        }

                    }
                } else if (a.hasOwnProperty('P')) {

                    let temp = "P";

                    //REGLA 8 y 12
                    if (a.Valor.hasOwnProperty('Operador') && (a.Valor.Operador == "+" || a.Valor.Operador == "-")) {
                        if (a.Valor.Valor1.hasOwnProperty('Numero') && a.Valor.Valor1.Numero == "0") {
                            let Valor2 = a.Valor.Valor2;
                            if (Valor2.hasOwnProperty('Temporal')) {

                                res += temp + " = " + Valor2.Temporal + ";\n";
                                if (a.Valor.Operador == "+") {
                                    this.new_Optimizacion(12, a.linea, a.columna, "x = y + 0");
                                } else {
                                    this.new_Optimizacion(13, a.linea, a.columna, "x = y - 0");
                                }


                            } else if (Valor2.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor2.Numero + ";\n";
                                if (a.Valor.Operador == "+") {
                                    this.new_Optimizacion(12, a.linea, a.columna, "x = y + 0");
                                } else {
                                    this.new_Optimizacion(13, a.linea, a.columna, "x = y - 0");
                                }
                            } else if (Valor2.hasOwnProperty('H')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = H;\n";
                                if (a.Valor.Operador == "+") {
                                    this.new_Optimizacion(12, a.linea, a.columna, "x = y + 0");
                                } else {
                                    this.new_Optimizacion(13, a.linea, a.columna, "x = y - 0");
                                }


                            } else if (Valor2.hasOwnProperty('P')) {
                                if (a.Valor.Operador == "+") {
                                    this.new_Optimizacion(8, a.linea, a.columna, "x = x - 0");
                                } else {
                                    this.new_Optimizacion(9, a.linea, a.columna, "x = x - 0");
                                }
                            } else if (Valor2.hasOwnProperty('E')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = E;\n";
                                if (a.Valor.Operador == "+") {
                                    this.new_Optimizacion(12, a.linea, a.columna, "x = y + 0");
                                } else {
                                    this.new_Optimizacion(13, a.linea, a.columna, "x = y - 0");
                                }
                            }
                        } else if (a.Valor.hasOwnProperty('Valor2') && a.Valor.Valor2.hasOwnProperty('Numero') && a.Valor.Valor2.Numero == "0") {
                            let Valor1 = a.Valor.Valor1;
                            if (Valor1.hasOwnProperty('Temporal')) {
                                if (temp == Valor1.Temporal) {
                                    if (a.Valor.Operador == "+") {
                                        this.new_Optimizacion(8, a.linea, a.columna, "x = x + 0");
                                    } else {
                                        this.new_Optimizacion(9, a.linea, a.columna, "x = x - 0");
                                    }
                                } else {
                                    res += temp + " = " + Valor1.Numero + ";\n";
                                    if (a.Valor.Operador == "+") {
                                        this.new_Optimizacion(12, a.linea, a.columna, "x = y + 0");
                                    } else {
                                        this.new_Optimizacion(13, a.linea, a.columna, "x = y - 0");
                                    }
                                }

                            } else if (Valor1.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor2.Numero + ";\n";
                                if (a.Valor.Operador == "+") {
                                    this.new_Optimizacion(12, a.linea, a.columna, "x = y + 0");
                                } else {
                                    this.new_Optimizacion(13, a.linea, a.columna, "x = y - 0");
                                }
                            } else if (Valor1.hasOwnProperty('H')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = H;\n";
                                if (a.Valor.Operador == "+") {
                                    this.new_Optimizacion(12, a.linea, a.columna, "x = y + 0");
                                } else {
                                    this.new_Optimizacion(13, a.linea, a.columna, "x = y - 0");
                                }
                            } else if (Valor1.hasOwnProperty('P')) {

                                if (a.Valor.Operador == "+") {
                                    this.new_Optimizacion(8, a.linea, a.columna, "x = x + 0");
                                } else {
                                    this.new_Optimizacion(9, a.linea, a.columna, "x = x - 0");
                                }
                            } else if (Valor1.hasOwnProperty('E')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = E;\n";
                                if (a.Valor.Operador == "+") {
                                    this.new_Optimizacion(12, a.linea, a.columna, "x = y + 0");
                                } else {
                                    this.new_Optimizacion(13, a.linea, a.columna, "x = y - 0");
                                }
                            }
                        } else {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            if (Valor1.hasOwnProperty('P')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = P " + a.Valor.Operador + " " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = P " + a.Valor.Operador + " " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = P " + a.Valor.Operador + " H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = P " + a.Valor.Operador + " E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = P " + a.Valor.Operador + " P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('H')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = H " + a.Valor.Operador + " " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = H " + a.Valor.Operador + " " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = H " + a.Valor.Operador + " H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = H " + a.Valor.Operador + " E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = H " + a.Valor.Operador + " P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('Numero')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = " + Valor1.Numero + " " + a.Valor.Operador + " " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = " + Valor1.Numero + " " + a.Valor.Operador + " " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = " + Valor1.Numero + " " + a.Valor.Operador + " H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = " + Valor1.Numero + " " + a.Valor.Operador + " E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = " + Valor1.Numero + " " + a.Valor.Operador + " P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('Temporal')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = " + Valor1.Temporal + " " + a.Valor.Operador + " " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = " + Valor1.Temporal + " " + a.Valor.Operador + " " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = " + Valor1.Temporal + " " + a.Valor.Operador + " H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = " + Valor1.Temporal + " " + a.Valor.Operador + " E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = " + Valor1.Temporal + " " + a.Valor.Operador + " P;\n";
                                } else if (Valor2.hasOwnProperty('Heap')) {
                                    res += temp + " = " + Valor1.Temporal + " " + a.Valor.Operador + " P;\n";
                                }
                            }

                        }
                    } else if (a.Valor.hasOwnProperty('Operador') && a.Valor.Operador == "*") {
                        /*************
                         * 
                         * EMPIEZAN LAS REGLAS 
                         *  10, 14,16,17
                         * 
                         * 
                         * 
                         * 
                         */

                        //10 y 14
                        if (a.Valor.Valor1.hasOwnProperty('Numero') && a.Valor.Valor1.Numero == "1") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor2.hasOwnProperty('Temporal')) {
                                res += temp + " = " + Valor2.Temporal + ";\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");


                            } else if (Valor2.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor2.Numero + ";\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");

                            } else if (Valor2.hasOwnProperty('H')) {
                                res += temp + " = H;\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");



                            } else if (Valor2.hasOwnProperty('P')) {
                                this.new_Optimizacion(10, a.linea, a.columna, "x = x * 1");
                            } else if (Valor2.hasOwnProperty('E')) {
                                res += temp + " = E;\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");

                            }

                        } else if (a.Valor.Valor2.hasOwnProperty('Numero') && a.Valor.Valor2.Numero == "1") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor1.hasOwnProperty('Temporal')) {
                                res += temp + " = " + Valor1.Temporal + ";\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");


                            } else if (Valor1.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor1.Numero + ";\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");

                            } else if (Valor1.hasOwnProperty('H')) {
                                res += temp + " = H;\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");




                            } else if (Valor1.hasOwnProperty('P')) {
                                this.new_Optimizacion(10, a.linea, a.columna, "x = x * 1");
                            } else if (Valor1.hasOwnProperty('E')) {
                                res += temp + " = E;\n";
                                this.new_Optimizacion(14, a.linea, a.columna, "x = y * 1");

                            }

                        } //10 y 14
                        // 16
                        else if (a.Valor.Valor1.hasOwnProperty('Numero') && a.Valor.Valor1.Numero == "2") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor2.hasOwnProperty('Temporal')) {
                                res += temp + " = " + Valor2.Temporal + " + " + Valor2.Temporal + ";\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");


                            } else if (Valor2.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor2.Numero + " + " + Valor2.Numero + ";\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");

                            } else if (Valor2.hasOwnProperty('H')) {
                                res += temp + " = H + H;\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");



                            } else if (Valor2.hasOwnProperty('P')) {
                                res += temp + " = P + P;\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");
                            } else if (Valor2.hasOwnProperty('E')) {
                                res += temp + " = E + E;\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");

                            }

                        } else if (a.Valor.Valor2.hasOwnProperty('Numero') && a.Valor.Valor2.Numero == "2") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor1.hasOwnProperty('Temporal')) {
                                res += temp + " = " + Valor1.Temporal + " + " + Valor1.Temporal + ";\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");


                            } else if (Valor1.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor1.Numero + " + " + Valor1.Numero + ";\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");

                            } else if (Valor1.hasOwnProperty('H')) {
                                res += temp + " = H + H;\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");




                            } else if (Valor1.hasOwnProperty('P')) {
                                res += temp + " = P + P;\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");
                            } else if (Valor1.hasOwnProperty('E')) {
                                res += temp + " = E + E;\n";
                                this.new_Optimizacion(16, a.linea, a.columna, "x = y * 2");

                            }
                            //terminan optimizaciones
                        } //16
                        // 17
                        else if (a.Valor.Valor1.hasOwnProperty('Numero') && a.Valor.Valor1.Numero == "0") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor2.hasOwnProperty('Temporal')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");


                            } else if (Valor2.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");

                            } else if (Valor2.hasOwnProperty('H')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");



                            } else if (Valor2.hasOwnProperty('P')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");
                            } else if (Valor2.hasOwnProperty('E')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");

                            }

                        } else if (a.Valor.Valor2.hasOwnProperty('Numero') && a.Valor.Valor2.Numero == "0") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor1.hasOwnProperty('Temporal')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");


                            } else if (Valor1.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");

                            } else if (Valor1.hasOwnProperty('H')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");




                            } else if (Valor1.hasOwnProperty('P')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");
                            } else if (Valor1.hasOwnProperty('E')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(17, a.linea, a.columna, "x = y * 0");

                            }
                            //terminan optimizaciones
                        } else {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            if (Valor1.hasOwnProperty('P')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = P * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = P * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = P * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = P * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = P * P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('H')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = H * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = H * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = H * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = H * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = H * P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('Numero')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = " + Valor1.Numero + " * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = " + Valor1.Numero + " * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = " + Valor1.Numero + " * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = " + Valor1.Numero + " * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = " + Valor1.Numero + " * P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('Temporal')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = " + Valor1.Temporal + " * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = " + Valor1.Temporal + " * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = " + Valor1.Temporal + " * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = " + Valor1.Temporal + " * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = " + Valor1.Temporal + " * P;\n";
                                } else if (Valor2.hasOwnProperty('Heap')) {
                                    res += temp + " = " + Valor1.Temporal + " * P;\n";
                                }
                            }
                        }




                    } else if (a.Valor.hasOwnProperty('Operador') && a.Valor.Operador == "/") {
                        /*************
                         * 
                         * EMPIEZAN LAS REGLAS 
                         *  11,15,18
                         * 
                         * 
                         * 
                         * 
                         */

                        //11
                        if (a.Valor.Valor1.hasOwnProperty('Numero') && a.Valor.Valor1.Numero == "1") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 11 y 15

                            if (Valor2.hasOwnProperty('Temporal')) {
                                res += temp + " = " + Valor2.Temporal + ";\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");


                            } else if (Valor2.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor2.Numero + ";\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");

                            } else if (Valor2.hasOwnProperty('H')) {
                                res += temp + " = H;\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");



                            } else if (Valor2.hasOwnProperty('P')) {
                                this.new_Optimizacion(11, a.linea, a.columna, "x = x / 1");
                            } else if (Valor2.hasOwnProperty('E')) {
                                res += temp + " = E;\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");

                            }

                        } else if (a.Valor.Valor2.hasOwnProperty('Numero') && a.Valor.Valor2.Numero == "1") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor1.hasOwnProperty('Temporal')) {
                                res += temp + " = " + Valor1.Temporal + ";\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");


                            } else if (Valor1.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = " + Valor1.Numero + ";\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");

                            } else if (Valor1.hasOwnProperty('H')) {
                                res += temp + " = H;\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");




                            } else if (Valor1.hasOwnProperty('P')) {
                                this.new_Optimizacion(11, a.linea, a.columna, "x = x / 1");
                            } else if (Valor1.hasOwnProperty('E')) {
                                res += temp + " = E;\n";
                                this.new_Optimizacion(15, a.linea, a.columna, "x = y / 1");

                            }

                        } //10 y 14
                        // 17
                        else if (a.Valor.Valor1.hasOwnProperty('Numero') && a.Valor.Valor1.Numero == "0") {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            //REGLA 10

                            if (Valor2.hasOwnProperty('Temporal')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(18, a.linea, a.columna, "x = 0 / y");


                            } else if (Valor2.hasOwnProperty('Numero')) {
                                let Valor1 = a.Valor.Valor1;
                                let Valor2 = a.Valor.Valor2;
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(18, a.linea, a.columna, "x = 0 / y");

                            } else if (Valor2.hasOwnProperty('H')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(18, a.linea, a.columna, "x = 0 / y");



                            } else if (Valor2.hasOwnProperty('P')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(18, a.linea, a.columna, "x = 0 / y");
                            } else if (Valor2.hasOwnProperty('E')) {
                                res += temp + " = 0;\n";
                                this.new_Optimizacion(18, a.linea, a.columna, "x = 0 / y");

                            }

                        } else {
                            let Valor1 = a.Valor.Valor1;
                            let Valor2 = a.Valor.Valor2;
                            if (Valor1.hasOwnProperty('P')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = P * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = P * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = P * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = P * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = P * P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('H')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = H * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = H * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = H * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = H * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = H * P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('Numero')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = " + Valor1.Numero + " * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = " + Valor1.Numero + " * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = " + Valor1.Numero + " * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = " + Valor1.Numero + " * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = " + Valor1.Numero + " * P;\n";
                                }
                            } else if (Valor1.hasOwnProperty('Temporal')) {
                                if (Valor2.hasOwnProperty('Numero')) {
                                    res += temp + " = " + Valor1.Temporal + " * " + Valor2.Numero + ";\n";
                                } else if (Valor2.hasOwnProperty('Temporal')) {
                                    res += temp + " = " + Valor1.Temporal + " * " + Valor2.Temporal + ";\n";
                                } else if (Valor2.hasOwnProperty('H')) {
                                    res += temp + " = " + Valor1.Temporal + " * H;\n";
                                } else if (Valor2.hasOwnProperty('E')) {
                                    res += temp + " = " + Valor1.Temporal + " * E;\n";
                                } else if (Valor2.hasOwnProperty('P')) {
                                    res += temp + " = " + Valor1.Temporal + " * P;\n";
                                } else if (Valor2.hasOwnProperty('Heap')) {
                                    res += temp + " = " + Valor1.Temporal + " * P;\n";
                                }
                            }
                        }




                    } else {

                        if (a.Valor.hasOwnProperty('Operador')) {
                            let Operador = a.Valor.Operador;
                            if (a.Valor.hasOwnProperty('Valor1')) {
                                if (a.Valor.hasOwnProperty('Valor2')) {
                                    let Valor1 = a.Valor.Valor1;
                                    let Valor2 = a.Valor.Valor2;

                                    if (Valor1.hasOwnProperty('P')) {
                                        if (Valor2.hasOwnProperty('Numero')) {
                                            res += temp + " = P " + Operador + " " + Valor2.Numero + ";\n";
                                        } else if (Valor2.hasOwnProperty('Temporal')) {
                                            res += temp + " = P " + Operador + " " + Valor2.Temporal + ";\n";
                                        } else if (Valor2.hasOwnProperty('H')) {
                                            res += temp + " = P " + Operador + "  H;\n";
                                        } else if (Valor2.hasOwnProperty('E')) {
                                            res += temp + " = P " + Operador + " E;\n";
                                        } else if (Valor2.hasOwnProperty('P')) {
                                            res += temp + " = P " + Operador + " P;\n";
                                        } else if (Valor2.hasOwnProperty('Stack')) {
                                            res += temp + " = P " + Operador + " P;\n";
                                        }
                                    } else if (Valor1.hasOwnProperty('H')) {
                                        if (Valor2.hasOwnProperty('Numero')) {
                                            res += temp + " = H " + Operador + " " + Valor2.Numero + ";\n";
                                        } else if (Valor2.hasOwnProperty('Temporal')) {
                                            res += temp + " = H " + Operador + " " + Valor2.Temporal + ";\n";
                                        } else if (Valor2.hasOwnProperty('H')) {
                                            res += temp + " = H " + Operador + " H;\n";
                                        } else if (Valor2.hasOwnProperty('E')) {
                                            res += temp + " = H " + Operador + " E;\n";
                                        } else if (Valor2.hasOwnProperty('P')) {
                                            res += temp + " = H " + Operador + " P;\n";
                                        }
                                    } else if (Valor1.hasOwnProperty('Numero')) {
                                        if (Valor2.hasOwnProperty('Numero')) {
                                            res += temp + " = " + Valor1.Numero + " " + Operador + " " + Valor2.Numero + ";\n";
                                        } else if (Valor2.hasOwnProperty('Temporal')) {
                                            res += temp + " = " + Valor1.Numero + " " + Operador + " " + Valor2.Temporal + ";\n";
                                        } else if (Valor2.hasOwnProperty('H')) {
                                            res += temp + " = " + Valor1.Numero + " " + Operador + " H;\n";
                                        } else if (Valor2.hasOwnProperty('E')) {
                                            res += temp + " = " + Valor1.Numero + " " + Operador + " E;\n";
                                        } else if (Valor2.hasOwnProperty('P')) {
                                            res += temp + " = " + Valor1.Numero + " " + Operador + " P;\n";
                                        }
                                    } else if (Valor1.hasOwnProperty('Temporal')) {
                                        if (Valor2.hasOwnProperty('Numero')) {
                                            res += temp + " = " + Valor1.Temporal + " " + Operador + " " + Valor2.Numero + ";\n";
                                        } else if (Valor2.hasOwnProperty('Temporal')) {
                                            res += temp + " = " + Valor1.Temporal + " " + Operador + " " + Valor2.Temporal + ";\n";
                                        } else if (Valor2.hasOwnProperty('H')) {
                                            res += temp + " = " + Valor1.Temporal + " " + Operador + " H;\n";
                                        } else if (Valor2.hasOwnProperty('E')) {
                                            res += temp + " = " + Valor1.Temporal + " " + Operador + " E;\n";
                                        } else if (Valor2.hasOwnProperty('P')) {
                                            res += temp + " = " + Valor1.Temporal + " " + Operador + " P;\n";
                                        }
                                    }

                                } else {
                                    // unario
                                    let Valor1 = a.Valor.Valor1;
                                    if (Valor1.hasOwnProperty('Numero')) {
                                        res += temp + " = " + Operador + " " + Valor1.Numero + ";\n";
                                    } else if (Valor1.hasOwnProperty('Temporal')) {
                                        res += temp + " = " + Operador + " " + Valor1.Temporal + ";\n";
                                    } else if (Valor1.hasOwnProperty('H')) {
                                        res += temp + " = " + Operador + " H;\n";
                                    } else if (Valor1.hasOwnProperty('E')) {
                                        res += temp + " = " + Operador + " E;\n";
                                    } else if (Valor1.hasOwnProperty('P')) {
                                        res += temp + " = " + Operador + " P;\n";
                                    }

                                }

                            }


                        } else {
                            // aqui van solo las copias
                            if (a.Valor.hasOwnProperty('Numero')) {
                                res += temp + " = " + a.Valor.Numero + ";\n";
                            } else if (a.Valor.hasOwnProperty('Temporal')) {
                                res += temp + " = " + a.Valor.Temporal + ";\n";
                            } else if (a.Valor.hasOwnProperty('H')) {
                                res += temp + " = H;\n";
                            } else if (a.Valor.hasOwnProperty('E')) {
                                res += temp + " = E;\n";
                            } else if (a.Valor.hasOwnProperty('P')) {
                                res += temp + " = P;\n";
                            } else if (a.Valor.hasOwnProperty('Stack')) {
                                let Valor1 = a.Valor.Stack;
                                if (Valor1.hasOwnProperty('Numero')) {
                                    res += temp + " = Stack[" + Valor1.Numero + "];\n";
                                } else if (Valor1.hasOwnProperty('Temporal')) {
                                    res += temp + " = Stack[" + Valor1.Temporal + "];\n";
                                } else if (Valor1.hasOwnProperty('H')) {
                                    res += temp + " = Stack[" + " H];\n";
                                } else if (Valor1.hasOwnProperty('E')) {
                                    res += temp + " = Stack[" + " E];\n";
                                } else if (Valor1.hasOwnProperty('P')) {
                                    res += temp + " = Stack[" + " P];\n";
                                }
                            } else if (a.Valor.hasOwnProperty('Heap')) {
                                let Valor1 = a.Valor.Heap;
                                if (Valor1.hasOwnProperty('Numero')) {
                                    res += temp + " = Heap[" + Valor1.Numero + "];\n";
                                } else if (Valor1.hasOwnProperty('Temporal')) {
                                    res += temp + " = Heap[" + Valor1.Temporal + "];\n";
                                } else if (Valor1.hasOwnProperty('H')) {
                                    res += temp + " = Heap[" + " H];\n";
                                } else if (Valor1.hasOwnProperty('E')) {
                                    res += temp + " = Heap[" + " E];\n";
                                } else if (Valor1.hasOwnProperty('P')) {
                                    res += temp + " = Heap[" + " P];\n";
                                }
                            } else {
                                console.log(a.Valor)
                            }

                        }

                    }
                } else if (a.hasOwnProperty('Stack')) {
                    /**
                     * Aqui van las asignaciones al Stack
                     */
                    if (a.Pos.hasOwnProperty('H')) {
                        if (a.Valor.hasOwnProperty('P')) {
                            res += "Stack[H] = P;\n";
                        } else if (a.Valor.hasOwnProperty('H')) {
                            res += "Stack[H] = H;\n";
                        } else if (a.Valor.hasOwnProperty('E')) {
                            res += "Stack[H] = E;\n";
                        } else if (a.Valor.hasOwnProperty('Numero')) {
                            res += "Stack[H] = " + a.Valor.Numero + ";\n";
                        } else if (a.Valor.hasOwnProperty('Temporal')) {
                            res += "Stack[H] = " + a.Valor.Temporal + ";\n";
                        } else if (a.Valor.hasOwnProperty('Operador') && a.Valor.Operador == "-" && (!a.Valor.hasOwnProperty('Valor2'))) {
                            if (a.Valor1.hasOwnProperty('P')) {
                                res += "Stack[H] = - P;\n";
                            } else if (a.Valor1.hasOwnProperty('H')) {
                                res += "Stack[H] = - H;\n";
                            } else if (a.Valor1.hasOwnProperty('E')) {
                                res += "Stack[H] = - E;\n";
                            } else if (a.Valor1.hasOwnProperty('Numero')) {
                                res += "Stack[H] = -" + a.Valor.Numero + ";\n";
                            } else if (a.Valor1.hasOwnProperty('Temporal')) {
                                res += "Stack[H] = -" + a.Valor.Temporal + ";\n";
                            } else if (a.Valor1.hasOwnProperty('Operador') && a.Valor.Operador == "-" && (!a.Valor.hasOwnProperty('Valor2'))) {
                                res += "Stack[H] = -" + a.Valor.Temporal + ";\n";
                            }
                        }
                    } else if (a.Pos.hasOwnProperty('P')) {
                        if (a.Valor.hasOwnProperty('P')) {
                            res += "Stack[P] = P;\n";
                        } else if (a.Valor.hasOwnProperty('H')) {
                            res += "Stack[P] = H;\n";
                        } else if (a.Valor.hasOwnProperty('E')) {
                            res += "Stack[P] = E;\n";
                        } else if (a.Valor.hasOwnProperty('Numero')) {
                            res += "Stack[P] = " + a.Valor.Numero + ";\n";
                        } else if (a.Valor.hasOwnProperty('Temporal')) {
                            res += "Stack[P] = " + a.Valor.Temporal + ";\n";
                        } else if (a.Valor.hasOwnProperty('Operador') && a.Valor.Operador == "-" && (!a.Valor.hasOwnProperty('Valor2'))) {
                            if (a.Valor1.hasOwnProperty('P')) {
                                res += "Stack[P] = - P;\n";
                            } else if (a.Valor1.hasOwnProperty('H')) {
                                res += "Stack[P] = - H;\n";
                            } else if (a.Valor1.hasOwnProperty('E')) {
                                res += "Stack[P] = - E;\n";
                            } else if (a.Valor1.hasOwnProperty('Numero')) {
                                res += "Stack[P] = -" + a.Valor.Numero + ";\n";
                            } else if (a.Valor1.hasOwnProperty('Temporal')) {
                                res += "Stack[P] = -" + a.Valor.Temporal + ";\n";
                            } else if (a.Valor1.hasOwnProperty('Operador') && a.Valor.Operador == "-" && (!a.Valor.hasOwnProperty('Valor2'))) {
                                res += "Stack[P] = -" + a.Valor.Temporal + ";\n";
                            }
                        }
                    } else if (a.Pos.hasOwnProperty('E')) {
                        if (a.Valor.hasOwnProperty('P')) {
                            res += "Stack[E] = P;\n";
                        } else if (a.Valor.hasOwnProperty('H')) {
                            res += "Stack[E] = H;\n";
                        } else if (a.Valor.hasOwnProperty('E')) {
                            res += "Stack[E] = E;\n";
                        } else if (a.Valor.hasOwnProperty('Numero')) {
                            res += "Stack[E] = " + a.Valor.Numero + ";\n";
                        } else if (a.Valor.hasOwnProperty('Temporal')) {
                            res += "Stack[E] = " + a.Valor.Temporal + ";\n";
                        } else if (a.Valor.hasOwnProperty('Operador') && a.Valor.Operador == "-" && (!a.Valor.hasOwnProperty('Valor2'))) {
                            if (a.Valor1.hasOwnProperty('P')) {
                                res += "Stack[E] = - P;\n";
                            } else if (a.Valor1.hasOwnProperty('H')) {
                                res += "Stack[E] = - H;\n";
                            } else if (a.Valor1.hasOwnProperty('E')) {
                                res += "Stack[E] = - E;\n";
                            } else if (a.Valor1.hasOwnProperty('Numero')) {
                                res += "Stack[E] = -" + a.Valor.Numero + ";\n";
                            } else if (a.Valor1.hasOwnProperty('Temporal')) {
                                res += "Stack[E] = -" + a.Valor.Temporal + ";\n";
                            } else if (a.Valor1.hasOwnProperty('Operador') && a.Valor.Operador == "-" && (!a.Valor.hasOwnProperty('Valor2'))) {
                                res += "Stack[E] = -" + a.Valor.Temporal + ";\n";
                            }
                        }
                    } else if (a.Pos.hasOwnProperty('Temporal')) {
                        if (a.Valor.hasOwnProperty('P')) {
                            res += "Stack[" + a.Pos.Temporal + "] = P;\n";
                        } else if (a.Valor.hasOwnProperty('H')) {
                            res += "Stack[" + a.Pos.Temporal + "] = H;\n";
                        } else if (a.Valor.hasOwnProperty('E')) {
                            res += "Stack[" + a.Pos.Temporal + "] = E;\n";
                        } else if (a.Valor.hasOwnProperty('Numero')) {
                            res += "Stack[" + a.Pos.Temporal + "] = " + a.Valor.Numero + ";\n";
                        } else if (a.Valor.hasOwnProperty('Temporal')) {
                            res += "Stack[" + a.Pos.Temporal + "] = " + a.Valor.Temporal + ";\n";
                        } else if (a.Valor.hasOwnProperty('Operador') && a.Valor.Operador == "-" && (!a.Valor.hasOwnProperty('Valor2'))) {
                            if (a.Valor1.hasOwnProperty('P')) {
                                res += "Stack[" + a.Pos.Temporal + "] = - P;\n";
                            } else if (a.Valor1.hasOwnProperty('H')) {
                                res += "Stack[" + a.Pos.Temporal + "] = - H;\n";
                            } else if (a.Valor1.hasOwnProperty('E')) {
                                res += "Stack[" + a.Pos.Temporal + "] = - E;\n";
                            } else if (a.Valor1.hasOwnProperty('Numero')) {
                                res += "Stack[" + a.Pos.Temporal + "] = -" + a.Valor.Numero + ";\n";
                            } else if (a.Valor1.hasOwnProperty('Temporal')) {
                                res += "Stack[" + a.Pos.Temporal + "] = -" + a.Valor.Temporal + ";\n";
                            } else if (a.Valor1.hasOwnProperty('Operador') && a.Valor.Operador == "-" && (!a.Valor.hasOwnProperty('Valor2'))) {
                                res += "Stack[" + a.Pos.Temporal + "] = -" + a.Valor.Temporal + ";\n";
                            }
                        }
                    } else if (a.Pos.hasOwnProperty('Numero')) {
                        if (a.Valor.hasOwnProperty('P')) {
                            res += "Stack[" + a.Pos.Numero + "] = P;\n";
                        } else if (a.Valor.hasOwnProperty('H')) {
                            res += "Stack[" + a.Pos.Numero + "] = H;\n";
                        } else if (a.Valor.hasOwnProperty('E')) {
                            res += "Stack[" + a.Pos.Numero + "] = E;\n";
                        } else if (a.Valor.hasOwnProperty('Numero')) {
                            res += "Stack[" + a.Pos.Numero + "] = " + a.Valor.Numero + ";\n";
                        } else if (a.Valor.hasOwnProperty('Temporal')) {
                            res += "Stack[" + a.Pos.Numero + "] = " + a.Valor.Temporal + ";\n";
                        } else if (a.Valor.hasOwnProperty('Operador') && a.Valor.Operador == "-" && (!a.Valor.hasOwnProperty('Valor2'))) {
                            if (a.Valor1.hasOwnProperty('P')) {
                                res += "Stack[" + a.Pos.Numero + "] = - P;\n";
                            } else if (a.Valor1.hasOwnProperty('H')) {
                                res += "Stack[" + a.Pos.Numero + "] = - H;\n";
                            } else if (a.Valor1.hasOwnProperty('E')) {
                                res += "Stack[" + a.Pos.Numero + "] = - E;\n";
                            } else if (a.Valor1.hasOwnProperty('Numero')) {
                                res += "Stack[" + a.Pos.Numero + "] = -" + a.Valor.Numero + ";\n";
                            } else if (a.Valor1.hasOwnProperty('Temporal')) {
                                res += "Stack[" + a.Pos.Numero + "] = -" + a.Valor.Temporal + ";\n";
                            } else if (a.Valor1.hasOwnProperty('Operador') && a.Valor.Operador == "-" && (!a.Valor.hasOwnProperty('Valor2'))) {
                                res += "Stack[" + a.Pos.Numero + "] = -" + a.Valor.Temporal + ";\n";
                            }
                        }
                    }

                } else if (a.hasOwnProperty('Heap')) {
                    /**
                     * Aqui van las asignaciones al Heap
                     */
                    if (a.Pos.hasOwnProperty('H')) {
                        if (a.Valor.hasOwnProperty('P')) {
                            res += "Heap[H] = P;\n";
                        } else if (a.Valor.hasOwnProperty('H')) {
                            res += "Heap[H] = H;\n";
                        } else if (a.Valor.hasOwnProperty('E')) {
                            res += "Heap[H] = E;\n";
                        } else if (a.Valor.hasOwnProperty('Numero')) {
                            res += "Heap[H] = " + a.Valor.Numero + ";\n";
                        } else if (a.Valor.hasOwnProperty('Temporal')) {
                            res += "Heap[H] = " + a.Valor.Temporal + ";\n";
                        } else if (a.Valor.hasOwnProperty('Operador') && a.Valor.Operador == "-") {

                            if (a.Valor.Valor1.hasOwnProperty('P')) {
                                res += "Heap[H] = - P;\n";
                            } else if (a.Valor.Valor1.hasOwnProperty('H')) {
                                res += "Heap[H] = - H;\n";
                            } else if (a.Valor.Valor1.hasOwnProperty('E')) {
                                res += "Heap[H] = - E;\n";
                            } else if (a.Valor.Valor1.hasOwnProperty('Numero')) {
                                res += "Heap[H] = -" + a.Valor.Valor1.Numero + ";\n";
                            } else if (a.Valor.Valor1.hasOwnProperty('Temporal')) {
                                res += "Heap[H] = -" + a.Valor.Valor1.Temporal + ";\n";
                            }
                        }
                    } else if (a.Pos.hasOwnProperty('P')) {
                        if (a.Valor.hasOwnProperty('P')) {
                            res += "Heap[P] = P;\n";
                        } else if (a.Valor.hasOwnProperty('H')) {
                            res += "Heap[P] = H;\n";
                        } else if (a.Valor.hasOwnProperty('E')) {
                            res += "Heap[P] = E;\n";
                        } else if (a.Valor.hasOwnProperty('Numero')) {
                            res += "Heap[P] = " + a.Valor.Numero + ";\n";
                        } else if (a.Valor.hasOwnProperty('Temporal')) {
                            res += "Heap[P] = " + a.Valor.Temporal + ";\n";
                        } else if (a.Valor.hasOwnProperty('Operador') && a.Valor.Operador == "-") {
                            if (a.Valor.Valor1.hasOwnProperty('P')) {
                                res += "Heap[P] = - P;\n";
                            } else if (a.Valor.Valor1.hasOwnProperty('H')) {
                                res += "Heap[P] = - H;\n";
                            } else if (a.Valor.Valor1.hasOwnProperty('E')) {
                                res += "Heap[P] = - E;\n";
                            } else if (a.Valor.Valor1.hasOwnProperty('Numero')) {
                                res += "Heap[P] = -" + a.Valor.Valor1.Numero + ";\n";
                            } else if (a.Valor.Valor1.hasOwnProperty('Temporal')) {
                                res += "Heap[P] = -" + a.Valor.Valor1.Temporal + ";\n";
                            }
                        }
                    } else if (a.Pos.hasOwnProperty('E')) {
                        if (a.Valor.hasOwnProperty('P')) {
                            res += "Heap[E] = P;\n";
                        } else if (a.Valor.hasOwnProperty('H')) {
                            res += "Heap[E] = H;\n";
                        } else if (a.Valor.hasOwnProperty('E')) {
                            res += "Heap[E] = E;\n";
                        } else if (a.Valor.hasOwnProperty('Numero')) {
                            res += "Heap[E] = " + a.Valor.Numero + ";\n";
                        } else if (a.Valor.hasOwnProperty('Temporal')) {
                            res += "Heap[E] = " + a.Valor.Temporal + ";\n";
                        } else if (a.Valor.hasOwnProperty('Operador') && a.Valor.Operador == "-") {
                            if (a.Valor.Valor1.hasOwnProperty('P')) {
                                res += "Heap[E] = - P;\n";
                            } else if (a.Valor.Valor1.hasOwnProperty('H')) {
                                res += "Heap[E] = - H;\n";
                            } else if (a.Valor.Valor1.hasOwnProperty('E')) {
                                res += "Heap[E] = - E;\n";
                            } else if (a.Valor.Valor1.hasOwnProperty('Numero')) {
                                res += "Heap[E] = -" + a.Valor.Valor1.Numero + ";\n";
                            } else if (a.Valor.Valor1.hasOwnProperty('Temporal')) {
                                res += "Heap[E] = -" + a.Valor.Valor1.Temporal + ";\n";
                            }
                        }
                    } else if (a.Pos.hasOwnProperty('Temporal')) {
                        if (a.Valor.hasOwnProperty('P')) {
                            res += "Heap[" + a.Pos.Temporal + "] = P;\n";
                        } else if (a.Valor.hasOwnProperty('H')) {
                            res += "Heap[" + a.Pos.Temporal + "] = H;\n";
                        } else if (a.Valor.hasOwnProperty('E')) {
                            res += "Heap[" + a.Pos.Temporal + "] = E;\n";
                        } else if (a.Valor.hasOwnProperty('Numero')) {
                            res += "Heap[" + a.Pos.Temporal + "] = " + a.Valor.Numero + ";\n";
                        } else if (a.Valor.hasOwnProperty('Temporal')) {
                            res += "Heap[" + a.Pos.Temporal + "] = " + a.Valor.Temporal + ";\n";
                        } else if (a.Valor.hasOwnProperty('Operador') && a.Valor.Operador == "-") {
                            if (a.Valor.Valor1.hasOwnProperty('P')) {
                                res += "Heap[" + a.Pos.Temporal + "] = - P;\n";
                            } else if (a.Valor1.hasOwnProperty('H')) {
                                res += "Heap[" + a.Pos.Temporal + "] = - H;\n";
                            } else if (a.Valor.Valor1.hasOwnProperty('E')) {
                                res += "Heap[" + a.Pos.Temporal + "] = - E;\n";
                            } else if (a.Valor.Valor1.hasOwnProperty('Numero')) {
                                res += "Heap[" + a.Pos.Temporal + "] = -" + a.Valor.Valor1.Numero + ";\n";
                            } else if (a.Valor.Valor1.hasOwnProperty('Temporal')) {
                                res += "Heap[" + a.Pos.Temporal + "] = -" + a.Valor.Valor1.Temporal + ";\n";
                            }
                        }
                    } else if (a.Pos.hasOwnProperty('Numero')) {
                        if (a.Valor.hasOwnProperty('P')) {
                            res += "Heap[" + a.Pos.Numero + "] = P;\n";
                        } else if (a.Valor.hasOwnProperty('H')) {
                            res += "Heap[" + a.Pos.Numero + "] = H;\n";
                        } else if (a.Valor.hasOwnProperty('E')) {
                            res += "Heap[" + a.Pos.Numero + "] = E;\n";
                        } else if (a.Valor.hasOwnProperty('Numero')) {
                            res += "Heap[" + a.Pos.Numero + "] = " + a.Valor.Numero + ";\n";
                        } else if (a.Valor.hasOwnProperty('Temporal')) {
                            res += "Heap[" + a.Pos.Numero + "] = " + a.Valor.Temporal + ";\n";
                        } else if (a.Valor.hasOwnProperty('Operador') && a.Valor.Operador == "-") {
                            if (a.Valor.Valor1.hasOwnProperty('P')) {
                                res += "Heap[" + a.Pos.Numero + "] = - P;\n";
                            } else if (a.Valor.Valor1.hasOwnProperty('H')) {
                                res += "Heap[" + a.Pos.Numero + "] = - H;\n";
                            } else if (a.Valor.Valor1.hasOwnProperty('E')) {
                                res += "Heap[" + a.Pos.Numero + "] = - E;\n";
                            } else if (a.Valor.Valor1.hasOwnProperty('Numero')) {
                                res += "Heap[" + a.Pos.Numero + "] = -" + a.Valor.Valor1.Numero + ";\n";
                            } else if (a.Valor.Valor1.hasOwnProperty('Temporal')) {
                                res += "Heap[" + a.Pos.Numero + "] = -" + a.Valor.Valor1.Temporal + ";\n";
                            }
                        }
                    }

                } else {
                    //  console.log(a);
                }

            } else {
                console.log(actual[i]);
            }

        }

        return res;
    }

}
module.exports = Optimizacion;