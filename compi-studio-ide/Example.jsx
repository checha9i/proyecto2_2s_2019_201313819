import React, { Component } from 'react';

export default class Example extends Component {
  render() {
    return (
      <div className="example">
        { this.props.children }
      </div>
    )
  }
}
