import React from 'react';
import './TablaErrores.css';
import { Table } from 'react-bootstrap';

class TablaErrores extends React.Component {
  constructor(props) {
    super(props);

  }
  render() {
    return (
      <div className="TablaErrores">
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Numero</th>
              <th>Descripcion</th>
              <th>Linea</th>
              <th>Columna</th>
              <th>Tipo</th>
            </tr>
          </thead>
          <tbody>
            {
              this.props.Errores.map((item, i) => (
                <tr>
                  <td>
                    {i}
                  </td>
                  <td>
                    {item.error.description}
                  </td>
                  <td>
                    {item.error.linea}
                  </td>
                  <td>
                    {item.error.columna}
                  </td>
                  <td>
                    {item.error.tipo}
                  </td>
                </tr>
              ))
            }

          </tbody>
        </Table>
      </div>
    );
  }
}

export default TablaErrores;
