import React from 'react';
//import PropTypes from 'prop-types';
import './Header.css';
import { Navbar, Nav } from 'react-bootstrap';
const Header = () => (
  <div className="Header">
    <Navbar bg="dark" variant="dark">
      <Navbar.Brand href="/">Compiladores 2</Navbar.Brand>
      <Nav className="mr-auto">
        <Nav.Link >1er semestre 2020</Nav.Link>
        <Nav.Link >Seccion C</Nav.Link>
        <Nav.Link >Cesar Javier Solares Orozco</Nav.Link>
        <Nav.Link >201313819</Nav.Link>
      </Nav>

    </Navbar>
  </div>
);

Header.propTypes = {};

Header.defaultProps = {};

export default Header;
