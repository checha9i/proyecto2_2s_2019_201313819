import React from 'react';
import './TablaSimbolos.css';
import { Table } from 'react-bootstrap';

class TablaSimbolos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      TS: this.props.TS
    };
  }

  render() {
    return (
      <div className="TablaSimbolos">
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Numero</th>
              <th>Id</th>
              <th>Valor</th>
              <th>Tipo</th>
              <th>Rol</th>
            </tr>
          </thead>
          <tbody>
            {
              this.props.TS.map(item => (
                <tr>
                  <td>
                    {item.cont}
              </td>
                  <td>
                    {item.id}
            </td>
                  <td>
                    {null}
            </td>
            <td>
              {item.tipo}
            </td>
            <td>
              {item.rol}
            </td>
                </tr>
              ))
            }

          </tbody>
        </Table>
      </div>
    );
  }
}

export default TablaSimbolos;
