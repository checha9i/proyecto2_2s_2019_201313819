import React, { PropTypes, Component } from 'react';


class Upload extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick(e) {
    this.refs.fileUploader.click();
  }
  render() {
    return (
      <div className="body-content">
        <div className="add-media" onClick={this.handleClick.bind(this)}>
          <i className="plus icon"></i>
          
        </div>
      </div>
    )
  }
}

export default Upload;