import React from 'react';

import './CodeEditor.css';
import AceEditor from "react-ace";
import "ace-builds/src-min-noconflict/mode-javascript";
import "ace-builds/src-min-noconflict/theme-monokai";


export default class CodeEditor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      code: "void principal(){"
        + " \n" +
        "}"
    };
  }
  onChange(e) {
    e.preventDefault()
    this.setState({ code: e });

    //console.log(this.state.code);
    // this.props.onchange(this.state.code);
    return true;
  }

  render() {
    return (
      <div className="CodeEditor">
        <AceEditor mode="javascript" theme="monokai" value={this.props.code} width={"auto"}
          onChange={this.props.handleChange} style={{ width: "100%", height: "300px" }}
        />
      </div>
    );
  }
}

