import React from 'react';

import './Console.css';
import AceEditor from "react-ace";
import "ace-builds/src-min-noconflict/mode-javascript";
import "ace-builds/src-min-noconflict/theme-monokai";


export default class Console extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      code: ""
    };

    this.onChange = this.onChange.bind(this);
  }
  onChange(e) {
    this.setState({ code: e });

  }

  render() {
    return (
      <div className="Console">
        <AceEditor mode="javascript" theme="monokai" value={this.props.code} width={"auto"}
          style={{ width: "100%", height: "300px" }} readOnly
        />
      </div>
    );
  }
}

