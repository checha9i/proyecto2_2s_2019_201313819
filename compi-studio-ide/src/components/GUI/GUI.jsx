import React from 'react';
import './GUI.css';
import { Container, Row, Form, Button, Col, Tabs, Tab, FormLabel, Nav, NavDropdown, Navbar } from 'react-bootstrap';
import CodeEditor from '../CodeEditor/CodeEditor';
import Console from '../Console/Console';
import axios from 'axios';
import TablaSimbolos from '../TablaSimbolos/TablaSimbolos';
import TablaErrores from '../TablaErrores/TablaErrores';
import TablaFunciones from '../TablaFunciones/TablaFunciones';
import TablaOptimizacion from '../TablaOptimizacion/TablaOptimizacion';
import Iframe from 'react-iframe';

class GUI extends React.Component {
  constructor(props) {

    super(props);
    this.state = {
      key: 1,
      TextToTranslate: "",
      Consola: "",
      Errores: [],
      TS: [],
      message: "",
      Textos: [""],
      activeKey: 0,
      Consolas: [""],
      cambio: 0,
      Funciones: [],
      TextoToOptimize: "",
      OutputOptimize: "",
      Optimizaciones: []

    }

    this.AST = this.AST.bind(this);
    this.Response = this.Response.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeOptimize = this.handleChangeOptimize.bind(this);
    this.handleClick = this.handleClick.bind(this);

    this.Translate = this.Translate.bind(this);
    this.CrearTab = this.CrearTab.bind(this);
    this.Eliminar = this.Eliminar.bind(this);
    this.Optimize = this.Optimize.bind(this);
  }

  CrearTab() {



    let floors = [...this.state.Textos];
    floors.push("")
    this.setState({ Textos: floors, activeKey: floors.length - 1 });

  }

  Eliminar() {
    let floors = [...this.state.Textos];
    let cons = [...this.state.Consolas];
    if (this.state.activeKey != 0) {
      floors.splice(this.state.activeKey, 1);
      cons.splice(this.state.activeKey, 1);
      this.setState({ activeKey: floors.length - 1, Textos: floors, Consolas: cons });
    }


  }

  AST(e) {
    window.open('http://localhost:5000/AST', '_blank');
  }

  Translate(e) {
    e.preventDefault();
    var entrada = { "Entrada": this.state.Textos[this.state.activeKey] };

    axios.post('http://localhost:5000/Translate', entrada)
      .then((response) => {

        this.setState({ TS: [], Errores: [] });
        let floors = [...this.state.Consolas];
        floors[this.state.activeKey] = response.data.Salida;
        this.setState({ TS: response.data.TS, Errores: response.data.Errores, Funciones: response.data.Funciones, Consolas: floors });
        // this.descargar()
      }, (error) => {
        console.log(error);
      });

  }


  Optimize(e) {
    e.preventDefault();
    var entrada = { "Entrada": this.state.TextoToOptimize };

    axios.post('http://localhost:5000/Optimize', entrada)
      .then((response) => {


        this.setState({ Optimizaciones: response.data.Optimizaciones, OutputOptimize: response.data.Salida });
        // this.descargar()
      }, (error) => {
        console.log(error);
      });

  }

  descargar = (e) => {

    const element = document.createElement("a");
    const file = new Blob([this.state.Consolas[this.state.activeKey]], { type: 'text/plain' });
    element.href = URL.createObjectURL(file);
    element.download = "C3D.txt";
    document.body.appendChild(element); // Required for this to work in FireFox
    element.click();
  }

  Response(e) {
    e.preventDefault();
    var entrada = { "Entrada": this.state.Textos[this.state.activeKey] };

    axios.post('http://localhost:5000/Prueba', entrada)
      .then((response) => {


        let floors = [...this.state.Consolas];
        floors[this.state.activeKey] = JSON.stringify(response.data).replace(/{/gi, "\n{\n").replace(/}/gi, "\n}\n");
        this.setState({ TS: [], Errores: [], Consolas: floors });

        this.setState({ cambio: 0 });

      }, (error) => {
        console.log(error);
      });

  }

  handleChange(e) {
    let floors = [...this.state.Textos];
    floors[this.state.activeKey] = e;
    this.setState({ Textos: floors });


  }

  handleChangeOptimize(e) {
    this.setState({ TextoToOptimize: e });
  }

  handleClick(e) {
    e.preventDefault()
    this.refs.fileUploader.click();

  }

  showFile = async (e) => {
    e.preventDefault()
    const reader = new FileReader()
    reader.onload = async (e) => {
      const text = (e.target.result)

      let floors = [...this.state.Textos];
      floors[this.state.activeKey] = text;
      this.setState({ Textos: floors });

    };
    reader.readAsText(e.target.files[0])
  }
  downloadTxtFile = (e) => {
    e.preventDefault()
    const element = document.createElement("a");
    const file = new Blob([this.state.Textos[this.state.activeKey]], { type: 'text/plain' });
    element.href = URL.createObjectURL(file);
    element.download = "File1.j";
    document.body.appendChild(element); // Required for this to work in FireFox
    element.click();
  }
  render() {
    return (
      <div className="col-auto">

        <Tabs
          id="Modo"
          defaultActiveKey={1}
          onSelect={(k) => this.setState({ k })}
        >
          <Tab eventKey={1} title="J#">
            <Container fluid>
              <Row>
                <Col md={12}>
                  <Navbar bg="light" expand="lg">
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                      <Nav className="mr-auto">
                        <NavDropdown title="Archivo" id="basic-nav-dropdown">
                          <NavDropdown.Item onClick={this.handleClick.bind(this)}>Abrir</NavDropdown.Item>
                          <NavDropdown.Item onClick={this.downloadTxtFile}>Guardar</NavDropdown.Item>
                          <NavDropdown.Divider />
                          <NavDropdown.Item onClick={this.CrearTab}>Crear</NavDropdown.Item>
                          <NavDropdown.Item onClick={this.Eliminar}>Eliminar</NavDropdown.Item>
                        </NavDropdown>
                        <Nav.Link onClick={this.Translate}>Traducir</Nav.Link>

                        {/*
                        <Nav.Link onClick={this.Response}>Probar</Nav.Link>
                        <Form onSubmit={this.Translate} inline>
                          <Button variant="primary" type="submit">Ejecutar</Button>
                        </Form>
                        <Form onSubmit={this.Response} inline>
                          <Button variant="primary" type="submit">Ejecutar</Button>
                        </Form>
                        */}
                        <Nav.Link onClick={this.AST}>Reporte AST</Nav.Link>

                      </Nav>
                    </Navbar.Collapse>
                  </Navbar>
                  <input type="file" id="file" ref="fileUploader" onChange={(e) => { e.preventDefault(); this.showFile(e) }} style={{ display: "none" }} />
                </Col>
              </Row>
              <Row >
                <Col md={12}>
                  <Container fluid>
                    <Tabs
                      id="Tabs"
                      defaultActiveKey={this.state.activeKey}
                      onSelect={(k) => { this.setState({ activeKey: k, TextToTranslate: this.state.Textos[k] }) }}
                      onChange={() => { this.forceUpdate(); }}
                    >
                      {this.state.Textos.map((T, k) => (

                        <Tab eventKey={k} title={k}>
                          <Row md={12}>

                            <Col md={6}>
                              <FormLabel>
                                Entrada:
                  </FormLabel>
                            </Col>
                            <Col md={6}>
                              <FormLabel>
                                Salida:
                  </FormLabel>
                            </Col>

                          </Row>
                          <Row >
                            <Col sm={6}>
                              <CodeEditor name="editor" handleChange={this.handleChange} code={this.state.Textos[this.state.activeKey]} />
                            </Col>

                            <Col sm={6}>
                              <Console name="consola" code={this.state.Consolas[this.state.activeKey]} />
                            </Col>
                          </Row>
                        </Tab>
                      ))}
                    </Tabs>
                  </Container>
                </Col>
              </Row >
              <Row >
                <Col >
                  <Tabs
                    id="Reportes"
                    defaultActiveKey={1}
                    onSelect={(k) => this.setState({ k })}
                  >
                    <Tab eventKey={1} title="Tabla de Simbolos">
                      <TablaSimbolos TS={this.state.TS}></TablaSimbolos>
                    </Tab>
                    <Tab eventKey={2} title="Tabla de Errores">
                      <TablaErrores Errores={this.state.Errores}></TablaErrores>
                    </Tab>
                    <Tab eventKey={3} title="Reporte de Funciones">
                      <TablaFunciones Funciones={this.state.Funciones}></TablaFunciones>
                    </Tab>
                  </Tabs>



                </Col>
              </Row>
            </Container>
          </Tab>
          <Tab eventKey={2} title="Optimizar">
            <Container fluid>
              <Row>
                <Col md={12}>
                  <Navbar bg="light" expand="lg">
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                      <Nav className="mr-auto">

                        <Nav.Link onClick={this.Optimize}>Optimizar</Nav.Link>
                      </Nav>
                    </Navbar.Collapse>
                  </Navbar>

                </Col>
              </Row>
              <Row >
                <Col md={12}>
                  <Container fluid>

                    <Row md={12}>

                      <Col md={6}>
                        <FormLabel>
                          Entrada:
                  </FormLabel>
                      </Col>
                      <Col md={6}>
                        <FormLabel>
                          Salida:
                  </FormLabel>
                      </Col>

                    </Row>
                    <Row >
                      <Col sm={6}>
                        <CodeEditor name="editorOptimize" handleChange={this.handleChangeOptimize} code={this.state.TextoToOptimize} />
                      </Col>

                      <Col sm={6}>
                        <Console name="consolaOptimizae" code={this.state.OutputOptimize} />
                      </Col>
                    </Row>
                    <Row >
                      <Col >
                        <Tabs
                          id="ReporteOptizacion"
                          defaultActiveKey={1}
                          onSelect={(k) => this.setState({ k })}
                        >
                          <Tab eventKey={1} title="Reporte Optimizacion">

                            <TablaOptimizacion TO={this.state.Optimizaciones}>

                            </TablaOptimizacion>
                          </Tab>
                        </Tabs>
                      </Col>
                    </Row>
                  </Container>
                </Col>
              </Row >
            </Container>
          </Tab>
          <Tab eventKey={3} title="Ejecutar C3D">
            <Iframe url="http://ec2-54-211-1-152.compute-1.amazonaws.com/" width="1450px"
              height="650px" />
          </Tab>

        </Tabs>
      </div>
    );

  }
}



export default GUI;
