import React from 'react';
import './TablaFunciones.css';
import { Table } from 'react-bootstrap';

class TablaFunciones extends React.Component {
  constructor(props) {
    super(props);

  }
  render() {
    return (
      <div className="TablaFunciones">
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>No.</th>
              <th>Tipo</th>
              <th>Nombre</th>
              <th>No_Parametros</th>
            </tr>
          </thead>
          <tbody>
            {
              this.props.Funciones.map((item, i) => (
                <tr>
                  <td>
                    {i}
                  </td>
                  <td>
                    {item.Tipo}
                  </td>
                  <td>
                    {item.Nombre}
                  </td>
                  <td>
                    {item.Parametros.length}
                  </td>
                </tr>
              ))
            }

          </tbody>
        </Table>
      </div>
    );
  }
}

export default TablaFunciones;
