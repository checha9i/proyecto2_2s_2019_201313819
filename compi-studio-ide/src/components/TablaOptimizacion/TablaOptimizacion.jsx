import React from 'react';
import './TablaOptimizacion.css';
import { Table } from 'react-bootstrap';

class TablaOptimizacion extends React.Component {
  constructor(props) {
    super(props);

  }
  render() {
    return (
      <div className="TablaOptimizacion">
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>No.</th>
              <th>Tipo</th>
              <th>Linea</th>
              <th>Columna</th>
              <th>Nombre</th>
              <th>Descripcion</th>
            </tr>
          </thead>
          <tbody>
            {
              this.props.TO.map((item, i) => (
                <tr>
                  <td>
                    {i}
                  </td>
                  <td>
                    {item.Regla}
                  </td>
                  <td>
                    {item.Linea}
                  </td>
                  <td>
                    {item.Columna}
                  </td>
                  <td>
                    {item.Nombre}
                  </td>
                  <td>
                    {item.Descripcion}
                  </td>
                </tr>
              ))
            }

          </tbody>
        </Table>
      </div>
    );
  }
}

export default TablaOptimizacion;
