# Proyecto 2 Compiladores 
 Universidad de San Carlos de Guatemala

 Facultad de Ingeniería

 Escuela de Ciencias y Sistemas
## Descripcion

Este proyecto se basa en javascript corriendo sobre Express y React JS.

El tema del proyecto es Traducir codigo J# a Codigo 3 Direcciones .

Posteriormente se puede Optimizar ese código.

Proyecto realizado en el Primer Semestre de 2020.


## Requisitos

Es necesario tener instalado Node JS instalado en la maquina, tambien Graphviz.

### Instalar Serve 
Se instala serve de forma global con el siguiente comando de node:

```bash
npm install -g serve
```

### Instalar dependencias de express
Acceder a la carpeta compi-studio-server y ejecutar el siguiente comando
```bash
npm install
```

## Deploy

### EXPRESS
Acceder a la carpet compi-studio-server y ejecutar el siguiente comando

```bash
npm start
```
### React
Acceder a la carpet compi-studio-ide y ejecutar el siguiente comando

```bash
serve -s build -l 80
```

## Uso
Acceder a [http://localhost/](http://localhost/)